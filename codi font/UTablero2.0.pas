unit UTablero;

interface

Uses NiceGrid, ExtCtrls, Graphics, Grids;

type
 TPesa = Class
  private
   // dades escalades
   t: Integer; // top  (y)
   l: Integer; // left (x)
   w: Integer;
   h: Integer;

   // dades escalades
   ft: Double; // top  (y)
   fl: Double; // left (x)
   fw: Double;
   fh: Double;

   //dades dibuix (necessito per a clica a sobre
   DX: Integer; // top  (y)
   DY: Integer; // left (x)
   DW: Integer;
   DH: Integer;

   Tab: Integer; // numero de tablero on posare la pe�a
   Us: Boolean;
   Sel: Boolean; //si esta seleccionada al interfa�. la clicada. la que se veu. la que se pot modificar

   ID: Integer; // numero dins la matriu de peces
  public
  // dades reals
   AmpleTot: Integer; // totals, en xapat i tot
   AltTot: Integer; // totals, en xapat i tot
   Ample: Integer; // sense xapat. real a tallat
   Alt: Integer; // sense xapat. real a tallat
   X: Integer;
   Y: Integer;
   XF: Integer; // distancia xapat frontal
   XD: Integer; // dreta
   XT: Integer; // Trasera
   XE: Integer; // esquerra

   Gir: Boolean;

   Constructor Create(Amplada, Altura, Frontal, Dreta, Esquerra, Detras, Girar, Pos: Integer);
   Destructor Destroy(); override;

   Procedure Girar();
   Procedure Limpiar();
   Procedure EnUs();
   Procedure Selecionada(Fun: Boolean);
 end;

 RMides = Record // estructura que ajuda a controlar les diferents mides de les peces
   Mida: Integer; // mida
   Num: Integer; // numero de peces que tenen esta mida

   Cal: Integer; // auxiliar calcul
   Tabs: Array of Integer; // taula hash en les peces ordenades per l'altra mida
 end;

 TMides = class
  private
   Mides: array of RMides;

   procedure PosarMida(Mida, IDPesa: Integer);
   
  public
   Constructor Create();
   Destructor Destroy();

   Procedure AddMida(Pesa: TPesa);

   Procedure Limpiar();
 end;

  TNum = Record // estructura de calculs de posicions de les peces
    Tab: Array of TPesa;
    ObjX: Integer;
    ObjY: Integer;
  end;

  TCalc = class // clase que realitza els calculs per a ordenar les peces dins dels tableros
   private
    FulT: Integer; // ample fulla
    Mida: Integer;
    Max: Integer;
    SobraSup: Integer;
    SobraTot: Integer;
    Num: Array of TNum;

    procedure ProcesObj();  // calcula, per a cada pe�a, el total sobrant que quedaria si la tragues

   public
    constructor Create(MidaX, MidaY: Integer);
    destructor Destroy(); override;

    function Add(Pesa: TPesa): Boolean; // posa una pe�a al calcul
    function Del(Pesa: TPesa): Boolean; // trau una pe�a del calcul
    procedure Clear(); overload; // limpia el calcul
    Procedure Clear(MidaX, MidaY: Integer); overload;

    function Count(): Integer; // retorna el numero de peces dins del calcul
    function Get(Pos, Col: Integer): Integer; // retorna l'index de una pe�a del calcul
    function InObj(valor: Integer; var Sobrant: Integer): Integer; // busca la pe�a que s'hauria de traure
    // per a que al posar una altra de mida "valor", el restant fos minim
  end;

 TTablero = Class
  private
   Tabs: Array of TPesa;
   Mides: TMides;

   C: TCalc;

   Qdla: TStringGrid;

   Ok: Boolean;

   AmpT: Integer; // amplada total  (menos els marges)
   AltT: Integer; // altura total  (menos els marges)
   AmpR: Integer; // amplada real
   AltR: Integer; // altura real
//   GroT: Integer; // gruis del tablero
   MarT: Integer; // marge del tablero
   FulT: Integer; // ample fulla
//   Color: Integer; // color del tablero
   IDTab: Integer; //id del tablero

   FX, FY: Double; // factors d'escala
   ActTab: Integer; // tablero actual
   MaxTab: Integer; // numero maxim de tableros usats

   // Log
   procedure Log_EscriureDades();
   procedure Log_EscriureMatrius();

   // funcions
   procedure LimpiarUs();
   function MaxMida(): Integer;
   procedure MarcarMida(Fun: Integer; Mida: Integer);
   procedure MarcarPesa(Fun: boolean; Pesa: Integer);
   function BuscarPesa(Fun: String; Pos: Integer; var Y: Integer): Integer;
   function PosarMides(AcumX, Tablero: Integer): Integer; // funcio que posa les mides a les peces una vegada se sap on van
   procedure Calcular();


   procedure PosarMida(Mida: Integer; Pesa: Integer);
   procedure EscalarTabs();
   procedure SetTablero(Num: Integer);

  public

   Property Tablero: Integer Read ActTab Write SetTablero;

   constructor Create(Ample, Alt, IDTablero, Marge, Fulla: Integer);
   destructor Destroy(); override;

   procedure AddTab(Unitats, Ample, Alt, Frontal, Dreta, Esquerra, Detras, Girar: Integer); // inclou una pe�a nova
   procedure Clear(); // limpia les dades
   procedure Dibuixar(DX, DY: Integer); overload; // dibuixa les peces opengl
   procedure Dibuixar(Pantalla: TStringGrid); overload; // dibuixa les peces api win
   procedure Escalar(XP, YP: Integer);

   Function XYPesaDibuix(X, Y: Integer):TPesa;
   procedure LimpiarSeleccio();
 end;

 TTableroInfo = Record  // estructura per a guardar la informacio de tots els
 //tableros usats i mostrar-la comodament
  Num: Integer;
  Pos: Integer;

  IDTab: Integer;
  StrNom: String;
  StrMat: String;
  StrCol: String;
  StrGru: String;
  StrMid: String;
 end;

 TKutMan = Class
  private
   Tableros: Array of TTablero;
   Info: Array of TTableroInfo;

//   FulT: Integer; // amplada de la fulla. descomptar al resto del tall
  public
   Constructor Create();
   Destructor Destroy(); override;

   procedure SetConfig(Config: Integer);

   Procedure AddTab(Num, Ample, Alt, IDTablero, Gir, Prioritat, XapFront, XapDrt, XapDetras, XapEsq: Integer);
   Procedure Calcular();
   Procedure MostrarTableros(Obj: TNiceGrid);
   Procedure Dibuixar(Tablero, DX, DY: Integer); overload; //opengl
   Procedure Dibuixar(Tablero: Integer; Qdla: TStringGrid); overload; //api win

   Function XYPesaDibuix(Tablero, X, Y: Integer):TPesa;

   Procedure Clear();
 end;

var
 T: TKutMan;

implementation

Uses OpenGL, UglContext, UFGL, SysUtils, ULog, UConfProg,
  UDades, UFDraw;

////////////////////////////////////////////////////////////////////////////////////////
//////////    TKutMan
/////////////////////////////////////////////////////////////////////////////////////

Constructor TKutMan.Create();
begin
 ULog.IniciLog();

 SetLength(Tableros, 0);
 SetConfig(3);
end;

Destructor TKutMan.Destroy();
begin
 Clear();

 ULog.FinalLog();

 inherited Destroy();
end;

Procedure TKutMan.Calcular();
var
 a: Integer;
begin
 for a:=0 to Length(Tableros)-1 do begin
  Tableros[a].Calcular();
 end;
end;

Procedure TKutMan.Clear();
var
 a: Integer;
begin
 for a:=0 to Length(Tableros)-1 do begin
  Tableros[a].Destroy();
 end;

 SetLength(Tableros, 0);
 SetLength(Info, 0);
end;

Procedure TKutMan.AddTab(Num, Ample, Alt, IDTablero, Gir, Prioritat, XapFront, XapDrt, XapDetras, XapEsq: Integer);
var
 a, t, p: Integer;
 am, al: Integer;
begin
 t := -1;
 a := 0;
 p := LTab.FindValue(IntToStr(IDTablero));
 am := LTab.GetAmple(p);
 al := LTab.GetAlt(p);

 if Length(Tableros) = 0 then begin// no ne tinc cap, lo creo
  SetLength(Tableros, Length(Tableros)+1);
  t := High(Tableros);
  Tableros[t] := TTablero.Create(am, al, IDTablero, UConfProg.TM, UConfProg.TF);
  Tableros[t].AddTab(Num, Ample, Alt, XapFront, XapDrt, XapEsq, XapDetras, Gir);
  if CLOG then
   ULog.Escriurelog('nou tablero: '+IntToStr(t));
 end else begin
  while (a < Length(Tableros)) do begin
  // recorro tots els tableros buscan un que tingue el mateix material i gruix que la pesa que vull posar
   if (Tableros[a].IDTab = IDTablero) then begin
    t := a;
    a := Length(Tableros);
   end;
   a := a + 1;
  end;

  // si el trobo, afegixo la pesa
  if t <> -1 then begin
   Tableros[t].AddTab(Num, Ample, Alt, XapFront, XapDrt, XapEsq, XapDetras, Gir);
  end else begin
  // sino, creo un tablero nou i poso la pesa
   SetLength(Tableros, Length(Tableros)+1);
   t := High(Tableros);
   Tableros[t] := TTablero.Create(am, al, IDTablero, UConfProg.TM, UConfProg.TF);
   Tableros[t].AddTab(Num, Ample, Alt, XapFront, XapDrt, XapEsq, XapDetras, Gir);
   if CLOG then
    ULog.Escriurelog('nou tablero: '+IntToStr(t));
  end;
 end;
end;

procedure TKutMan.SetConfig(Config: Integer);
begin
 ULog.CTXT := False;
 ULog.CLOG := False;

 if (Config div 2) = 1 then
  ULog.CLOG := True;
 Config := Config mod 2;


 if (Config div 1) = 1 then
  ULog.CTXT := True;
end;

Procedure TKutMan.MostrarTableros(Obj: TNiceGrid);
var
 a, b, Tot, pt: Integer;
 col: TNiceColumn;
begin
 SetLength(Info, 0);

 // files
 for a:=0 to Length(Tableros)-1 do begin
  pt := LTab.FindValue(IntToStr(Tableros[a].IDTab));
  for b:=1 to Tableros[a].MaxTab do begin
   SetLength(Info, Length(Info)+1);
   Tot := High(Info);
   Info[Tot].Num := a;
   Info[Tot].Pos := b;

   Info[Tot].IDTab := Tableros[a].IDTab;
   Info[Tot].StrNom := LTab.GetNom(pt);
//   info[Tot].StrMat := LMat.FindStringFromValue(LTab.GetMaterial(pt));
//   info[Tot].StrCol := LCol.FindStringFromValue(LTab.GetColor(pt));
//   info[Tot].StrGru := LGruix.FindStringFromValue(LTab.GetGruix(pt));
   info[Tot].StrMid := IntToStr(LTab.GetAmple(pt))+'x'+IntToStr(LTab.GetAlt(pt));
  end;
 end;

 Obj.RowCount := Length(Info);

 for a:=0 to Length(Info)-1 do begin
  Obj.Cells[0, a] := IntToStr(a);
  Obj.Cells[1, a] := Info[a].StrNom;
  Obj.Cells[2, a] := Info[a].StrMid;
 end;

end;

Procedure TKutMan.Dibuixar(Tablero, DX, DY: Integer);
begin
 Tableros[Info[Tablero].Num].ActTab := Info[Tablero].Pos;
 Tableros[Info[Tablero].Num].Dibuixar(DX, DY);
end;

Procedure TKutMan.Dibuixar(Tablero: Integer; Qdla: TStringGrid);
begin
 Tableros[Info[Tablero].Num].LimpiarSeleccio(); //limpio sempre perque al
 //buscar una pesa en la funcio XYPesaDibuix, ja redibuixo lo tablero

 Tableros[Info[Tablero].Num].ActTab := Info[Tablero].Pos;
 Tableros[Info[Tablero].Num].Dibuixar(Qdla);
end;

Function TKutMan.XYPesaDibuix(Tablero, X, Y: Integer):TPesa;
begin
 Result := Tableros[Info[Tablero].Num].XYPesaDibuix(X, Y);
end;

////////////////////////////////////////////////////////////////////////////////////////
//////////    TTablero
/////////////////////////////////////////////////////////////////////////////////////

constructor TTablero.Create(Ample, Alt, IDTablero, Marge, Fulla: Integer);
var
 aux: Integer;
begin
 SetLength(Tabs, 0);
 SetLength(Mides, 0);

 ActTab := 1;

 if Ample > Alt then begin //Alt sempre ha de ser la mida mes llarga
  aux := Ample;
  Ample := Alt;
  Alt := aux;
 end;

 AmpR := Ample;
 AltR := Alt;
 AmpT := Ample - (Marge * 2);
 AltT := Alt - (Marge * 2);
 IDTab := IDTablero;
 MarT := Marge;
 FulT := Fulla;

 Ok := True;

 FX := 1;
 FY := 1;
 ActTab := 0;
 MaxTab := 1;

// C: TCalc;

 if CLOG then
  Log_EscriureDades();
end;

destructor TTablero.Destroy();
begin
 Ok := False;
 Clear();

 inherited Destroy();
end;

// funcio que limpia totes les matrius
procedure TTablero.Clear();
var
 a: Integer;
begin
 for a:=0 to Length(Tabs)-1 do
  Tabs[a].Destroy();

 SetLength(Tabs, 0);

 for a:=0 to High(Mides) do
  SetLength(Mides[a].Tabs, 0);

 SetLength(Mides, 0);

// Dibuixar();
end;

// funcio que inclou una nova pe�a
procedure TTablero.AddTab(Unitats, Ample, Alt, Frontal, Dreta, Esquerra, Detras, Girar: Integer);
var
 a, t: Integer;
begin
 for a:=1 to Unitats do begin
  SetLength(Tabs, Length(Tabs)+1);
  t := High(Tabs);
  Tabs[t] := TPesa.Create(Ample, Alt, Frontal, Dreta, Esquerra, Detras, Girar, t);

  // poso les mides de totes les unitats, ja que si no no se posea be a la taula de hash
  PosarMida(Ample, t);
  PosarMida(Alt, t);

  if CLOG then begin
   ULog.Escriurelog('nova pe�a num : '+IntToStr(t)+' '+IntToStr(Ample)+' x '+IntToStr(Alt));
   ULog.Escriurelog('  '+IntToStr(Frontal)+' - '+IntToStr(Dreta)+' - '+IntToStr(Esquerra)+' - '+IntToStr(Detras)+' - '+IntToStr(Girar));
  end;
 end;
end;

// funcio que inclou una nova mida dins de la matriu de les mides.
// es una matriu auxiliar que ajuda a agilitzar els calculs
procedure TTablero.PosarMida(Mida: Integer; Pesa: Integer);
var
 b, ok, ok2: Integer;
 auxm, auxn: Integer;
 m1, m2: Integer;
 Aux: TMides;
begin
  b:=0;
  ok := 0;
  while (b < Length(Mides)) and (ok < 1) do begin
   if Mides[b].Mida = Mida then begin
    Mides[b].Num := Mides[b].Num + 1;

    SetLength(Mides[b].Tabs, Length(Mides[b].Tabs) + 1);
    auxn := High(Mides[b].Tabs);
    Mides[b].Tabs[auxn] := pesa;

    ok2 := 1;
    while (auxn > 0) and (ok2 = 1) do begin
     if Tabs[Mides[b].Tabs[auxn]].Ample = mida then m1 := Tabs[Mides[b].Tabs[auxn]].Alt
     else m1 := Tabs[Mides[b].Tabs[auxn]].Ample;

     if Tabs[Mides[b].Tabs[auxn-1]].Ample = mida then m2 := Tabs[Mides[b].Tabs[auxn-1]].Alt
     else m2 := Tabs[Mides[b].Tabs[auxn-1]].Ample;

     if m1  < m2 then begin
      auxm := Mides[b].Tabs[auxn];
      Mides[b].Tabs[auxn] := Mides[b].Tabs[auxn-1];
      Mides[b].Tabs[auxn-1] := auxm;
     end else ok2:=0;

     auxn := auxn - 1;
    end;

    ok := ok + 1;
   end;

   b:=b+1;
  end;

  if ok = 0 then begin // no te mida
   SetLength(Mides, Length(Mides)+1);
   b:=High(Mides);

   Mides[b].Mida := Mida;
   Mides[b].Num := 1;
   SetLength(Mides[b].Tabs, {Length(Mides[b].Tabs) +} 1);
   auxn := High(Mides[b].Tabs);
   Mides[b].Tabs[auxn] := pesa;

   ok2 := 1;     // ordeno la nova mida dins de les demes
    while (b > 0) and (ok2 = 1) do begin
     if Mides[b].Mida < Mides[b-1].Mida then begin
    {  auxm := Mides[b].Mida;
      auxn := Mides[b].Num;

      Mides[b].Mida := Mides[b-1].Mida;
      Mides[b].Num := Mides[b-1].Num;

      Mides[b-1].Mida := auxm;
      Mides[b-1].Num := auxn;}

      Aux := Mides[b];
      Mides[b] := Mides[b-1];
      Mides[b-1] := Aux;

     end else ok2:=0;
     b := b - 1;
    end;
  end;
end;

procedure TTablero.SetTablero(Num: Integer);
begin
 if Num < 1 then
  ActTab := 1
 else begin
  if (Num > MaxTab) then
   ActTab := MaxTab
  else
   ActTab := Num;
 end;

// Dibuixar();
end;

//api windows
procedure TTablero.Dibuixar(Pantalla: TStringGrid);
var
 a: Integer;
 LX, LY: Integer; //marges de la zona de dibuix
 DX, DY: Integer; //tamany zona de dibuix
 RX, RY: Integer; //tamany del tablero en proporcio correcta
begin
 if Ok = True then begin
  Qdla := Pantalla;

  LX:=40; //marges minims
  LY:=40;
  DX:= Qdla.Width-(2*LX);
  DY:= Qdla.Height-(2*LX);

  RX := (AmpT * DY) div AltT;
  if (RX < DX) then begin //si per altura, que es lo mes logic, ja cap, perfecte
   RY := DY;
  end else begin // si per altura no cap, sera per amplada
   RY := (AltT * DX) div AmpT;
   RX := DX;
  end;

  //calculo marges centrar
  LX := (Qdla.Width - RX) div 2;
  LY := (Qdla.Height - RY) div 2;

  // escalo. sempre abans de dibuixar
  // factors d'escala
  FX := RX / AmpT;
  FY := RY / AltT;

  // escalo tabs
  EscalarTabs();

  //uso el valor de font donat al grid
  Qdla.Canvas.Font := Qdla.Font;

  // limpiar la pantalla
  Qdla.Canvas.Brush.Color := clWhite;
  Qdla.Canvas.Rectangle(0, 0, Qdla.Width, Qdla.Height);

  //dibuixar base
  Qdla.Canvas.Brush.Color := clBlack;
  Qdla.Canvas.Brush.Style := bsDiagCross; //TBrushStyle
  Qdla.Canvas.Rectangle(LX, LY, LX+RX, LY+RY);

  Qdla.Canvas.Brush.Color := clWhite;
  Qdla.Canvas.Brush.Style := bsSolid;
  Qdla.Canvas.TextOut(LX, LY-35, IntToStr(MarT));
  Qdla.Canvas.TextOut(LX+(RX div 2), LY-35, IntToStr(AmpT));

  UFDraw.FDraw.DrawAngledText(Qdla.Canvas, LX-30, LY, 90, IntToStr(MarT));
  UFDraw.FDraw.DrawAngledText(Qdla.Canvas, LX-30, LY+(RY div 2), 90, IntToStr(AltT));
//  Qdla.Canvas.TextOut(LX-30, LY, IntToStr(MarT));
//  Qdla.Canvas.TextOut(LX-30, LY+(RY div 2), IntToStr(AltR));

  for a:=0 to High(Tabs) do
   if Tabs[a].Tab = ActTab then begin
    //calculo mides dibuix
    Tabs[a].DX := Tabs[a].l+LX;
    Tabs[a].DY := Tabs[a].t+LY;
    Tabs[a].DW := Tabs[a].l +LX+ Tabs[a].w;
    Tabs[a].DH := Tabs[a].t +LY+ Tabs[a].h;

    if Tabs[a].t = 0 then  //escric mida final en X
     Qdla.Canvas.TextOut(Tabs[a].l +LX+ Tabs[a].w, LY-18, IntToStr(Tabs[a].X + Tabs[a].Ample));

    if Tabs[a].Sel then begin
     Qdla.Canvas.Brush.Color := clSilver;
    end else begin
     Qdla.Canvas.Brush.Color := clWhite;
    end;
    Qdla.Canvas.Rectangle(Tabs[a].DX, Tabs[a].DY, Tabs[a].DW, Tabs[a].DH);

    if CTXT then begin // mostrar les mides de les peces
     Qdla.Canvas.Brush.Color := clWhite;
     Qdla.Canvas.TextOut(Tabs[a].l + (Tabs[a].w div 2)+LX, Tabs[a].t + 2+LY, IntToStr(Tabs[a].Ample));
     UFDraw.FDraw.DrawAngledText(Qdla.Canvas, Tabs[a].l + 2+LX, Tabs[a].t + (Tabs[a].h div 2)+LY, 90, IntToStr(Tabs[a].Alt));
//     Qdla.Canvas.TextOut(Tabs[a].l + 10+LX, Tabs[a].t + 10+LY, IntToStr(Tabs[a].Ample) + ' x ' + IntToStr(Tabs[a].Alt));
    end;
   end;
 end;
end;

 //opengl
procedure TTablero.Dibuixar(DX, DY: Integer);
var
 a: Integer;
 ColCount, RowCount: Integer;
 ColWidth, RowHeight : Double;
begin
 if Ok = True then begin
  // escalo. sempre abans de dibuixar
  Escalar(DX, DY); // ara ja tinc els factors d'escala posats.. peces incloses

  {
  // quadricula
  ColCount := (AmpT div 500) + 1;
  ColWidth := 500 * FX;

  RowCount := (AltT div 500) + 1;
  RowHeight := 500 * FY;

  for a:= 0 to ColCount do
   UglContext.Linia(ColWidth * a, 0, -0.01, ColWidth * a, YP, -0.01, 1.0, 0.0, 0.0);
  for a:= 0 to RowCount do
   UglContext.Linia(0, RowHeight * a, -0.01, XP, RowHeight * a, -0.01, 1.0, 0.0, 0.0);
 // }

 //textes mids
 a := LTab.FindValue(IntToStr(IDTab));
 UglContext.MostrarText(DX/2, -6, -0.01, 0.7, IntToStr(LTab.GetAlt(a)));
 UglContext.MostrarText(-10, DY/2, -0.01, 0.7, IntToStr(LTab.GetAmple(a)));

  for a:=0 to High(Tabs) do
   if Tabs[a].Tab = ActTab then begin
{    case (a mod 6) of
     0: UglContext.Quadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 1.0, 0.0, 0.0);
     1: UglContext.Quadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 0.0, 1.0, 0.0);
     2: UglContext.Quadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 0.0, 0.0, 1.0);
     3: UglContext.Quadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 0.0, 1.0, 1.0);
     4: UglContext.Quadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 1.0, 1.0, 0.0);
     5: UglContext.Quadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 1.0, 0.0, 1.0);
    end; //}

//    UglContext.Quadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 1.0, 1.0, 1.0);
    UglContext.LiniesQuadrat(Tabs[a].fl, Tabs[a].ft, 0.0, Tabs[a].fw, Tabs[a].fh, 0, 0, 1.0);
    if ULog.CTXT then begin // mostrar les mides de les peces
     UglContext.MostrarText(Tabs[a].fl+(Tabs[a].fw/2), Tabs[a].ft+(Tabs[a].fh/2), -0.01, 0.7, IntToStr(Tabs[a].Ample) + ' x ' + IntToStr(Tabs[a].Alt));
    end;
   end;
 end;
end;

// funcio que escala les mides de tot
procedure TTablero.Escalar(XP, YP: Integer);
begin
 if Ok = True then begin
  // factors d'escala
  FX := XP / AmpT;
  FY := YP / AltT;

   // area aprofitable
  UFGL.SetMarges(MarT * FX, MarT * FY);

  // escalo tabs
  EscalarTabs();
 end;
end;

// funcio que escala les peces
procedure TTablero.EscalarTabs();
var
 a: Integer;
begin
 if Ok = True then begin
  // escalo tabs
  for a:=0 to High(Tabs) do begin
   Tabs[a].l := Round(Tabs[a].X * FX);
   Tabs[a].t := Round(Tabs[a].Y * FY);
   Tabs[a].w := Round(Tabs[a].Ample * FX);
   Tabs[a].h := Round(Tabs[a].Alt * FY);

   Tabs[a].fl := Tabs[a].X * FX;
   Tabs[a].ft := Tabs[a].Y * FY;
   Tabs[a].fw := Tabs[a].Ample * FX;
   Tabs[a].fh := Tabs[a].Alt * FY;
  end;
 end;
end;

Function TTablero.XYPesaDibuix(X, Y: Integer):TPesa;
var
 a: Integer;
begin
 Result := nil;
 LimpiarSeleccio();
 a := 0;
 while a <= High(Tabs) do begin
  if Tabs[a].Tab = ActTab then
   if (Tabs[a].DX <= X) and (Tabs[a].DW >= X) then
    if (Tabs[a].DY <= Y) and (Tabs[a].DH >= Y) then begin
     Tabs[a].Selecionada(True);
     Dibuixar(Qdla);
     Result := Tabs[a];
     a := High(Tabs) + 1; //for�o sortida
    end;
  a := a + 1;
 end;
end;

procedure TTablero.LimpiarSeleccio();
var
 a: Integer;
begin
 for a:=0 to High(Tabs) do
  Tabs[a].Selecionada(False);

end;

procedure TTablero.LimpiarUs();
var
 a: Integer;
begin
 for a:=0 to High(Tabs) do
  Tabs[a].Limpiar();

 for a:=0 to High(Mides) do
  Mides[a].Cal := Mides[a].Num;
end;

function TTablero.MaxMida(): Integer;
var
 auxn, b: Integer;
begin
 // busco la mida en lo num maxim
 Result := 0;
 auxn := 0;
// PosMaxMida := 0;
// for b:=0 to High(Mides) do // de mida mes menuda a mes gran
 for b:=High(Mides) downto 0 do // de mida mes gran a mes menuda
  if auxn < Mides[b].Cal then begin
   auxn := Mides[b].Cal;
   Result := b; //Mides[b].Mida;
//   PosMaxMida := b;
  end;

  if ULog.CLOG then
   ULog.Escriurelog('busco mida maxima i trobo '+ IntToStr(Mides[Result].Mida));
end;

procedure TTablero.MarcarMida(Fun: Integer; Mida: Integer);
var
 a: Integer;
begin
 a:=0;
 while (a < Length(Mides)) do begin
  if (Mides[a].Mida = Mida) then begin
   Mides[a].Cal := Mides[a].Cal + Fun;
   a := Length(Mides);
  end;
  a := a+1;
 end;
end;

procedure TTablero.MarcarPesa(Fun: boolean; Pesa: Integer);
begin
 Tabs[Pesa].Us := Fun;

 if Fun = True then begin
  MarcarMida(-1, Tabs[Pesa].Ample);
  MarcarMida(-1, Tabs[Pesa].Alt);

  if ULog.CLOG then
   ULog.Escriurelog('marco pesa en us '+ IntToStr(Pesa));

 end else begin
  MarcarMida(1, Tabs[Pesa].Ample);
  MarcarMida(1, Tabs[Pesa].Alt);

  if ULog.CLOG then
   ULog.Escriurelog('marco pesa sense us '+ IntToStr(Pesa));
 end;
end;

function TTablero.BuscarPesa(Fun: String; Pos: Integer; var Y: Integer): Integer;
var
 a, b: Integer;
begin
 Result := -1;
 Y := 0;

if (Pos >= 0) and (Pos <= High(Mides)) then begin
                                             // ja estan ordenades
 if Fun = 'Des' then begin
  b := High(Mides[Pos].Tabs);
  while (b >= 0 {Length(Mides[Pos].Tabs)}) and (Result = -1) do begin
   a := Mides[Pos].Tabs[b];

   if Tabs[a].us = False then begin
    if Tabs[a].Ample = Mides[Pos].Mida then
     if Tabs[a].Alt > Y then begin
      Y := Tabs[a].Alt;
      Result := a;
     end; // end if ample

    if Tabs[a].Alt = Mides[Pos].Mida then
     if Tabs[a].Ample > Y then begin
      Y := Tabs[a].Ample;
      Result := a;
     end; // end if ample
   end; // end if us
   b := b - 1;
  end; // end while}
 end; // end if desc

 if Fun = 'Asc' then begin
  b := 0;
  while (b < Length(Mides[Pos].Tabs)) and (Result = -1) do begin
   a := Mides[Pos].Tabs[b];

   if Tabs[a].us = False then begin
    if Tabs[a].Ample = Mides[Pos].Mida then
     if Tabs[a].Alt > Y then begin
      Y := Tabs[a].Alt;
      Result := a;
     end; // end if ample

    if Tabs[a].Alt = Mides[Pos].Mida then
     if Tabs[a].Ample > Y then begin
      Y := Tabs[a].Ample;
      Result := a;
     end; // end if ample
   end; // end if us
   b := b + 1;
  end; // end while}
 end; // end if asc


 if ULog.CLOG then
  ULog.Escriurelog('busco pesa en funcio '+Fun+' i mida '+ IntToStr(Mides[Pos].Mida)+' i trobo pe�a ' +IntToStr(Result));
end else
 if ULog.CLOG then
  ULog.Escriurelog('busco pesa en funcio '+Fun+' i posicio '+ IntToStr(Pos)+' i trobo pe�a ' +IntToStr(Result));

end;

function TTablero.PosarMides(AcumX, Tablero: Integer): Integer; // funcio que posa les mides a les peces una vegada se sap on van
var
 a, b, p, pesa, AcumY, X: Integer;
begin
 Result := 0;
 pesa:=0;

  AcumY := 0;
  for a:=0 to C.Count()-1 do begin
   b := 0;
   X := 0;
   p := C.Get(a, b);
   while p >= 0 do begin
    pesa := p;
    Tabs[pesa].X := AcumX + X;
    Tabs[pesa].Y := AcumY;
    Tabs[Pesa].Tab := Tablero;

    X := Tabs[pesa].Ample + FulT;
    b := b + 1;
    p := C.Get(a, b);
   end;

   if Tabs[pesa].Ample > Result then
     Result := Tabs[pesa].Ample;

   AcumY := AcumY + Tabs[pesa].Alt + FulT;
  end;
end;

//////////////////// Log ////////////////////////////////////////////

procedure TTablero.Log_EscriureDades();
begin
 WriteLn(Ftx, '');
 WriteLn(Ftx, 'Dades Tableros:');

 WriteLn(Ftx, 'Amplada Real '+IntToStr(AmpR));
 WriteLn(Ftx, 'Altura Real '+IntToStr(AltR));
 WriteLn(Ftx, 'Amplada Us '+IntToStr(AmpT));
 WriteLn(Ftx, 'Altura Us '+IntToStr(AltT));
 WriteLn(Ftx, 'Marges '+IntToStr(MarT));
 WriteLn(Ftx, 'IDTablero '+IntToStr(IDTab));
 WriteLn(Ftx, 'Amplada Fulla '+IntToStr(FulT));

 WriteLn(Ftx, '');
 Flush(Ftx);
end;

procedure TTablero.Log_EscriureMatrius();
var
 a, b: integer;
begin
{ // dades tablero
 MaxTab: Integer; // numero maxim de tableros usats}

 WriteLn(Ftx, '');
 WriteLn(Ftx, 'Llistat matrius');
 WriteLn(Ftx, IntToStr(Length(Tabs))+' Peces:');
 for a:=0 to High(Tabs) do begin
  WriteLn(Ftx, 'Pe�a '+IntToStr(a));
  WriteLn(Ftx, 'Ample: '+IntToStr(Tabs[a].Ample)+
               '| Alt: '+IntToStr(Tabs[a].Alt)+
               '| X: '+IntToStr(Tabs[a].X)+
               '| Y: '+IntToStr(Tabs[a].Y)+
               '| tablero: '+IntToStr(Tabs[a].Tab)+
               '| YF: '+IntToStr(Tabs[a].Y + Tabs[a].Alt)+
               '| Sobrant: '+IntToStr(AltT - (Tabs[a].Y + Tabs[a].Alt)));
 end;
 WriteLn(Ftx, '');

 WriteLn(Ftx, 'Llistat mides');
 WriteLn(Ftx, IntToStr(Length(Mides))+' Mides:');
 for a:=0 to High(Mides) do begin
  WriteLn(Ftx, 'Mida '+IntToStr(a));
  WriteLn(Ftx, 'Mida: '+IntToStr(Mides[a].Mida)+
               '| Num: '+IntToStr(Mides[a].Num)+
               '| Peces:');
  for b:=0 to High(Mides[a].Tabs) do
   Write(Ftx, IntToStr(Mides[a].Tabs[b])+'| ');

  WriteLn(Ftx, '');
 end;
 WriteLn(Ftx, '');

 Flush(Ftx);
end;


//////////////////////////////////////////////////////////////////////////////////////
/////////////// TMides
///////////////////////////////////////////////////////////////////////////////////////

{ TMides = class
  private
   Mides: array of RMides;

  public
   Procedure AddMida(Pesa: TPesa);
 end;}

Constructor TMides.Create();
begin
 Limpiar();
end;

Destructor TMides.Destroy();
begin
 Limpiar();
end;

Procedure TMides.Limpiar();
begin

end;

procedure TMides.PosarMida(Mida, IDPesa: Integer);
var
 b, ok, ok2: Integer;
 auxm, auxn: Integer;
 m1, m2: Integer;
 Aux: RMides;
begin
  b:=0;
  ok := 0;
  while (b < Length(Mides)) and (ok < 1) do begin
   if Mides[b].Mida = Mida then begin
    Mides[b].Num := Mides[b].Num + 1;

    SetLength(Mides[b].Tabs, Length(Mides[b].Tabs) + 1);
    auxn := High(Mides[b].Tabs);
    Mides[b].Tabs[auxn] := IDPesa;

    ok2 := 1;
    while (auxn > 0) and (ok2 = 1) do begin
     if Tabs[Mides[b].Tabs[auxn]].Ample = mida then m1 := Tabs[Mides[b].Tabs[auxn]].Alt
     else m1 := Tabs[Mides[b].Tabs[auxn]].Ample;

     if Tabs[Mides[b].Tabs[auxn-1]].Ample = mida then m2 := Tabs[Mides[b].Tabs[auxn-1]].Alt
     else m2 := Tabs[Mides[b].Tabs[auxn-1]].Ample;

     if m1  < m2 then begin
      auxm := Mides[b].Tabs[auxn];
      Mides[b].Tabs[auxn] := Mides[b].Tabs[auxn-1];
      Mides[b].Tabs[auxn-1] := auxm;
     end else ok2:=0;

     auxn := auxn - 1;
    end;

    ok := ok + 1;
   end;

   b:=b+1;
  end;

  if ok = 0 then begin // no te mida
   SetLength(Mides, Length(Mides)+1);
   b:=High(Mides);

   Mides[b].Mida := Mida;
   Mides[b].Num := 1;
   SetLength(Mides[b].Tabs, 1);
   auxn := High(Mides[b].Tabs);
   Mides[b].Tabs[auxn] := IDPesa;

   ok2 := 1;     // ordeno la nova mida dins de les demes
    while (b > 0) and (ok2 = 1) do begin
     if Mides[b].Mida < Mides[b-1].Mida then begin
      Aux := Mides[b];
      Mides[b] := Mides[b-1];
      Mides[b-1] := Aux;

     end else ok2:=0;
     b := b - 1;
    end;
  end;
end;

Procedure TMides.AddMida(Pesa: TPesa);
begin
 PosarMida(Pesa.Ample, Pesa.ID);
 PosarMida(Pesa.Alt, Pesa.ID);
end;

//////////////////////////////////////////////////////////////////////////////////////
/////////////// TCalc
///////////////////////////////////////////////////////////////////////////////////////

constructor TCalc.Create(MidaX, MidaY: Integer);
begin
 Clear(MidaX, MidaY);
end;

destructor TCalc.Destroy();
var
 a: Integer;
begin
 for a:=0 to Length(Num)-1 do
  SetLength(Num[a].Tab, 0);

 SetLength(Num, 0);

 inherited Destroy();
end;

procedure TCalc.Clear();
begin
 Clear(Mida, Max);
end;

Procedure TCalc.Clear(MidaX, MidaY: Integer);
var
 a: Integer;
begin
 for a:=0 to High(Num) do
  SetLength(Num[a].Tab, 0);

 SetLength(Num, 0);

 Mida := MidaX;  // mida de la columa
 Max := MidaY; // altura total de la columna
 SobraSup := MidaY; // sobrant en distancia
 SobraTot := MidaX * MidaY; // sobrant en area
end;

// calcula, per a cada pe�a, el total sobrant que quedaria si la tragues
procedure TCalc.ProcesObj();
var
 a, b: Integer;
begin
 if ULog.CLOG then
  ULog.Escriurelog('Proces Obj:');

 for a:=0 to High(Num) do begin
  Num[a].ObjX := Mida;
  for b:=0 to High(Num[a].Tab) do
   Num[a].ObjX := Num[a].ObjX - (Num[a].Tab[b].Ample + FulT);

  Num[a].ObjY := Num[a].Tab[0].Alt {Y}+ SobraSup + FulT;

  if ULog.CLOG then
   ULog.Escriurelog('Pe�a '+IntToStr(Num[a].Tab[0].ID)+' te un sobrant de '+ IntToStr(Num[a].ObjX) +' x '+ IntToStr(Num[a].ObjY));
 end;
end;

// busca la pe�a que s'hauria de traure
// per a que al posar una altra de mida "valor", el restant fos minim
function TCalc.InObj(valor: Integer; var Sobrant: Integer): Integer;  
var
 a, Min: Integer;
begin
 Result := -1;
 Min := valor;
 Sobrant := Min;

 if ULog.CLOG then
  ULog.Escriurelog('In Obj:');

 for a:=0 to High(Num) do
  if Valor <= Num[a].ObjY then
   if (Num[a].ObjY - Valor) < Min then begin
    Min := Num[a].ObjY - Valor;
    Sobrant := Min;
    Result := Num[a].Tab[0].ID;
   end;

 if ULog.CLOG then
  ULog.Escriurelog('Busco '+IntToStr(Valor)+' i trobo pe�a '+ IntToStr(Result)+' en un sobrant de '+ IntToStr(Sobrant));
end;

function TCalc.Add(Pesa: TPesa): boolean;
var
 N, T: Integer;
begin
 Result := False;

 if (Pesa.Ample <> Mida) and (Pesa.Alt = Mida) then
  Pesa.Girar
 else begin
  if (Pesa.Alt > Pesa.Ample) then
   Pesa.Girar();

  if (Pesa.Ample > Mida) then
   Pesa.Girar();
 end;

 if Pesa.Alt <= SobraSup then begin
  // busco si hi ha lloc al costat de la ultima pe�a posada
  if (Length(Num) > 0) and (Num[High(Num)].ObjX >= Pesa.Ample) then begin
   // pe�a al costat

   N := High(Num);
   SetLength(Num[N].Tab, Length(Num[N].Tab)+1);
   T := High(Num[N].Tab);

   Num[N].Tab[T] := Pesa;

   // ordenar peces ???

//   SobraSup := SobraSup - Tabs[Pesa].Alt - FulT;
   SobraTot := SobraTot - (Pesa.Ample * Pesa.Alt);
  end else begin
   // pe�a baix
   SetLength(Num, Length(Num)+1);
   N := High(Num);
   SetLength(Num[N].Tab, Length(Num[N].Tab)+1);
   T := High(Num[N].Tab);

   Num[N].Tab[T] := Pesa;

   SobraSup := SobraSup - Pesa.Alt - FulT;
   SobraTot := SobraTot - (Pesa.Ample * Pesa.Alt);

  end;


  // marco pe�a en us
//  MarcarPesa(True, Pesa);
  Pesa.EnUs();

  ProcesObj();

  Result := True;
 end;
end;

function TCalc.Del(Pesa: TPesa): Boolean;
var
 a, b, pos: integer;
 ok: Boolean;
begin
 ok := False;
 for a:=0 to High(Num) do
  if (Num[a].Tab[0].ID = Pesa.ID) then begin
   pos := a;
   ok := True;
  end;

 if ok = True then begin
  for a:=pos to High(Num)-1 do
   Num[a] := Num[a+1];

  SetLength(Num, Length(Num)-1);

  // llevo l'us
//  MarcarPesa(False, Pesa);

  SobraSup := SobraSup + Pesa.Alt + FulT;
  SobraTot := SobraTot + (Pesa.Ample * Pesa.Alt);
 end;

 ProcesObj();

 Result := ok;
end;

function TCalc.Count(): Integer;
begin
 Result := Length(Num);
end;

function TCalc.Get(Pos, Col: Integer): Integer;
begin
 Result := -1;
 if (Pos >= 0) and (Pos <= High(Num)) then
  if (Col >= 0) and (Col <= High(Num[Pos].Tab)) then
   Result := Num[Pos].Tab[Col].ID;
end;

/////////////////////////////////////////////////////////////////////////////////////
////////// TPesa
///////////////////////////////////////////////////////////////////////////////////

Constructor TPesa.Create(Amplada, Altura, Frontal, Dreta, Esquerra, Detras, Girar, Pos: Integer);
begin
 AmpleTot := Amplada;
 AltTot := Altura;
 XF := Frontal; // distancia xapat frontal
 XD := Dreta; // dreta
 XT := Detras; // Trasera
 XE := Esquerra; // esquerra

 Gir := True;
 if Girar = 0 then Gir := False;

 ID := Pos;
 Us := False;
 Sel := False;

 // calculs
 Ample := AmpleTot - (XD + XE);
 Alt := AltTot - (XF + XT);

 X := -1;
 Y := -1;

 // dades escalades
 t := -1;
 l := -1;
 w := -1;
 h := -1;

 // dades escalades
 ft := -1;
 fl := -1;
 fw := -1;
 fh := -1;

 DX := 0;
 DY := 0;
 DW := 0;
 DH := 0;

 Tab := -1;
end;

Destructor TPesa.Destroy();
begin
 inherited Destroy();
end;

procedure TPesa.Girar();
var
 aux: Integer;
begin
 if Gir then begin
  aux := Ample;
  Ample := Alt;
  Alt := aux;

  if ULog.CLOG then
   ULog.Escriurelog('giro pesa '+ IntToStr(ID));
 end else begin
  if ULog.CLOG then
   ULog.Escriurelog('no puc girar pesa '+ IntToStr(ID));
 end;
end;

Procedure TPesa.Limpiar();
begin
 us := False;
 Sel := False;
 X := -1;
 Y := -1;
end;

Procedure TPesa.EnUs();
begin
 us := True;
end;

Procedure TPesa.Selecionada(Fun: Boolean);
begin
 Sel := Fun;
end;


///////////////////////////////////////////////////////////////////////////////
//////// Calcular
///////////////////////////////////////////////////////////////////////////////

procedure TTablero.Calcular();
var
 a: Integer;
 pesa, X, Y: Integer;
 AcumX, AcumY, Tablero: Integer;
 PosMaxMida, NumPeces: Integer;

 Ple: Boolean;
begin
// if Ok = True then begin
  LimpiarUs();

  if ULog.CLOG then begin
   ULog.EscriureSeparador();
   Log_EscriureMatrius();
  end;

  AcumX := 0;
  AcumY := 0;
  Tablero := 1;
  MaxTab := Tablero;
  X := 0;

  NumPeces := Length(Tabs);

  if NumPeces = 1 then begin // una de sola. no problem
   Tabs[0].X := 0;
   Tabs[0].Y := 0;
   Tabs[0].Tab := Tablero;

   if Tabs[0].Ample > Tabs[0].Alt then
    Tabs[0].Girar();

  end else begin // dos o mes
   PosMaxMida := MaxMida();
   if Mides[PosMaxMida].Num = 1 then begin // totes les peces tenen mides diferents
    a := High(Mides);

    while (NumPeces > 0) do begin
     while (AcumX < AmpT) and (NumPeces > 0) do begin
      while (AcumY < AltT) and (NumPeces > 0) do begin
       pesa := BuscarPesa('Des', a, Y);
       while (pesa = -1) and (a>= 0) do begin
        a := a - 1;
        pesa := BuscarPesa('Des', a, Y);
       end;

       if pesa > -1 then begin // tinc pesa
        if Tabs[pesa].Ample <> Mides[a].Mida then
         Tabs[pesa].Girar();

        if (AcumY + Tabs[Pesa].Alt) <= AltT then
        if (AcumX + Tabs[Pesa].Ample) <= AmpT then begin

         Tabs[pesa].X := AcumX;
         Tabs[pesa].Y := AcumY;
         Tabs[Pesa].Tab := Tablero;

         MarcarPesa(True, pesa);

         NumPeces := NumPeces - 1;
        end;

        if Tabs[pesa].Ample > X then
         X := Tabs[pesa].Ample;

        AcumY := AcumY + Y + FulT;
       end;
      end; // end while Y

      AcumX := AcumX + X + FulT;
      X := 0;
      AcumY := 0;
     end; // While X

     AcumX := 0;
     AcumY := 0;
     MaxTab := Tablero;
     Tablero := Tablero + 1;
    end; // end while num peces

   end else begin // hi ha varies peces en la mateixa mida
    C := TCalc.Create(Mides[PosMaxMida].Mida, AltT);

    while (NumPeces > 0) do begin
     while (AcumX < AmpT) and (NumPeces > 0) do begin
      // omplir calc en les mides basiques mes probables
      C.Clear(Mides[PosMaxMida].Mida, AltT);

      Ple := True;
      while (Ple = True) and (NumPeces > 0) and (PosMaxMida >= 0) do begin
       pesa := BuscarPesa('Des', PosMaxMida, Y);
       while (pesa = -1) and (PosMaxMida >= 0) do begin
        //PosMaxMida := MaxMida();
        PosMaxMida := PosMaxMida - 1;
        pesa := BuscarPesa('Des', PosMaxMida, Y);
       end;

       if pesa > -1 then begin// tinc pesa
        if Tabs[pesa].Ample < (AmpT - AcumX) then begin
         Ple := C.Add(Tabs[pesa]);
         if Ple then
          NumPeces := NumPeces - 1;
        end else begin
         Ple := False;
        end;
       end;
      end; // end while peces basiques

// intento omplir en los de la mateixa mida, pero mes menudes
      Ple := True;
      while (Ple = True) and (NumPeces > 0) and (PosMaxMida >= 0) do begin
       pesa := BuscarPesa('Asc', PosMaxMida, Y);
       while (pesa = -1) and (PosMaxMida >= 0) do begin
        //PosMaxMida := MaxMida();
        PosMaxMida := PosMaxMida - 1;
        pesa := BuscarPesa('Asc', PosMaxMida, Y);
       end;

       if pesa > -1 then begin// tinc pesa
        if Tabs[pesa].Ample < (AmpT - AcumX) then begin
         Ple := C.Add(Tabs[pesa]);
         if Ple then
          NumPeces := NumPeces - 1;
        end else begin
         Ple := False;
        end;
       end;
      end; // end while peces basiques


      // aqui poden pasar 3 coses
      //a) s'han posat totes les peces -> posar mides
      if NumPeces = 0 then begin
       X := PosarMides(AcumX, Tablero);

      end else
      // b) s'ha omplert la fila, pero hi ha sobrant -> buscar mes combinacions
       if C.SobraSup > 0 then begin
         // 1- determinar objectius.
         //ja estan posats. se calculen sols al posar les peces

         // 2- buscar dins la mateixa mida
         // 3- buscar en altres mides

         // posar mides
         X := PosarMides(AcumX, Tablero);

        end else begin
      // c) s'ha omplert la fila i no hi ha sobrant -> perfecte. psoar mides i marcar
         X := PosarMides(AcumX, Tablero);

        end; // end if C.SobraSup

       PosMaxMida := MaxMida();
        
       AcumX := AcumX + X + FulT;
 //      X := 0;
 //      AcumY := 0;
     end; // end while AcumX

     AcumX := 0;
//     AcumY := 0;
     MaxTab := Tablero;
     Tablero := Tablero + 1;
    end; // end while elements

   end;// end if mides.num = 1
  end;
// end; // end if

if ULog.CLOG then
 Log_EscriureMatrius();
end;

end.
