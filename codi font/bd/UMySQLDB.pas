unit UMySQLDB;

interface

uses UBaseBD, mysql;

type

 TMySQLQuery = class(TBaseQuery)
  private
    Res: PMYSQL_RES;
    ROW: PMYSQL_ROW;
    Field: PMYSQL_FIELD;

    LastID: my_ulonglong;

  public
   Constructor Create(Response: PMYSQL_RES);
   Destructor Destroy(); override;
   procedure Next();
 end;

TMySQLDB = class(TBaseDB)
 private
  LibHandle: PMYSQL;
  mySQL_Res: PMYSQL_RES;

  LastID: my_ulonglong;

  MQ: TMySQLQuery;

  function dbExists(DBName: String): Boolean;
  Procedure TestDB();

  // function list all databases in servewr
  function DBS(): PMYSQL_RES;
  // function list all tables in database
  function Tables(): PMYSQL_RES;
  // funcio per a fer queries directes al mysql
  function ResQuery(SQL: String): PMYSQL_RES;

 public
  Constructor Create(Config: String); overload;
  Constructor Create(DbFile, Host, User, Pass: String; Port: Integer); overload;
  Destructor Destroy(); override;

  procedure ModQuery(SQL: String);
  function Query(SQL: String): TMySQLQuery;

  property NewID: my_ulonglong read LastID;

  // funcions de gestio automatica
   procedure ChangeObj(ID, FillSQL: String);
   procedure UpdateObj(ID: String);

   procedure Fill();
   procedure Show();

   function Insert(): Integer;
   procedure Update(Key: String); overload;
   procedure Update(); overload;
   procedure Delete(Key: String); overload;
//   procedure Delete(); overload;
end;

implementation

uses SysUtils, IniFiles;

////////////////////////////////////////////////////////////////////////
//////////// TMySQLDB
///////////////////////////////////////////////////////////////////////

Constructor TMySQLDB.Create(Config: String);
var
  IniFile:    TIniFile;
  host, user, pass, DbFile: String;
  port: Integer;
begin
 SetEncDecKey();

 IniFile := TIniFile.Create(config);
 host := IniFile.ReadString('MySQL', 'Host', '127.0.0.1');
 user := IniFile.ReadString('MySQL', 'User', 'root');
 pass := DecStr(IniFile.ReadString('MySQL', 'Pass', ''));
 port := IniFile.ReadInteger('MySQL', 'Port', 7188);
 DbFile := IniFile.ReadString('MySQL', 'DB', 'db.db');
 IniFile.Free;
 Create(DbFile, host, user, pass, port);
end;

Constructor TMySQLDB.Create(DbFile, Host, User, Pass: String; Port: Integer);
begin
 inherited Create(DbFile);

 MQ := nil;

 // carrego la llibreria
 libmysql_fast_load(nil);

 // conecto en lo server
// seguretat
 if mySQL_Res<>nil then mysql_free_result(mySQL_Res);
 mySQL_Res := nil;
 if LibHandle<>nil then begin
  mysql_close(LibHandle);
  LibHandle := nil;
 end;

 LibHandle := mysql_init(nil);
 if LibHandle=nil then raise Exception.Create('mysql_init failed');
 if (mysql_real_connect(LibHandle,
                        PAnsiChar(AnsiString(host)),
                        PAnsiChar(AnsiString(user)),
                        PAnsiChar(AnsiString(pass)),
                        nil, // db
                        //0,   // port x defecte
                        port, // port
                        nil, // unix socket
                        0)=nil) // client flag
 then raise Exception.Create(mysql_error(LibHandle));

 TestDB(); 
end;

Destructor TMySQLDB.Destroy();
begin
 if MQ <> nil then begin
  MQ.Destroy;
  MQ := nil;
 end;

 // descarrego la llibreria
  if mySQL_Res<>nil
  then
    mysql_free_result(mySQL_Res);
  if libmysql_status=LIBMYSQL_READY
  then
    mysql_close(LibHandle);
  libmysql_free;

 inherited Destroy();
end;

Procedure TMySQLDB.TestDB();
var
 Sql: String;
 i, j: Integer;

 Res: PMYSQL_RES;
 ROW: PMYSQL_ROW;
begin
 if AllOk then begin
  // 1- comrpobo si la base de dades esta creada
  if Not(dbExists(DBName)) then begin
   Sql := 'CREATE DATABASE '+DBName;
   ModQuery(Sql);
  end;
  // la marco com a la seleccionada
  ModQuery('USE '+DBName);

  // bucle totes les taules
  i := 0;
  while i < Length(Taules) do begin
   // 2- comrpobo si la taula esta creada
   if mysql_num_rows(ResQuery('SHOW TABLES LIKE "'+ Taules[i].Nom +'"')) = 0 then begin
    // si no esta creada, la creo
    Sql := 'CREATE TABLE '+ Taules[i].Nom +' (';
    j := 0;
    while j < Length(Taules[i].Camps) do begin
	   if Taules[i].Camps[j].Tipo = 'CLAU' then begin // la clau
  	  Sql := Sql + Taules[i].Camps[j].Nom  +' INT NOT NULL AUTO_INCREMENT, ';
     end else begin
      if Taules[i].Camps[j].Tipo = 'SUBTAULA' then begin // subtaula
       Sql := Sql + Taules[i].Camps[j].Nom  +' INT, ';
      end else begin // altres tipos
       Sql := Sql + Taules[i].Camps[j].Nom  +' '+ Taules[i].Camps[j].Tipo +', ';
      end; // end if subtaula
     end; // enf if clau

     j := j + 1;
    end; // end while camps
    Sql := Sql + 'PRIMARY KEY('+ Taules[i].Clau +'));';
    ModQuery(Sql); // la creo
   end else begin // la taula ja existeix. comprobo tots els camps
    j := 0;
    while j < Length(Taules[i].Camps) do begin
     Sql := 'SHOW COLUMNS FROM '+ Taules[i].Nom +' WHERE Field = "'+ Taules[i].Camps[j].Nom +'"';
     Res := ResQuery(Sql);
     if mysql_num_rows(Res) = 0 then begin // el camp no existeix
      // busco el tipo de camp
      if Taules[i].Camps[j].Tipo = 'CLAU' then begin // la clau
    	  Sql := 'ALTER TABLE '+ Taules[i].Nom +' ADD '+ Taules[i].Camps[j].Nom +' INT NOT NULL AUTO_INCREMENT';
      end else begin
       if Taules[i].Camps[j].Tipo = 'SUBTAULA' then begin // subtaula
        Sql := 'ALTER TABLE '+ Taules[i].Nom +' ADD '+ Taules[i].Camps[j].Nom +' INT';
       end else begin // altres tipos
        Sql := 'ALTER TABLE '+ Taules[i].Nom +' ADD '+ Taules[i].Camps[j].Nom +' '+ Taules[i].Camps[j].Tipo;
       end; // end if subtaula
      end; // enf if clau
      ModQuery(Sql); // modifico
     end else begin // el camp si que existeix. comprobo el tipo
      ROW := mysql_fetch_row(Res);
      if ROW^[1] <> Taules[i].Camps[j].Tipo then begin// tinc que canviar el tipo
       // busco el tipo de camp
       if Taules[i].Camps[j].Tipo = 'CLAU' then begin // la clau
     	  Sql := 'ALTER TABLE '+ Taules[i].Nom +' CHANGE '+ Taules[i].Camps[j].Nom +' '+ Taules[i].Camps[j].Nom +' INT NOT NULL AUTO_INCREMENT';
       end else begin
        if Taules[i].Camps[j].Tipo = 'SUBTAULA' then begin // subtaula
         Sql := 'ALTER TABLE '+ Taules[i].Nom +' CHANGE '+ Taules[i].Camps[j].Nom +' '+ Taules[i].Camps[j].Nom +' INT';
        end else begin // altres tipos
         Sql := 'ALTER TABLE '+ Taules[i].Nom +' CHANGE '+ Taules[i].Camps[j].Nom +' '+ Taules[i].Camps[j].Nom +' '+ Taules[i].Camps[j].Tipo;
        end; // end if subtaula
       end; // enf if clau
       ModQuery(Sql); // modifico

      end; // end if canvi de tipo
     end; // end if
     j := j + 1;
    end; // end while camps

   end; // end if crear taula

   i := i + 1;
  end; // end while

 end; // end if
end;

function TMySQLDB.dbExists(DBName: String): Boolean;
var
 MyResult: Integer;
begin
 Result := False;

 MyResult := mysql_select_db(LibHandle, PAnsiChar(AnsiString(DBName)));
 if MyResult<>0 then begin//raise Exception.Create(mysql_error(LibHandle));
  Result := False;
 end else begin
  Result := True;
 end;
end;

function TMySQLDB.DBS(): PMYSQL_RES;
begin
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);
  mySQL_Res := nil;
  mySQL_Res := mysql_list_dbs(LibHandle, nil);
  if mySQL_Res=nil then raise Exception.Create(mysql_error(LibHandle));

  //Get Data
 // mySQL_Res := mysql_store_result(LibHandle);
  Result := mySQL_Res;
end;

function TMySQLDB.Tables(): PMYSQL_RES;
begin
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);
  mySQL_Res := nil;
  mySQL_Res := mysql_list_tables(LibHandle, nil);
  if mySQL_Res=nil then raise Exception.Create(mysql_error(LibHandle));

  //Get Data
 // mySQL_Res := mysql_store_result(LibHandle);
  Result := mySQL_Res;
end;

procedure TMySQLDB.ModQuery(SQL: String);
begin
 SQL := PChar(AnsiString(SQL));
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);
  mySQL_Res := nil;
  if mysql_real_query(LibHandle, PAnsiChar(SQL), Length(SQL)) <> 0 then raise Exception.Create(mysql_error(LibHandle));
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);

  LastID := mysql_insert_id(LibHandle);
end;

function TMySQLDB.Query(SQL: String): TMySQLQuery;
begin
 SQL := PChar(AnsiString(SQL));
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);
  mySQL_Res := nil;
  if mysql_real_query(LibHandle, PAnsiChar(SQL), Length(SQL)) <> 0 then raise Exception.Create(mysql_error(LibHandle));
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);

  Result := TMySQLQuery.Create(mysql_store_result(LibHandle));
  LastID := mysql_insert_id(LibHandle);
end;

function TMySQLDB.ResQuery(SQL: String): PMYSQL_RES;
begin
 SQL := PChar(AnsiString(SQL));
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);
  mySQL_Res := nil;
  if mysql_real_query(LibHandle, PAnsiChar(SQL), Length(SQL)) <> 0 then raise Exception.Create(mysql_error(LibHandle));
  if mySQL_Res <> nil then mysql_free_result(mySQL_Res);

  Result := mysql_store_result(LibHandle);
  LastID := mysql_insert_id(LibHandle);
end;

procedure TMySQLDB.ChangeObj(ID, FillSQL: String);
var
 Pos: Integer;
 Q: TMySQLQuery;
begin
 Pos := FindID(ID);
 if Pos > -1 then begin
  if FillSQL <> '' then Fields[Pos].FillSql := FillSQL;

  if Fields[Pos].FillSql <> '' then begin
   Q := Query(Fields[Pos].FillSql);

   FillObj(Q, Pos);

   Q.Destroy();
   Q := nil;
  end else begin
   raise EMyException.Create('NO hi ha SQL per omplir l�objecte '+ID);
  end;
 end else begin
  raise EMyException.Create('El ID '+ID+' NO esta registrat');
 end; // end if pos > -1
end;

procedure TMySQLDB.UpdateObj(ID: String);
begin
 ChangeObj(ID, '');
end;

procedure TMySQLDB.Fill();
var
 a: Integer;
 Q: TMySQLQuery;
begin
 for a:=0 to Length(Fields)-1 do
  if Fields[a].FillSql <> '' then begin
   Q := Query(Fields[a].FillSql);

   FillObj(Q, a);

   Q.Destroy();
   Q := nil;   
  end;
end;

procedure TMySQLDB.Show();
var
 a: Integer;
 SQL: String;
begin
 SQL :='SELECT ';
 for a:=0 to Length(Fields)-1 do
  if a = 0 then begin
   SQL := SQL + GetFieldTable(a) + '.' + GetFieldField(a);
  end else
   if GetFieldField(a) <> '' then
    SQL := SQL + ', ' + GetFieldTable(a) + '.' + GetFieldField(a);

 if Table = '' then
  SQL := SQL + ' FROM ' + GetFieldTable(0)
 else
  SQL := SQL + ' FROM ' + Table;

 if Filter <> '' then
  SQL := SQL + ' WHERE ' + Filter;

 if Order <> '' then
  SQL := SQL + ' ORDER BY ' + Order;

 if MQ <> nil then begin
   MQ.Destroy; // limpio si ja he usat abans. sino, problemes
   MQ := nil;
 end;

 MQ := Query(SQL);

 Write(MQ, True);
end;

function TMySQLDB.Insert(): Integer;
var
 SQL: String;
 a, b: Integer;
begin
 Result := 0;
 b := 0;
 for a:=0 to Length(Fields)-1 do
  if Fields[a].Insertable then begin
   if b = 0 then begin
    if Table <> '' then
     SQL := 'INSERT INTO ' + Table + ' ('
    else
     SQL := 'INSERT INTO ' + GetFieldTable(a) + ' (';

    SQL := SQL + GetFieldField(a);
   end else SQL := SQL + ', ' + GetFieldField(a);
   b := b + 1;
  end;

 if b = 0 then
  raise EMyException.Create('No hi ha cap camp insertable')
 else begin
  SQL := SQL + ') VALUES (';

  b := 0;
  for a:=0 to Length(Fields)-1 do
   if Fields[a].Insertable then begin
    if b = 0 then begin
     SQL := SQL + QuoteString(GetFieldValue(a));
    end else begin
     SQL := SQL + ', ' + QuoteString(GetFieldValue(a));
    end;
    b := b + 1;
   end;

  SQL := SQL + ');';

  ModQuery(SQL);

  Value := IntToStr(LastID);
  Result := LastID;
 end; // fi end b=0
end;

procedure TMySQLDB.Update(Key: String);
var
 SQL: String;
 a, b: Integer;
begin
 b := 0;
 for a:=0 to Length(Fields)-1 do
  if Fields[a].Updatable then begin
    if b = 0 then begin
     if Table <> '' then
      SQL := 'UPDATE ' + Table + ' SET '
     else
      SQL := 'UPDATE ' + GetFieldTable(a) + ' SET ';

      SQL := SQL + GetFieldField(a) +'='+ QuoteString(GetFieldValue(a));
    end else begin
      SQL := SQL + ', ' + GetFieldField(a) +'='+ QuoteString(GetFieldValue(a));
    end;
   b := b + 1;
  end;

 if b = 0 then
  raise EMyException.Create('No hi ha cap camp modificable')
 else begin
  SQL := SQL + 'WHERE '+Key+';';

  ModQuery(SQL);
 end; // fi end b=0
end;

procedure TMySQLDB.Update();
begin
 if (Key <> '') then begin
  if (Value <> '') then
   Update(Key+'="'+Value+'"')
  else raise EMyException.Create('No hi ha cap valor del camp clau');
 end else raise EMyException.Create('No hi ha indicat cap camp clau');
end;

procedure TMySQLDB.Delete(Key: String);
var
 SQL: String;
begin
 if Table <> '' then
  SQL := 'DELETE FROM '+Table+' WHERE '+Key+';'
 else
  SQL := 'DELETE FROM '+GetFieldTable(0)+' WHERE '+Key+';';

 ModQuery(SQL);
end;




///////////////////////////////////////////////////////////
///////////////  class TMySQLQuery
///////////////////////////////////////////////////////////

Constructor TMySQLQuery.Create(Response: PMYSQL_RES);
var
 i: Integer;
begin
 inherited Create();

 Res := Response;
 NumRow := mysql_num_rows(Res);
 NumCol := mysql_num_fields(Res);

 for i := 0 to NumCol - 1 do begin
   Field := mysql_fetch_field(Res);
   if Field<>nil then begin
    SetLength(ColNames, Length(ColNames)+1);
    ColNames[Length(ColNames)-1].Name := UpperCase(mysql_field_name(Field));
    ColNames[Length(ColNames)-1].Table := UpperCase(mysql_field_tablename(Field));
   end;
 end;

 if NumRow > 0 then begin
//  ROW := mysql_fetch_row(Res);
  OK := True;
  SetLength(TmpValues, NumCol);
  Next();
 end else begin
  if Res<>nil then mysql_free_result(Res); // limpio
  OK := False;
 end;
end;

Destructor TMySQLQuery.Destroy();
begin
 if OK then
  if Res<>nil then mysql_free_result(Res); // limpio

 inherited Destroy();
end;

procedure TMySQLQuery.Next();
var
 a: Integer;
begin
 if OK then begin
  ROW := mysql_fetch_row(Res);
  if ROW <> nil then
  for a:=0 to NumCol-1 do
   TmpValues[a] := ROW^[a];
 end;
end;


{
crear una base de dades: CREATE DATABASE nom;
canviar de base de dades: USE nom;
eliminar base de dades: DROP DATABASE nom
vore les taules: SHOW TABLES;
Crear una taula: CREATE TABLE nom (id INT NOT NULL AUTO_INCREMENT, camp1 VARCHAR(20), camp2 DATE, PRIMARY KEY(id));
vore camps taula: DESCRIBE nom;
buidar una taula: TRUNCATE TABLE nom;
eliminar taula: DROP TABLE nom;
insert: INSERT INTO UsedFiles (Disk, Folder, File, Type, Num) VALUES ("' + FDisk + '","' + F + '","' + FFile + '","' + FType + '", "1")
update: UPDATE UsedFiles SET Num="'+IntToStr(a)+'" WHERE ID="'+S+'"
delete: DELETE FROM Files WHERE Disk="'+NumSerie+'"
select inner join:
 SQL := 'SELECT Files.ID, Files.Folder, Files.File, Files.Disk, ';
 SQL := SQL + 'Disks.Letter, Disks.Disk, ';
 SQL := SQL + 'Music.IDFile, Music.Title, Music.Artist, Music.Album, Music.Genre, Music.Num, Music.Disk ';
 SQL := SQL + 'FROM Music INNER JOIN (Files INNER JOIN Disks ON Files.Disk = Disks.Disk) ON Music.IDFile = Files.ID ';

 Where := SQL + 'WHERE Music.Genre LIKE "%'+Str+'%" ORDER BY Music.Num';

}
end.
