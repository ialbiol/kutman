unit UBaseBD;

interface

uses SysUtils;

type

EMyException = class(Exception)
end;

TBase = class
 protected
  // codifica i descodifica un string
  EncDecKey: string;
  // codifica i descodifica un string
  function EncStr(const Str: string): string;
  function DecStr(const Str: string): string;
  function DecToPoint(S: String): String;

  Procedure SetEncDecKey();
 public
  Constructor Create();
  Destructor Destroy(); override;

end;

 TBaseQuery = class(TBase)
  protected
    NumRow, NumCol: Integer;
    ColNames: Array of Record
               Name: String;
               Table: String;
              end;

    OK: Boolean;
    TmpValues: Array of String;

    function SearchID(Col: String): Integer;
  public
   Constructor Create();
   Destructor Destroy(); override;
//   procedure Next();
   function FieldCount(): Integer;
   function Count(): Integer;

   function GetField(Col: Integer): String;

   function GetStr(Col: Integer): String; overload;
   function GetStr(Col: String): String; overload;
   function GetStrCrypt(Col: Integer): String; overload;
   function GetStrCrypt(Col: String): String; overload;
   function GetInt(Col: Integer): Integer; overload;
   function GetInt(Col: String): Integer; overload;
   function GetFloat(Col: Integer): Double; overload;
   function GetFloat(Col: String): Double; overload;
   function GetFloatD(Col: Integer): Double; overload;
   function GetFloatD(Col: String): Double; overload;

   procedure Next(); Virtual; Abstract;
 end;

TBaseDB = class(TBase)
 protected
  Taules: Array of Record
           Nom: String;
           Clau: String;
           Camps: Array of Record
             Nom: String;
             Tipo: String;
             SubTaula: String;
            end;
          end;

  Fields: Array of record  // temporals en us
               ID: String;
               Name: Integer;
               Table: Integer;
               Obj: TObject;
               Variable: Pointer;
               ObjType: String;
               FillSql: String;
               Activable: Boolean;
               Insertable: Boolean;
               Updatable: Boolean;
               Clearable: Boolean;
          end;

  NumTaules: Integer;
  AllOK: Boolean;
  DBName: String;

   Filter: String;
   Table: String;
   Order: String;
   Key: String;
   Value: String;

  function FindTable(TableName: String): Integer;
  function FindField(Table: Integer; FieldName: String): Integer;
  function GetFieldTable(Table: Integer): String;
  function GetFieldField(Field: Integer): String;
  function GetFieldValue(Field: Integer): String;

  function FindID(ID: String): Integer;
  procedure SetField(ID, FieldName, TableName, FillSQL, ObjType: String; FieldUses: Integer; ObjSource: TObject; VarAddress: Pointer);

  procedure FillObj(Q: TBaseQuery; Pos: Integer);
  procedure Write(Q: TBaseQuery; Query: Boolean);

//  procedure TestDB(); Virtual; Abstract;
 public
 
  Constructor Create(DbFile: String);
  Procedure LoadDB(FileName: String);
  Destructor Destroy(); override;

  procedure SetObject(ID, FieldName, TableName: String; FieldUses: Integer; ObjSource: TObject); overload;
  procedure SetObject(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; ObjSource: TObject); overload;

  procedure SetInteger(ID, FieldName, TableName: String; FieldUses: Integer; VarAddress: Pointer); overload;
  procedure SetInteger(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; VarAddress: Pointer); overload;
  procedure SetReal(ID, FieldName, TableName: String; FieldUses: Integer; VarAddress: Pointer); overload;
  procedure SetReal(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; VarAddress: Pointer); overload;
  procedure SetString(ID, FieldName, TableName: String; FieldUses: Integer; VarAddress: PChar); overload;
  procedure SetString(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; VarAddress: PChar); overload;

  procedure SetTable(TableName: String);
  procedure SetFilter(TableFilter: String);
  procedure SetOrder(TableOrder: String);

  procedure ShowInfo();
  procedure Activate(Fun: Boolean);
  procedure Clear();
  
end;

const
  NOTHING = 0;
  ACTIVABLE = 1;      // el camp usa el control d'activacio de l'objecte
  INSERTABLE = 2;       // el camp participa al insert a la bd
  UPDATABLE = 4;    // el camp participa al update a la bd
  CLEARABLE = 8;     // el camp participa a l'hora de limpiar-los
  ALL = 15;          // tots els permisos

implementation

//uses mysql, ShellApi, Forms, Windows, SysUtils, dialogs, IniFiles;

uses Grids, StdCtrls;

{$WARN UNSAFE_CODE OFF}

////////////////////////////////////////////////////////////////////////
//////////// TBaseDB
///////////////////////////////////////////////////////////////////////

Constructor TBaseDB.Create(DbFile: String);
begin
 inherited Create();

 SetLength(Fields, 0);
 SetLength(Taules, 0);
 NumTaules := 0;
 AllOK := True;

 DBName := '';

 Table := '';
 Filter := '';
 Order := '';
 Key := '';
 Value := '';

 LoadDB(DbFile);
end;

Destructor TBaseDB.Destroy();
var
 a: integer;
begin
 for a:=0 to Length(Taules)-1 do begin
  SetLength(Taules[a].Camps, 0);
 end;
 SetLength(Taules, 0);

 SetLength(Fields, 0);

 AllOk := False;

 inherited Destroy();
end;

procedure TBaseDB.SetTable(TableName: String);
begin
 Table := TableName;
end;

procedure TBaseDB.SetFilter(TableFilter: String);
begin
 Filter := TableFilter;
end;

procedure TBaseDB.SetOrder(TableOrder: String);
begin
 Order := TableOrder;
end;

Procedure TBaseDB.LoadDB(FileName: String);
var
 Ftx: TextFile;
 S, Taula: String;
 C: Array of String;
 a, b, z: Integer;
begin
 if FileExists(FileName) then begin
  AssignFile(Ftx, FileName);
  Reset(Ftx);

  while not(eof(Ftx)) do begin
   ReadLn(Ftx, S);
   S := Trim(S);
   if S <> '' then begin
    if (S[1] <> '/') and (S[2] <> '/') then begin // llevo els comentaris
    // explode
    SetLength(C, 1);
    b:=0;
    C[b] := '';
    for a:=1 to Length(S) do begin
     if S[a] = ' ' then begin
      b := b + 1;
      SetLength(C, Length(C)+1);
      C[b] := '';
     end else begin
      C[b] := C[b] + S[a];
     end;
    end;
    // fin exclude
    if C[0] = 'DBNAME' then begin
     DBName := C[1]; // nom de la base de dades
    end else begin
    if C[0] = 'TAULA' then begin // nova taula
     Taula := C[1];
     SetLength(Taules, Length(Taules)+1);
     Taules[NumTaules].Nom := C[1];
    end else begin // si no es taula, sera camp
     if C[0] = 'END' then begin // nova taula
      Taula := '';
      NumTaules := NumTaules + 1;
     end else begin // si no es fin taula, sera camp
      SetLength(Taules[NumTaules].Camps, Length(Taules[NumTaules].Camps) + 1);
      z := High(Taules[NumTaules].Camps);
      Taules[NumTaules].Camps[z].Nom := C[0];
      if C[1] = 'CLAU' then begin
       Taules[NumTaules].Clau := C[0]
      end;
      if Length(C) = 2 then begin // no hi ha subtaula
       Taules[NumTaules].Camps[z].Tipo := C[1];
       Taules[NumTaules].Camps[z].SubTaula := '';
      end else begin // hi ha subtaula
       Taules[NumTaules].Camps[z].Tipo := C[1];
       Taules[NumTaules].Camps[z].SubTaula := C[2];
      end;
     end;// fin if
    end; // fin if
    end;
   end;
   end;
  end;

  CloseFile(Ftx);
 end else begin
  // error, no hi ha descripcio de base de dades
  AllOK := False;
 end;
end;

function TBaseDB.FindTable(TableName: String): Integer;
var
 a: Integer;
begin
 Result := -1; // error
 a:= 0;
 while (a < Length(Taules)) and (Result = -1) do begin
  if Taules[a].Nom = TableName then Result := a;
  a := a+1;
 end;

 if Result = -1 then // error
  raise EMyException.Create('La taula '+TableName+' NO esta a la base de dades');
end;

function TBaseDB.FindField(Table: Integer; FieldName: String): Integer;
var
 a: Integer;
begin
 Result := -1; // error

 if (Table >= 0) and (Table < Length(Taules)) then begin
  a:= 0;
  while (a < Length(Taules[Table].Camps)) and (Result = -1) do begin
   if Taules[Table].Camps[a].Nom = FieldName then Result := a;
   a := a+1;
  end;

  if Result = -1 then // error
   raise EMyException.Create('El camp '+FieldName+' NO existeix a la taula '+Taules[Table].Nom);
 end else begin
  raise EMyException.Create('NO existeix la taula num '+ IntToStr(Table));
 end;
end;

function TBaseDB.GetFieldTable(Table: Integer): String;
begin
 Result := Taules[Fields[Table].Table].Nom;
end;

function TBaseDB.GetFieldField(Field: Integer): String;
begin
 Result := Taules[Fields[Field].Table].Camps[Fields[Field].Name].Nom;
end;

function TBaseDB.GetFieldValue(Field: Integer): String;
begin
 Result := '';

 if Fields[Field].ObjType = 'TEdit' then Result := (Fields[Field].Obj As TEDit).Text;
 if Fields[Field].ObjType = 'TMemo' then Result := (Fields[Field].Obj As TMemo).Text;
 if Fields[Field].ObjType = 'TComboBox' then Result := (Fields[Field].Obj As TComboBox).Items.ValueFromIndex[(Fields[Field].Obj As TComboBox).ItemIndex]; ////CBClients.Items.ValueFromIndex[CBClients.ItemIndex]

 if Fields[Field].ObjType = 'INT' then Result := IntToStr(PInteger(Fields[Field].Variable)^);
 if Fields[Field].ObjType = 'FLT' then Result := DecToPoint(FloatToStr(PCurrency(Fields[Field].Variable)^));
 if Fields[Field].ObjType = 'STR' then Result := PString(Fields[Field].Variable)^;
end;

function TBaseDB.FindID(ID: String): Integer;
var
 a: Integer;
begin
 Result := -1; // error
 a:= 0;
 while (a < Length(Fields)) and (Result = -1) do begin
  if Fields[a].ID = ID then Result := a;
  a := a+1;
 end;

 if Result = -1 then // error
  raise EMyException.Create('El ID '+ID+' NO existeix');
end;

procedure TBaseDB.SetField(ID, FieldName, TableName, FillSQL, ObjType: String; FieldUses: Integer; ObjSource: TObject; VarAddress: Pointer);
var
 PosT, PosC: Integer;
begin
 PosT := FindTable(TableName);
 if PosT > -1 then begin
  PosC := FindField(PosT, FieldName);
  if PosC > -1 then begin
   SetLength(Fields, Length(Fields)+1);

   Fields[Length(Fields)-1].ID := ID;
   Fields[Length(Fields)-1].Name := PosC;
   Fields[Length(Fields)-1].Table := PosT;
   Fields[Length(Fields)-1].Obj := ObjSource;
   Fields[Length(Fields)-1].Variable := VarAddress;
   Fields[Length(Fields)-1].FillSql := FillSQL;
   Fields[Length(Fields)-1].ObjType := ObjType;
   Fields[Length(Fields)-1].Activable := (FieldUses and ACTIVABLE) = ACTIVABLE;
   Fields[Length(Fields)-1].Insertable := (FieldUses and INSERTABLE) = INSERTABLE;
   Fields[Length(Fields)-1].Updatable := (FieldUses and UPDATABLE) = UPDATABLE;
   Fields[Length(Fields)-1].Clearable := (FieldUses and CLEARABLE) = CLEARABLE;
  end else begin
   // el camp no existeix a la taula
//   raise EMyException.Create('El camp '+FieldName+' NO existeix a la taula '+Taules[PosT].Nom);
  end;
 end else begin
  // la taula no existeix
//  raise EMyException.Create('La taula '+TableName+' NO esta a la base de dades');  
 end;
end;

procedure TBaseDB.SetInteger(ID, FieldName, TableName: String; FieldUses: Integer; VarAddress: Pointer);
begin
 SetField(ID, FieldName, TableName, '', 'INT', FieldUses, nil, VarAddress);
end;

procedure TBaseDB.SetInteger(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; VarAddress: Pointer);
begin
 SetField(ID, FieldName, TableName, FillSQL, 'INT', FieldUses, nil, VarAddress);
end;

procedure TBaseDB.SetReal(ID, FieldName, TableName: String; FieldUses: Integer; VarAddress: Pointer);
begin
 SetField(ID, FieldName, TableName, '', 'FLT', FieldUses, nil, VarAddress);
end;

procedure TBaseDB.SetReal(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; VarAddress: Pointer);
begin
 SetField(ID, FieldName, TableName, FillSQL, 'FLT', FieldUses, nil, VarAddress);
end;

procedure TBaseDB.SetString(ID, FieldName, TableName: String; FieldUses: Integer; VarAddress: PChar);
begin
 SetField(ID, FieldName, TableName, '', 'STR', FieldUses, nil, VarAddress);
end;

procedure TBaseDB.SetString(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; VarAddress: PChar);
begin
 SetField(ID, FieldName, TableName, FillSQL, 'STR', FieldUses, nil, VarAddress);
end;

procedure TBaseDB.SetObject(ID, FieldName, TableName: String; FieldUses: Integer; ObjSource: TObject);
begin
 SetField(ID, FieldName, TableName, '', ObjSource.ClassName, FieldUses, ObjSource, nil);
end;

procedure TBaseDB.SetObject(ID, FieldName, TableName, FillSQL: String; FieldUses: Integer; ObjSource: TObject);
begin
 SetField(ID, FieldName, TableName, FillSQL, ObjSource.ClassName, FieldUses, ObjSource, nil);
end;

procedure TBaseDB.FillObj(Q: TBaseQuery; Pos: Integer);
var
 b, c: Integer;
begin
//  if Fields[Pos].FillSql <> '' then begin
//   Q := ObjMysql.Query(Fields[Pos].FillSql, True);

   if Fields[Pos].ObjType = 'TComboBox' then begin
    (Fields[Pos].Obj As TComboBox).Clear;
    (Fields[Pos].Obj As TComboBox).Items.NameValueSeparator := '-';

    if Q.Count > 0 then begin
     for b:=1 to Q.Count do begin
      (Fields[Pos].Obj As TComboBox).Items.Add(Q.GetStr(0) + '-' +  Q.GetStr(1));
      Q.Next;
     end;
     (Fields[Pos].Obj As TComboBox).ItemIndex := 0;
    end;
   end;

   if Fields[Pos].ObjType = 'TStringGrid' then begin
    (Fields[Pos].Obj As TStringGrid).Rows[1].Clear;
    (Fields[Pos].Obj As TStringGrid).RowCount := 2;

    if Q.Count > 0 then begin
     for b:=1 to Q.Count do begin
      for c:=0 to Q.FieldCount-1 do
       (Fields[Pos].Obj As TStringGrid).Cells[c, b] := Q.GetStr(c);

      Q.Next;
     end;

     if Q.Count = 1 then b := 2 else b:= Q.Count;
     (Fields[Pos].Obj As TStringGrid).RowCount := b;
    end;
   end;

//   if Q <> nil then begin
//    Q.Destroy; // limpio si ja he usat abans. sino, problemes
//    Q := nil;
//   end;
//  end;
end;

procedure TBaseDB.Write(Q: TBaseQuery; Query: Boolean);
var
 a, rows: Integer;
 CC: String;
begin
 for a:=0 to Length(Fields)-1 do begin
  if Query then begin
//   if Fields[a].Name <> '' then begin
    if Fields[a].ObjType = 'TEdit' then
     (Fields[a].Obj As TEdit).Text := Q.GetStr(GetFieldField(a));

    if Fields[a].ObjType = 'TMemo' then
     (Fields[a].Obj As TMemo).Text := Q.GetStr(GetFieldField(a));

    if Fields[a].ObjType = 'TComboBox' then begin
      CC := Q.GetStr(GetFieldField(a));
      rows := 0;
      while (rows < (Fields[a].Obj As TComboBox).Items.Count) and ((Fields[a].Obj As TComboBox).Items.ValueFromIndex[rows] <> CC) do rows := rows + 1;
      if (rows < (Fields[a].Obj As TComboBox).Items.Count) then (Fields[a].Obj As TComboBox).ItemIndex := rows
      else (Fields[a].Obj As TComboBox).ItemIndex := 0;
    end;

    if Fields[a].ObjType = 'INT' then
     PInteger(Fields[a].Variable)^  := Q.GetInt(GetFieldField(a));
    if Fields[a].ObjType = 'FLT' then
     PCurrency(Fields[a].Variable)^ := Q.GetFloatD(GetFieldField(a));
    if Fields[a].ObjType = 'STR' then
     PString(Fields[a].Variable)^ := Q.GetStr(GetFieldField(a));
//   end;
  end else
  if Fields[a].Clearable then begin
   if Fields[a].ObjType = 'TEdit' then
    (Fields[a].Obj As TEdit).Text := '';

   if Fields[a].ObjType = 'TMemo' then
    (Fields[a].Obj As TMemo).Text := '';

   if Fields[a].ObjType = 'TComboBox' then
    (Fields[a].Obj As TComboBox).ItemIndex := 0;

   if Fields[a].ObjType = 'INT' then
    PInteger(Fields[a].Variable)^  := 0;
   if Fields[a].ObjType = 'FLT' then
    PCurrency(Fields[a].Variable)^ := 0;
   if Fields[a].ObjType = 'STR' then
    PString(Fields[a].Variable)^ := '';

  end;
 end;
end;

procedure TBaseDB.ShowInfo();
var
 a: Integer;
begin
 for a:=0 to Length(Fields)-1 do begin
  if Fields[a].ObjType = 'TEdit' then
   (Fields[a].Obj As TEdit).Text :=  Fields[a].Obj.ClassName +' '+ GetFieldTable(a) + ' ' + GetFieldField(a);

  if Fields[a].ObjType = 'TComboBox' then
   (Fields[a].Obj As TComboBox).Text :=  Fields[a].Obj.ClassName +' '+ GetFieldTable(a) + ' ' + GetFieldField(a);
 end;
end;

procedure TBaseDB.Activate(Fun: Boolean);
var
 a: Integer;
begin
 for a:=0 to Length(Fields)-1 do
  if Fields[a].Activable then begin
   if Fields[a].ObjType = 'TEdit' then (Fields[a].Obj As TEdit).ReadOnly := Fun;
   if Fields[a].ObjType = 'TComboBox' then (Fields[a].Obj As TComboBox).Enabled := Not(Fun);
   if Fields[a].ObjType = 'TStringGrid' then (Fields[a].Obj As TStringGrid).Enabled := Not(Fun);
  end;
end;

procedure TBaseDB.Clear();
begin
 Write(nil, False);
end;

/////////////// criptografia  ///////////////////////
/////////////////////////////////////////////////////
/////////////// TBase
///////////////////////////////////////////////////////

Constructor TBase.Create();
begin
 SetEncDecKey();
end;

Destructor TBase.Destroy();
begin
 inherited Destroy();
end;

Procedure TBase.SetEncDecKey();
begin
  // must be 65 chars, no duplicate chars, uppercases and lowercases allowed
  EncDecKey :=  'PLOK4321MIJNcrfv0987tgolpUHB#YGVTqazw@sxedFCRDXbyhnujESZmikWAQ65$';
end;

function TBase.DecToPoint(S: String): String;
var
 a: Integer;
begin
 Result := '';
// DecimalSeparator := '.';
  a:=1;
  while a <= Length(S) do begin
   if S[a] = DecimalSeparator then begin
     S[a] := '.';
     a := Length(S) + 1; // paro el bucle perque nomes pot haber un sol signe decimal
   end;
   a := a+1;
  end;
  Result := S;
end;

function TBase.EncStr(const Str: string): string;
var
  c: Byte;
  n, l: Integer;
  Count: Integer;
  d: array[0..3] of Byte;
begin
  SetLength(Result, ((Length(Str) + 2) div 3) * 4);
  l := 1;
  Count := 1;
  while (Count <= Length(Str)) do
  begin
    c := Ord(Str[Count]);
    Inc(Count);
    d[0] := (c and $FC) shr 2;
    d[1] := (c and $03) shl 4;
    if (Count <= Length(Str)) then
    begin
      c := Ord(Str[Count]);
      Inc(Count);
      d[1] := d[1] + (c and $F0) shr 4;
      d[2] := (c and $0F) shl 2;
      if (Count <= Length(Str)) then
      begin
        c := Ord(Str[Count]);
        Inc(Count);
        d[2] := d[2] + (c and $C0) shr 6;
        d[3] := (c and $3F);
      end else
        d[3] := $40;
    end else
    begin
      d[2] := $40;
      d[3] := $40;
    end;
    for n := 0 to 3 do
    begin
      Result[l] := EncDecKey[d[n] + 1];
      Inc(l);
    end;
  end;
end;

function TBase.DecStr(const Str: string): string;
var
  x, y, n, l: Integer;
  d: array[0..3] of Byte;
begin
  SetLength(Result, Length(Str));
  x := 1;
  l := 1;
  while (x < Length(Str)) do
  begin
    for n := 0 to 3 do
    begin
      if (x > Length(Str))
        then d[n] := 64
        else
        begin
          y := Pos(Str[x], EncDecKey);
          if (y < 1)
            then y := 1;
          d[n] := y - 1;
        end;
      Inc(x);
    end;
    Result[l] := Char((D[0] and $3F) shl 2 + (D[1] and $30) shr 4);
    Inc(l);
    if (d[2] <> 64) then
    begin
      Result[l] := Char((D[1] and $0F) shl 4 + (D[2] and $3C) shr 2);
      Inc(l);
      if (d[3] <> 64) then
      begin
        Result[l] := Char((D[2] and $03) shl 6 + (D[3] and $3F));
        Inc(l);
      end;
    end;
  end;
  Dec(l);
  SetLength(Result, l);
end;


///////////////////////////////////////////////////////////
///////////////  class TBaseQuery
///////////////////////////////////////////////////////////

Constructor TBaseQuery.Create();
begin
 inherited Create();
 
 OK := False;
 NumRow := 0;
 NumCol := 0;

 SetLength(ColNames, 0);
 SetLength(TmpValues, 0);
end;

Destructor TBaseQuery.Destroy();
begin
 SetLength(ColNames, 0);
 SetLength(TmpValues, 0);

 inherited Destroy();
end;

{
procedure TBaseQuery.Next();
begin
 if OK then
  ROW := mysql_fetch_row(Res);
end;
}

function TBaseQuery.FieldCount(): Integer;
begin
 Result := NumCol;
end;

function TBaseQuery.Count(): Integer;
begin
 Result := NumRow;
end;

function TBaseQuery.GetField(Col: Integer): String;
begin
 Result := '';
 if (Col > 0) and (Col < NumCol) then
  Result := ColNames[Col].Name;
end;

function TBaseQuery.SearchID(Col: String): Integer;
var
 i: Integer;
begin
 Result := -1;
 i := 0;
 while (i < NumCol) and (Result = -1) do begin
  if (ColNames[i].Name = Col) or ((ColNames[i].Table+'.'+ColNames[i].Name) = Col) then begin
   Result := i;
  end;
  i := i+1;
 end;
end;

function TBaseQuery.GetStr(Col: Integer): String;
begin
 Result := '';
 if OK then
  Result := TmpValues[Col];
end;

function TBaseQuery.GetStr(Col: String): String;
var
 i: Integer;
begin
 i := SearchID(UpperCase(Col));
 if i = -1 then begin// error
  raise EMyException.Create('El camp '+Col+' NO esta a la consulta');
 end else begin
  Result := GetStr(i);
 end;
end;

function TBaseQuery.GetStrCrypt(Col: Integer): String;
begin
 Result := '';
 if OK then
  Result := DecStr(GetStr(Col));
end;

function TBaseQuery.GetStrCrypt(Col: String): String;
begin
 Result := '';
 if OK then
  Result := DecStr(GetStr(Col));
end;

function TBaseQuery.GetInt(Col: Integer): Integer;
begin
 Result := 0;
 if OK then
  Result := StrToInt(TmpValues[Col]);
end;

function TBaseQuery.GetInt(Col: String): Integer;
var
 i: Integer;
begin
 i := SearchID(UpperCase(Col));
 if i = -1 then begin// error
  raise EMyException.Create('El camp '+Col+' NO esta a la consulta');
 end else begin
  Result := GetInt(i);
 end;
end;

function TBaseQuery.GetFloat(Col: Integer): Double;
begin
 Result := 0;
// DecimalSeparator := '.';
 if OK then
  Result := StrToFloat(TmpValues[Col]);
// DecimalSeparator := ','; 
end;

function TBaseQuery.GetFloat(Col: String): Double;
var
 i: Integer;
begin
 i := SearchID(UpperCase(Col));
 if i = -1 then begin// error
  raise EMyException.Create('El camp '+Col+' NO esta a la consulta');
 end else begin
  Result := GetFloat(i);
 end;
end;

function TBaseQuery.GetFloatD(Col: Integer): Double;
var
 Str: String;
 a: Integer;
begin
 Result := 0;
// DecimalSeparator := '.';
 if OK then begin
  Str := TmpValues[Col];
  a:=1;
  while a <= Length(Str) do begin
   if Str[a] = '.' then begin
     Str[a] := DecimalSeparator;
     a := Length(Str) + 1; // paro el bucle perque nomes pot haber un sol signe decimal
   end;
   a := a+1;
  end;
  Result := StrToFloat(Str);
 end;
// DecimalSeparator := ',';
end;

function TBaseQuery.GetFloatD(Col: String): Double;
var
 i: Integer;
begin
 i := SearchID(UpperCase(Col));
 if i = -1 then begin// error
  raise EMyException.Create('El camp '+Col+' NO esta a la consulta');
 end else begin
  Result := GetFloatD(i);
 end;
end;

end.
