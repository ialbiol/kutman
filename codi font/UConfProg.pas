unit UConfProg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TFConfProg = class(TForm)
    Label1: TLabel;
    ETFulla: TLabeledEdit;
    CBRetalls: TCheckBox;
    CBEtqTab: TCheckBox;
    CBEtqPesa: TCheckBox;
    CBRepartir: TCheckBox;
    RGOptimitzar: TRadioGroup;
    Label2: TLabel;
    CBMidesX: TComboBox;
    CBMidesY: TComboBox;
    Label3: TLabel;
    BTOk: TBitBtn;
    BTKo: TBitBtn;
    Label4: TLabel;
    CBIdioma: TComboBox;
    EMarge: TLabeledEdit;
    ERXap: TLabeledEdit;
    CBCompletar: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BTKoClick(Sender: TObject);
    procedure BTOkClick(Sender: TObject);
  private
    { Private declarations }

    procedure SaveConf();
  public
    { Public declarations }
  end;


procedure LoadConf(ftx: String);

var
 FConfProg: TFConfProg;
 //var conf
 FtxConfName: String;

 BDRetall: Boolean;
 EtqTab: Boolean;
 EtqPesa: Boolean;
 RepRetall: Boolean;
 CompTab: Boolean;
 TOptim: Integer;
 TF: Integer;
 TM: Integer;
 RX: Integer;
 MidesX: Integer;
 MidesY: Integer;
 IDIdioma: Integer;


implementation

uses UIdioma, IniFiles;

{$R *.dfm}

procedure LoadConf(ftx: String);
var
 IniFile: TIniFile;
 aux: Integer;
 s: String;
begin
 FtxConfName := ftx;

 IniFile := TIniFile.Create(FtxConfName);

 s := IniFile.ReadString('PROGRAM', 'Lang', 'Catala');
 UIdioma.Idioma := ExtractFilePath(ParamStr(0))+ 'lang/' + s + '.ini';
 if (s = 'Castellano') then IDIdioma := 0;
 if (s = 'Italiano') then IDIdioma := 1;
 if (s = 'Fran�ais') then IDIdioma := 2;
 if (s = 'English') then IDIdioma := 3;
 if (s = 'Catala') then IDIdioma := 4;

 aux := IniFile.ReadInteger('PROGRAM', 'BDRetalls', 0);
 BDRetall := (aux = 1);

 aux := IniFile.ReadInteger('PROGRAM', 'EtqTab', 0);
 EtqTab := (aux = 1);

 aux := IniFile.ReadInteger('PROGRAM', 'EtqPesa', 0);
 EtqPesa := (aux = 1);

 aux := IniFile.ReadInteger('PROGRAM', 'CompTab', 0);
 CompTab := (aux = 1);

 aux := IniFile.ReadInteger('PROGRAM', 'Repartir', 0);
 RepRetall := (aux = 1);

 TOptim := IniFile.ReadInteger('PROGRAM', 'TOptim', 0);

 TF := IniFile.ReadInteger('PROGRAM', 'Fulla', 4);

 TM := IniFile.ReadInteger('PROGRAM', 'Marge', 20);

 RX := IniFile.ReadInteger('PROGRAM', 'RetallXap', 20);

 MidesX := IniFile.ReadInteger('PROGRAM', 'MidesX', 0);

 MidesY := IniFile.ReadInteger('PROGRAM', 'MidesY', 0);

 IniFile.Free;
end;

procedure TFConfProg.SaveConf();
var
 IniFile: TIniFile;
 aux: Integer;
begin
 IniFile := TIniFile.Create(FtxConfName);

 case IDIdioma of
  0: begin
   IniFile.WriteString('PROGRAM', 'Lang', 'Castellano');
   UIdioma.Idioma := ExtractFilePath(ParamStr(0))+ 'lang/Castellano.ini';
  end;
  1: begin
   IniFile.WriteString('PROGRAM', 'Lang', 'Italiano');
   UIdioma.Idioma := ExtractFilePath(ParamStr(0))+ 'lang/Italiano.ini';
  end;
  2: begin
   IniFile.WriteString('PROGRAM', 'Lang', 'Fran�ais');
   UIdioma.Idioma := ExtractFilePath(ParamStr(0))+ 'lang/Fran�ais.ini';
  end;
  3: begin
   IniFile.WriteString('PROGRAM', 'Lang', 'English');
   UIdioma.Idioma := ExtractFilePath(ParamStr(0))+ 'lang/English.ini';
  end;
  4: begin
   IniFile.WriteString('PROGRAM', 'Lang', 'Catala');
   UIdioma.Idioma := ExtractFilePath(ParamStr(0))+ 'lang/Catala.ini';
  end;
 end;

 aux:=0;
 if (BDRetall = True) then aux := 1;
 IniFile.WriteInteger('PROGRAM', 'BDRetalls', aux);

 aux:=0;
 if (EtqTab = True) then aux := 1;
 IniFile.WriteInteger('PROGRAM', 'EtqTab', aux);

 aux:=0;
 if (CompTab = True) then aux := 1;
 IniFile.WriteInteger('PROGRAM', 'CompTab', aux);

 aux:=0;
 if (EtqPesa = True) then aux := 1;
 IniFile.WriteInteger('PROGRAM', 'EtqPesa', aux);

 aux:=0;
 if (RepRetall = True) then aux := 1;
 IniFile.WriteInteger('PROGRAM', 'Repartir', aux);

 IniFile.WriteInteger('PROGRAM', 'TOptim', TOptim);

 IniFile.WriteInteger('PROGRAM', 'Fulla', TF);

 IniFile.WriteInteger('PROGRAM', 'Marge', TM);

 IniFile.WriteInteger('PROGRAM', 'RetallXap', RX);

 IniFile.WriteInteger('PROGRAM', 'MidesX', MidesX);

 IniFile.WriteInteger('PROGRAM', 'MidesY', MidesY);

 IniFile.Free;
end;

////////////////////////////////////////////////////////////
procedure TFConfProg.FormShow(Sender: TObject);
begin
 //traduir
 UIdioma.TraduirForm(FConfProg);

 //mostrar dades

 CBRetalls.Checked := BDRetall;
 CBCompletar.Checked := CompTab;
 CBEtqTab.Checked := EtqTab;
 CBEtqPesa.Checked := EtqPesa;
 CBRepartir.Checked := RepRetall;
 RGOptimitzar.ItemIndex := TOptim;
 ETFulla.Text := IntToStr(TF);
 EMarge.Text := IntToStr(TM);
 ERXap.Text := IntToStr(RX);
 CBMidesX.ItemIndex := MidesX;
 CBMidesY.ItemIndex := MidesY;
 CBIdioma.ItemIndex := IDIdioma;
end;

procedure TFConfProg.BTKoClick(Sender: TObject);
begin
 FConfProg.ModalResult := mrCancel;
end;

procedure TFConfProg.BTOkClick(Sender: TObject);
begin
//guardar
 BDRetall := CBRetalls.Checked;
 CompTab := CBCompletar.Checked;
 EtqTab := CBEtqTab.Checked;
 EtqPesa := CBEtqPesa.Checked;
 RepRetall := CBRepartir.Checked;
 TOptim := RGOptimitzar.ItemIndex;
 TF := StrToIntDef(ETFulla.Text, 4);
 TM := StrToIntDef(EMarge.Text, 20);
 RX := StrToIntDef(ERXap.Text, 20);
 MidesX := CBMidesX.ItemIndex;
 MidesY := CBMidesY.ItemIndex;
 IDIdioma := CBIdioma.ItemIndex;

 SaveConf();

 FConfProg.ModalResult := mrOk;
end;

end.
