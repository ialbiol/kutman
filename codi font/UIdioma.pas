unit UIdioma;

interface

uses Windows, Buttons, Messages, SysUtils, Variants, Classes, Graphics, Controls,
 Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls, ToolWin, IniFiles;

Function TraduirTxt(Txt: String): String;
procedure TraduirForm(F: TComponent);

var
 Idioma: String; // nom fitxer del idioma usat

implementation

Function TraduirTxt(Txt: String): String;
var
 F: TIniFile;
 S: String;
begin
 F := TIniFile.Create(Idioma);

 S := F.ReadString('TXT', Txt, '-');

 if S = '-' then begin
  F.WriteString('TXT', Txt, Txt);
  S := Txt;
 end;

 Result := S;

 F.Free;
end;

procedure TraduirForm(F: TComponent);
var
 obj: TComponent;
 a, b: Integer;
 Ftx: TIniFile;
 S, C: String;
begin
 Ftx := TIniFile.Create(Idioma);

 for a:=0 to F.ComponentCount - 1 do begin
  Obj := F.Components[a];
  if Obj <> nil then begin
   if Obj.ClassName = 'TLabel' then
    if (Obj As TLabel).Caption <> '' then begin
     C := (Obj As TLabel).Caption;

     S := Ftx.ReadString('TXT', C, '-');
     if S = '-' then begin
      Ftx.WriteString('TXT', C, C);
      S := C;
     end;

     (Obj As TLabel).Caption := S;
    end;

   if Obj.ClassName = 'TButton' then
    if (Obj As TButton).Caption <> '' then begin
     C := (Obj As TButton).Caption;

     S := Ftx.ReadString('TXT', C, '-');
     if S = '-' then begin
      Ftx.WriteString('TXT', C, C);
      S := C;
     end;

     (Obj As TButton).Caption := S;
    end;

   if Obj.ClassName = 'TBitBtn' then
    if (Obj As TBitBtn).Caption <> '' then begin
     C := (Obj As TBitBtn).Caption;

     S := Ftx.ReadString('TXT', C, '-');
     if S = '-' then begin
      Ftx.WriteString('TXT', C, C);
      S := C;
     end;

     (Obj As TBitBtn).Caption := S;
    end;

   if Obj.ClassName = 'TSpeedButton' then
    if (Obj As TSpeedButton).Caption <> '' then begin
     C := (Obj As TSpeedButton).Caption;

     S := Ftx.ReadString('TXT', C, '-');
     if S = '-' then begin
      Ftx.WriteString('TXT', C, C);
      S := C;
     end;

     (Obj As TSpeedButton).Caption := S;
    end;

   if Obj.ClassName = 'TPanel' then
    if (Obj As TPanel).Caption <> '' then begin
     C := (Obj As TPanel).Caption;

     S := Ftx.ReadString('TXT', C, '-');
     if S = '-' then begin
      Ftx.WriteString('TXT', C, C);
      S := C;
     end;

     (Obj As TPanel).Caption := S;
    end;

   if Obj.ClassName = 'TTabSheet' then
    if (Obj As TTabSheet).Caption <> '' then begin
     C := (Obj As TTabSheet).Caption;

     S := Ftx.ReadString('TXT', C, '-');
     if S = '-' then begin
      Ftx.WriteString('TXT', C, C);
      S := C;
     end;

     (Obj As TTabSheet).Caption := S;
    end;

   if Obj.ClassName = 'TTabControl' then
    for b:=0 to (Obj As TTabControl).Tabs.Count -1 do
     if (Obj As TTabControl).Tabs.Strings[b] <> '' then begin
      C := (Obj As TTabControl).Tabs.Strings[b];

      S := Ftx.ReadString('TXT', C, '-');
      if S = '-' then begin
       Ftx.WriteString('TXT', C, C);
       S := C;
      end;

      (Obj As TTabControl).Tabs.Strings[b] := S;
     end;

  end; // obj <> nil
 end; // for

 Ftx.Free;
end;

end.
