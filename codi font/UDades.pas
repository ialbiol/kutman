unit UDades;

interface

uses UBaseBD;

type
 TList = class
  private
   Dades: Array of Record
           Str: String;
           Value: String;
          end;

  public
   Constructor Create();
   Destructor Destroy(); override;

   Procedure Add(Value, Str: String);
   Procedure DeleteValue(Value: String); 
   Procedure DeleteString(Str: String);
   Procedure Delete(Pos: Integer);
   Procedure Change(Pos: Integer; Str: String);
   Procedure Clear();
   Procedure FillCB(CB: TObject);
   procedure AddDB(T: TBaseQuery);

   Function FindString(Value: String): Integer;
   Function FindPosString(Pos: Integer): String;
   Function FindValue(Value: String): Integer;
   Function FindPosValue(Pos: Integer): String;
   Function FindStringFromValue(Value: String): String;
   Function FindValueFromString(Str: String): String;
 end;

 TTabList = class
  private
   Dades: Array of Record
           Value: String;
           Str: String;
           IDColor: String;
           IDMaterial: String;
           IDGruix: String;
           Ample: String;
           Alt: String;
           Quant: String;

           MUs: Integer;
           MTot: Integer;
          end;

  public
   Constructor Create();
   Destructor Destroy(); override;

   Procedure Add(Value, Str, Color, Mat, Gruix, Ample, Alt, Quant: String);
   Procedure Change(Pos: Integer; Str, Color, Mat, Gruix, Ample, Alt, Quant: String);
   Procedure FillCB(CB: TObject);
   procedure AddDB(T: TBaseQuery);

   Procedure DeleteString(Str: String);
   Procedure Delete(Pos: Integer);

   Procedure Clear();

   Function FindString(Value: String): Integer;
   Function FindValue(Value: String): Integer;
   Function FindValueFromString(Str: String): String;

   Function GetNom(Pos: Integer): String;
   Function GetAmple(Pos: Integer): Integer;
   Function GetAlt(Pos: Integer): Integer;
   Function GetColor(Pos: Integer): String;
   Function GetMaterial(Pos: Integer): String;
   Function GetGruix(Pos: Integer): String;

   Procedure ResetUs();
   Procedure MUsPos(Pos, Mm: Integer);
   Procedure FillCBUs(CB: TObject);
   Procedure UpdateUs();
 end;

 TXapList = class
  private
   Dades: Array of Record
           Value: String;
           Str: String;
           IDColor: String;
           Gruix: String;
           MXRoll: String;
           Quant: String;

           MUs: Integer;
           MTot: Integer;
          end;
  public
   Constructor Create();
   Destructor Destroy(); override;

   Procedure Add(Value, Str, Color, Gruix, MXRoll, Quant: String);
   Procedure Change(Pos: Integer; Str, Color, Gruix, MXRoll, Quant: String);
   Procedure FillCB(CB: TObject);
   procedure AddDB(T: TBaseQuery);

   Procedure DeleteString(Str: String);
   Procedure Delete(Pos: Integer);

   Procedure Clear();

   Function FindString(Value: String): Integer;
   Function FindValue(Value: String): Integer;
   Function FindValueFromString(Str: String): String;

   Function GetNom(Pos: Integer): String;
   Function GetColor(Pos: Integer): String;
   Function GetGruix(Pos: Integer): Integer;

   Procedure ResetUs();
   Procedure MUsPos(Pos, Mm: Integer);
   Procedure FillCBUs(CB: TObject);
   Procedure UpdateUs();
 end;

 TKtFile = class
  private
   Dades: Array of Record
           Q: String;
           Am: String;
           Al: String;
           T: String;
           G: String;
           Xs: String;
           Xe: String;
           Xi: String;
           Xd: String;
           P: String;
          end;

  public
   Constructor Create();
   Destructor Destroy(); override;

   Procedure Load(FtxName: String);
   Procedure WriteTo(CB: TObject);
   Procedure ReadFrom(CB: TObject);
   Procedure Save(FtxName: String);

  private
   Procedure Clear();

 end;

var
 LCol: TList;
 LMat: TList;
 LGruix: TList;
 LTab: TTabList;
 LXap: TXapList;

implementation

uses Grids, StdCtrls, UMySQLDB, SysUtils, NiceGrid, UConfProg, UInici;

////////////////////////////////////////////////////////////////////////////////////
///////////////// TList
/////////////////////////////////////////////////////////////////////////////////////

Constructor TList.Create();
begin
 Clear();
end;

Destructor TList.Destroy();
begin
 Clear();
end;


Procedure TList.Add(Value, Str: String);
begin
 SetLength(Dades, Length(Dades)+1);
 Dades[High(Dades)].Value := Value;
 Dades[High(Dades)].Str := Str;
end;

Procedure TList.DeleteValue(Value: String);
begin
 Delete(FindValue(Value));
end;

Procedure TList.DeleteString(Str: String);
begin
 Delete(FindString(Str));
end;

Procedure TList.Delete(Pos: Integer);
var
 a: Integer;
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  for a:= Pos to Length(Dades)-2 do
   Dades[a] := Dades[a+1];

  SetLength(Dades, Length(Dades)-1); 
 end;
end;

Procedure TList.Change(Pos: Integer; Str: String);
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Dades[Pos].Str := Str;
 end;
end;

Procedure TList.Clear();
begin
 SetLength(Dades, 0);
end;

Procedure TList.FillCB(CB: TObject);
var
 b: Integer;
begin
   if CB.ClassName = 'TComboBox' then begin
    (CB As TComboBox).Clear;
//    (CB As TComboBox).Items.NameValueSeparator := '-';

    if Length(Dades) > 0 then begin
     for b:=0 to Length(Dades)-1 do begin
      (CB As TComboBox).Items.Add(Dades[b].Str)  //(Q.GetStr(0) + '-' +  Q.GetStr(1));
     end;
     (CB As TComboBox).ItemIndex := 0;
    end;
   end;

   if CB.ClassName = 'TStringGrid' then begin
    (CB As TStringGrid).Rows[1].Clear;
    (CB As TStringGrid).RowCount := 2;

    if Length(Dades) > 0 then begin
     for b:=1 to Length(Dades) do begin
       (CB As TStringGrid).Cells[0, b] := Dades[b-1].Str;
     end;

     if Length(Dades) = 1 then b := 2 else b:= Length(Dades);
     (CB As TStringGrid).RowCount := b;
    end;
   end;

   if CB.ClassName = 'TNiceGrid' then begin
    if Length(Dades) > 0 then begin
    (CB As TNiceGrid).RowCount := Length(Dades)+1;
     for b:=0 to Length(Dades)-1 do begin
       (CB As TNiceGrid).Cells[0, b] := Dades[b].Str;
     end;
    end;
   end;
end;

procedure TList.AddDB(T: TBaseQuery);
var
 a: Integer;
begin
 Clear();
 a:=1;
 while a <= T.Count do begin
  Add(T.GetStr(0), T.GetStr(1));

  (T As TMySQLQuery).Next();
  a := a + 1;
 end;
end;

Function TList.FindString(Value: String): Integer;
var
 a: Integer;
begin
 a := 0;
 Result := -1;
 while (a < Length(Dades)) and (Result = -1) do begin
  if Dades[a].Str = Value then
   Result := a;
  a := a + 1;
 end;
end;

Function TList.FindPosString(Pos: Integer): String;
begin
 if (Pos >=0) and (Pos < Length(Dades)) then
  Result := Dades[Pos].Str;
end;

Function TList.FindValue(Value: String): Integer;
var
 a: Integer;
begin
 a := 0;
 Result := -1;
 while (a < Length(Dades)) and (Result = -1) do begin
  if Dades[a].Value = Value then
   Result := a;
  a := a + 1;
 end;
end;

Function TList.FindPosValue(Pos: Integer): String;
begin
 if (Pos >=0) and (Pos < Length(Dades)) then
  Result := Dades[Pos].Value;
end;

Function TList.FindStringFromValue(Value: String): String;
var
 a: Integer;
begin
 a := 0;
 Result := '';
 while (a < Length(Dades)) and (Result = '') do begin
  if Dades[a].Value = Value then
   Result := Dades[a].Str;
  a := a + 1;
 end;
end;

Function TList.FindValueFromString(Str: String): String;
var
 a: Integer;
begin
 a := 0;
 Result := '';
 while (a < Length(Dades)) and (Result = '') do begin
  if Dades[a].Str = Str then
   Result := Dades[a].Value;
  a := a + 1;
 end;
end;

////////////////////////////////////////////////////////////////////////////////////
///////////////// TKtFile
/////////////////////////////////////////////////////////////////////////////////////

Constructor TKtFile.Create();
begin
 Clear();
end;

Destructor TKtFile.Destroy();
begin
 Clear();
end;

Procedure TKtFile.Load(FtxName: String);
var
 F: TextFile;
 S: string;
 C: Array of String;
 a, b, z: Integer;
begin
 AssignFile(F, FtxName);
 Reset(F);
 while not Eof(F) do begin
  Readln(F, S);
  S := Trim(S);
  if S <> '' then begin
   if (S[1] <> '/') and (S[2] <> '/') then begin // llevo els comentaris
    // explode
    SetLength(C, 1);
    b:=0;
    C[b] := '';
    for a:=1 to Length(S) do begin
     if S[a] = ' ' then begin
      b := b + 1;
      SetLength(C, Length(C)+1);
      C[b] := '';
     end else begin
      C[b] := C[b] + S[a];
     end;
    end;
    // fin exclude

    //entro la dada
    SetLength(Dades, Length(Dades)+1);
    z := High(Dades);
    Dades[z].Q := C[0];
    Dades[z].Am := C[1];
    Dades[z].Al := C[2];
    Dades[z].T := C[3];
    Dades[z].G := C[4];
    Dades[z].Xs := C[5];
    Dades[z].Xe := C[6];
    Dades[z].Xi := C[7];
    Dades[z].Xd := C[8];
    Dades[z].P := C[9];

    SetLength(C, 0); //limpiesa
   end;
  end;
 end;

 CloseFile(F);
end;

Procedure TKtFile.WriteTo(CB: TObject);
var
 b: Integer;
 g: String;
begin
 if CB.ClassName = 'TStringGrid' then begin
  (CB As TStringGrid).Rows[1].Clear;
  (CB As TStringGrid).RowCount := 2;

  if Length(Dades) > 0 then begin
   for b:=1 to Length(Dades) do begin
    g:='No';
    if Dades[b-1].G = '1' then g:='Si';
    (CB As TStringGrid).Cells[0, b] := Dades[b-1].Q;
    (CB As TStringGrid).Cells[1, b] := Dades[b-1].Al;
    (CB As TStringGrid).Cells[2, b] := Dades[b-1].Am;
    (CB As TStringGrid).Cells[3, b] := LTab.GetNom(LTab.FindValue(Dades[b-1].T));
    (CB As TStringGrid).Cells[4, b] := g;
    (CB As TStringGrid).Cells[5, b] := Dades[b-1].P;
    (CB As TStringGrid).Cells[6, b] := Dades[b-1].Xs;
    (CB As TStringGrid).Cells[7, b] := Dades[b-1].Xe;
    (CB As TStringGrid).Cells[8, b] := Dades[b-1].Xi;
    (CB As TStringGrid).Cells[9, b] := Dades[b-1].Xd;
   end;

   if Length(Dades) = 1 then b := 2 else b:= Length(Dades);
    (CB As TStringGrid).RowCount := b;
  end;
 end;

 if CB.ClassName = 'TNiceGrid' then begin
  if Length(Dades) = 1 then b := 2 else b:= Length(Dades);
  (CB As TNiceGrid).RowCount := b;

  if Length(Dades) > 0 then begin
   for b:=0 to Length(Dades)-1 do begin
    g:='No';
    if Dades[b].G = '1' then g:='Si';
    (CB As TNiceGrid).Cells[0, b] := Dades[b].Q;
    (CB As TNiceGrid).Cells[1, b] := Dades[b].Al;
    (CB As TNiceGrid).Cells[2, b] := Dades[b].Am;
    (CB As TNiceGrid).Cells[3, b] := LTab.GetNom(LTab.FindValue(Dades[b].T));
    (CB As TNiceGrid).Cells[4, b] := g;
    (CB As TNiceGrid).Cells[5, b] := Dades[b].P;
    (CB As TNiceGrid).Cells[6, b] := Dades[b].Xs;
    (CB As TNiceGrid).Cells[7, b] := Dades[b].Xe;
    (CB As TNiceGrid).Cells[8, b] := Dades[b].Xi;
    (CB As TNiceGrid).Cells[9, b] := Dades[b].Xd;
   end;
  end;
 end;
end;

Procedure TKtFile.ReadFrom(CB: TObject);
var
 b, z: Integer;
 g: String;
begin
 Clear();

 if CB.ClassName = 'TStringGrid' then begin
  if (CB As TStringGrid).RowCount > 1 then begin
   for b:=1 to (CB As TStringGrid).RowCount do begin
    SetLength(Dades, Length(Dades)+1);
    z := High(Dades);
    g:='0';
    if (CB As TStringGrid).Cells[4, b] = 'Si' then g:='1';
    Dades[z].Q := (CB As TStringGrid).Cells[0, b];
    Dades[z].Am := (CB As TStringGrid).Cells[1, b];
    Dades[z].Al := (CB As TStringGrid).Cells[2, b];
    Dades[z].T := LTab.FindValueFromString((CB As TStringGrid).Cells[3, b]);
    Dades[z].G := g;
    Dades[z].P :=  (CB As TStringGrid).Cells[5, b];
    Dades[z].Xs := (CB As TStringGrid).Cells[6, b];
    Dades[z].Xe := (CB As TStringGrid).Cells[7, b];
    Dades[z].Xi := (CB As TStringGrid).Cells[8, b];
    Dades[z].Xd := (CB As TStringGrid).Cells[9, b];
   end;
  end;
 end;

 if CB.ClassName = 'TNiceGrid' then begin
  if (CB As TNiceGrid).RowCount > 0 then begin
   for b:=0 to (CB As TNiceGrid).RowCount-1 do begin
    SetLength(Dades, Length(Dades)+1);
    z := High(Dades);
    g:='0';
    if (CB As TNiceGrid).Cells[4, b] = 'Si' then g:='1';
    Dades[z].Q := (CB As TNiceGrid).Cells[0, b];
    Dades[z].Am := (CB As TNiceGrid).Cells[1, b];
    Dades[z].Al := (CB As TNiceGrid).Cells[2, b];
    Dades[z].T := LTab.FindValueFromString((CB As TNiceGrid).Cells[3, b]);
    Dades[z].G := g;
    Dades[z].P := (CB As TNiceGrid).Cells[5, b];
    Dades[z].Xs := (CB As TNiceGrid).Cells[6, b];
    Dades[z].Xe := (CB As TNiceGrid).Cells[7, b];
    Dades[z].Xi := (CB As TNiceGrid).Cells[8, b];
    Dades[z].Xd := (CB As TNiceGrid).Cells[9, b];
   end;
  end;
 end;
end;

Procedure TKtFile.Save(FtxName: String);
var
 F: TextFile;
 a: Integer;
 S: String;
begin
 AssignFile(F, FtxName);
 Rewrite(F);
 //comentari inicial
 WriteLn(F, '//quantitat ample alt idtablero gir(1-si, 0-no) xapatsup xesq xinf xdrt prioritat');
 if Length(Dades)>0 then begin
  for a:=0 to Length(Dades)-1 do begin
   S := Dades[a].Q +' ';
   S := S + Dades[a].Am +' ';
   S := S + Dades[a].Al +' ';
   S := S + Dades[a].T +' ';
   S := S + Dades[a].G +' ';
   S := S + Dades[a].Xs +' ';
   S := S + Dades[a].Xe +' ';
   S := S + Dades[a].Xi +' ';
   S := S + Dades[a].Xd +' ';
   S := S + Dades[a].P;

   WriteLn(F, S);
  end;
 end;
 CloseFile(F);
end;

Procedure TKtFile.Clear();
begin
 SetLength(Dades, 0);
end;

////////////////////////////////////////////////////////////////////////////////////
///////////////// TTabList
/////////////////////////////////////////////////////////////////////////////////////

Constructor TTabList.Create();
begin
 Clear();
end;

Destructor TTabList.Destroy();
begin
 Clear();
end;

Procedure TTabList.Add(Value, Str, Color, Mat, Gruix, Ample, Alt, Quant: String);
begin
 SetLength(Dades, Length(Dades)+1);
 Dades[High(Dades)].Value := Value;
 Dades[High(Dades)].Str := Str;
 Dades[High(Dades)].IDColor := Color;
 Dades[High(Dades)].IDMaterial := Mat;
 Dades[High(Dades)].IDGruix := Gruix;
 Dades[High(Dades)].Ample := Ample;
 Dades[High(Dades)].Alt := Alt;
 Dades[High(Dades)].Quant := Quant;
 Dades[High(Dades)].MTot := STrToIntDef(Quant, 0);
end;

Procedure TTabList.Change(Pos: Integer; Str, Color, Mat, Gruix, Ample, Alt, Quant: String);
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Dades[Pos].Str := Str;
  Dades[Pos].IDColor := Color;
  Dades[Pos].IDMaterial := Mat;
  Dades[Pos].IDGruix := Gruix;
  Dades[Pos].Ample := Ample;
  Dades[Pos].Alt := Alt;
  Dades[Pos].Quant := Quant;
 end;
end;

Procedure TTabList.FillCB(CB: TObject);
var
 b: Integer;
begin
 if CB.ClassName = 'TNiceGrid' then begin
  if Length(Dades) > 0 then begin
   (CB As TNiceGrid).RowCount := Length(Dades)+1;
   for b:=0 to Length(Dades)-1 do begin
    (CB As TNiceGrid).Cells[0, b] := Dades[b].Str;
    (CB As TNiceGrid).Cells[1, b] := LCol.FindStringFromValue(Dades[b].IDColor);
    (CB As TNiceGrid).Cells[2, b] := LMat.FindStringFromValue(Dades[b].IDMaterial);
    (CB As TNiceGrid).Cells[3, b] := LGruix.FindStringFromValue(Dades[b].IDGruix);
    (CB As TNiceGrid).Cells[4, b] := Dades[b].Ample;
    (CB As TNiceGrid).Cells[5, b] := Dades[b].Alt;
    (CB As TNiceGrid).Cells[6, b] := Dades[b].Quant;
   end;
  end;
 end;

 if CB.ClassName = 'TComboBox' then begin
  (CB As TComboBox).Clear;
//  (CB As TComboBox).Items.NameValueSeparator := '-';

  if Length(Dades) > 0 then begin
   for b:=0 to Length(Dades)-1 do begin
    (CB As TComboBox).Items.Add(Dades[b].Str);
   end;
   (CB As TComboBox).ItemIndex := 0;
  end;
 end;
end;

procedure TTabList.AddDB(T: TBaseQuery);
var
 a: Integer;
begin
 Clear();
 a:=1;
 while a <= T.Count do begin
  Add(T.GetStr(0), T.GetStr(1), T.GetStr(2), T.GetStr(3), T.GetStr(4), T.GetStr(5), T.GetStr(6), T.GetStr(7));

  (T As TMySQLQuery).Next();
  a := a + 1;
 end;
end;

Procedure TTabList.DeleteString(Str: String);
begin
 Delete(FindString(Str));
end;

Procedure TTabList.Delete(Pos: Integer);
var
 a: Integer;
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  for a:= Pos to Length(Dades)-2 do
   Dades[a] := Dades[a+1];

  SetLength(Dades, Length(Dades)-1);
 end;
end;

Procedure TTabList.Clear();
begin
 SetLength(Dades, 0);
end;

Function TTabList.FindString(Value: String): Integer;
var
 a: Integer;
begin
 a := 0;
 Result := -1;
 while (a < Length(Dades)) and (Result = -1) do begin
  if Dades[a].Str = Value then
   Result := a;
  a := a + 1;
 end;
end;

Function TTabList.FindValue(Value: String): Integer;
var
 a: Integer;
begin
 a := 0;
 Result := -1;
 while (a < Length(Dades)) and (Result = -1) do begin
  if Dades[a].Value = Value then
   Result := a;
  a := a + 1;
 end;
end;

Function TTabList.FindValueFromString(Str: String): String;
var
 a: Integer;
begin
 a := 0;
 Result := '';
 while (a < Length(Dades)) and (Result = '') do begin
  if Dades[a].Str = Str then
   Result := Dades[a].Value;
  a := a + 1;
 end;
end;

Function TTabList.GetNom(Pos: Integer): String;
begin
 Result := '';
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := Dades[Pos].Str;
 end;
end;

Function TTabList.GetAmple(Pos: Integer): Integer;
begin
 Result := 0;
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := StrToIntDef(Dades[Pos].Ample, 0);
 end;
end;

Function TTabList.GetAlt(Pos: Integer): Integer;
begin
 Result := 0;
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := StrToIntDef(Dades[Pos].Alt, 0);
 end;
end;

Function TTabList.GetColor(Pos: Integer): String;
begin
 Result := '0';
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := Dades[Pos].IDColor;
 end;
end;

Function TTabList.GetMaterial(Pos: Integer): String;
begin
 Result := '0';
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := Dades[Pos].IDMaterial;
 end;
end;

Function TTabList.GetGruix(Pos: Integer): String;
begin
 Result := '0';
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := Dades[Pos].IDGruix;
 end;
end;

Procedure TTabList.ResetUs();
var
 a: Integer;
begin
 for a:=0 to Length(Dades) do begin
  Dades[a].MUs := 0;
 end;
end;

Procedure TTabList.MUsPos(Pos, Mm: Integer);
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Dades[Pos].MUs := Dades[Pos].MUs + Mm;
 end;
end;

Procedure TTabList.FillCBUs(CB: TObject);
var
 b: Integer;
begin
 if CB.ClassName = 'TNiceGrid' then begin
  if Length(Dades) > 0 then begin
   (CB As TNiceGrid).RowCount := Length(Dades)+1;
   for b:=0 to Length(Dades)-1 do begin
    if Dades[b].MUs > 0 then begin
     (CB As TNiceGrid).Cells[0, b] := Dades[b].Str;
     (CB As TNiceGrid).Cells[1, b] := IntToStr(Dades[b].MTot);
     (CB As TNiceGrid).Cells[2, b] := IntToStr(Dades[b].MUs);
    end;
   end;
  end;
 end;
end;

Procedure TTabList.UpdateUs();
var
 b: Integer;
 Sql: String;
begin
 if Length(Dades) > 0 then begin
  for b:=0 to Length(Dades)-1 do begin
   if Dades[b].MUs > 0 then begin
    Dades[b].MTot := Dades[b].MTot - Dades[b].MUs;
    Dades[b].Quant := IntToStr(Dades[b].MTot);

    Sql := 'UPDATE Tableros SET Quant="'+Dades[b].Quant+'" WHERE IDTablero="'+Dades[b].Value+'"';
    BD.ModQuery(Sql);
   end;
  end;
 end;
end;

////////////////////////////////////////////////////////////////////////////////////
///////////////// TXapList
/////////////////////////////////////////////////////////////////////////////////////

Constructor TXapList.Create();
begin
 Clear();
 Add('0', '0 mm', '0', '0', '1000', '1000000');
end;

Destructor TXapList.Destroy();
begin
 Clear();
end;

Procedure TXapList.Add(Value, Str, Color, Gruix, MXRoll, Quant: String);
begin
 SetLength(Dades, Length(Dades)+1);
 Dades[High(Dades)].Value := Value;
 Dades[High(Dades)].Str := Str;
 Dades[High(Dades)].IDColor := Color;
 Dades[High(Dades)].Gruix := Gruix;
 Dades[High(Dades)].MXRoll := MXRoll;
 Dades[High(Dades)].Quant := Quant;
 Dades[High(Dades)].MUs := 0;
 Dades[High(Dades)].MTot := STrToIntDef(Quant, 0); //* (StrToIntDef(MXRoll, 0) * 1000); //distancia total en mm
end;

Procedure TXapList.Change(Pos: Integer; Str, Color, Gruix, MXRoll, Quant: String);
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Dades[Pos].Str := Str;
  Dades[Pos].IDColor := Color;
  Dades[Pos].Gruix := Gruix;
  Dades[Pos].MXRoll := MXRoll;
  Dades[Pos].Quant := Quant;
 end;
end;

Procedure TXapList.FillCB(CB: TObject);
var
 b: Integer;
begin
 if CB.ClassName = 'TNiceGrid' then begin
  if Length(Dades) > 0 then begin
   (CB As TNiceGrid).RowCount := Length(Dades)+1;
   for b:=0 to Length(Dades)-1 do begin
    (CB As TNiceGrid).Cells[0, b] := Dades[b].Str;
    (CB As TNiceGrid).Cells[1, b] := LCol.FindStringFromValue(Dades[b].IDColor);
    (CB As TNiceGrid).Cells[2, b] := Dades[b].Gruix;
    (CB As TNiceGrid).Cells[3, b] := Dades[b].MXRoll;
    (CB As TNiceGrid).Cells[4, b] := Dades[b].Quant;
   end;
  end;
 end;

 if CB.ClassName = 'TComboBox' then begin
  (CB As TComboBox).Clear;
//  (CB As TComboBox).Items.NameValueSeparator := '-';

  if Length(Dades) > 0 then begin
   for b:=0 to Length(Dades)-1 do begin
    (CB As TComboBox).Items.Add(Dades[b].Str);
   end;
   (CB As TComboBox).ItemIndex := 0;
  end;
 end;
end;

procedure TXapList.AddDB(T: TBaseQuery);
var
 a: Integer;
begin
 Clear();
 Add('0', '0 mm', '0', '0', '1000', '1000000');
 a:=1;
 while a <= T.Count do begin
  Add(T.GetStr(0), T.GetStr(1), T.GetStr(2), T.GetStr(3), T.GetStr(4), T.GetStr(5));

  (T As TMySQLQuery).Next();
  a := a + 1;
 end;
end;

Procedure TXapList.DeleteString(Str: String);
begin
 Delete(FindString(Str));
end;

Procedure TXapList.Delete(Pos: Integer);
var
 a: Integer;
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  for a:= Pos to Length(Dades)-2 do
   Dades[a] := Dades[a+1];

  SetLength(Dades, Length(Dades)-1);
 end;
end;

Procedure TXapList.Clear();
begin
 SetLength(Dades, 0);
end;

Function TXapList.FindString(Value: String): Integer;
var
 a: Integer;
begin
 a := 0;
 Result := -1;
 while (a < Length(Dades)) and (Result = -1) do begin
  if Dades[a].Str = Value then
   Result := a;
  a := a + 1;
 end;
end;

Function TXapList.FindValue(Value: String): Integer;
var
 a: Integer;
begin
 a := 0;
 Result := -1;
 while (a < Length(Dades)) and (Result = -1) do begin
  if Dades[a].Value = Value then
   Result := a;
  a := a + 1;
 end;
end;

Function TXapList.FindValueFromString(Str: String): String;
var
 a: Integer;
begin
 a := 0;
 Result := '';
 while (a < Length(Dades)) and (Result = '') do begin
  if Dades[a].Str = Str then
   Result := Dades[a].Value;
  a := a + 1;
 end;
end;

Function TXapList.GetNom(Pos: Integer): String;
begin
 Result := '';
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := Dades[Pos].Str;
 end;
end;

Function TXapList.GetColor(Pos: Integer): String;
begin
 Result := '0';
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := Dades[Pos].IDColor;
 end;
end;

Function TXapList.GetGruix(Pos: Integer): Integer;
begin
 Result := 0;
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Result := StrToIntDef(Dades[Pos].Gruix, 0);
 end;
end;

Procedure TXapList.ResetUs();
var
 a: Integer;
begin
 for a:=0 to Length(Dades) do begin
  Dades[a].MUs := 0;
 end;
end;

Procedure TXapList.MUsPos(Pos, Mm: Integer);
begin
 if (Pos >=0) and (Pos < Length(Dades)) then begin
  Dades[Pos].MUs := Dades[Pos].MUs + Mm + (UConfProg.RX * 2);
 end;
end;

Procedure TXapList.FillCBUs(CB: TObject);
var
 b: Integer;
begin
 if CB.ClassName = 'TNiceGrid' then begin
  if Length(Dades) > 0 then begin
   (CB As TNiceGrid).RowCount := Length(Dades)+1;
   for b:=0 to Length(Dades)-1 do begin
    if Dades[b].MUs > 0 then begin
     (CB As TNiceGrid).Cells[0, b] := Dades[b].Str;
     (CB As TNiceGrid).Cells[1, b] := IntToStr(Dades[b].MTot);
     (CB As TNiceGrid).Cells[2, b] := IntToStr(Dades[b].MUs);
    end;
   end;
  end;
 end;
end;

Procedure TXapList.UpdateUs();
var
 b: Integer;
 Sql: String;
begin
 if Length(Dades) > 0 then begin
  for b:=0 to Length(Dades)-1 do begin
   if Dades[b].MUs > 0 then begin
    Dades[b].MTot := Dades[b].MTot - Dades[b].MUs;
    Dades[b].Quant := IntToStr(Dades[b].MTot);

    Sql := 'UPDATE Xapats SET Quant="'+Dades[b].Quant+'" WHERE IDXapat="'+Dades[b].Value+'"';
    BD.ModQuery(Sql);
   end;
  end;
 end;
end;
end.
