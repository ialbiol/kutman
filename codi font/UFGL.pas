unit UFGL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OpenGL, ComCtrls, ToolWin, ExtCtrls, NiceGrid;

type
  TFGL = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    NGTabs: TNiceGrid;
    Panel1: TPanel;
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure NGTabsColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

     Ok: Boolean;

  public
    { Public declarations }
  end;

procedure IniGrafics();
procedure LoadTextures();
procedure FinGrafics();
procedure DibuixarTablero(Num: Integer);

procedure SetMarges(MX, MY: Double);

var
 XP: Integer;
 YP: Integer;
 ZP: Integer;
 FGL: TFGL;

implementation

uses UglContext, TextGL, UTexture, UTablero;

var
 Tex_Fondo: TTexture;

{$R *.dfm}

procedure TFGL.FormCreate(Sender: TObject);
begin
 Ok := False;
end;

procedure TFGL.NGTabsColRowChanged(Sender: TObject; Col, Row: Integer);
begin
 if Ok then
  DibuixarTablero(StrToIntDef(NGTabs.Cells[0, Row], 0));
end;

procedure TFGL.FormShow(Sender: TObject);
begin
 T.MostrarTableros(NGTabs);
 
 XP := 100;
 YP := 50;
 ZP := -136;

 CreateGLContext(Panel1.Handle); // Pass the window handle to create opengl context
 UFGL.IniGrafics();
// T.EscalarOGL(UFGL.XP, UFGL.YP);

 Ok := True;
end;

procedure TFGL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 FGL.ModalResult := mrOk;
end;

procedure TFGL.FormDestroy(Sender: TObject);
begin
 FinGrafics();
 DestroyGLContext; // Free device context created for opengl
end;

procedure TFGL.FormResize(Sender: TObject);
begin
 ResizeGL(Width, Height);  // Pass dimensions for Viewport and aspect ratio
end;

procedure TFGL.FormPaint(Sender: TObject);
begin

 glClearColor(1,1,1,0); // Ponemos un color Oscuro de Fondo...
 glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT); // Algo as� como borrar la Pizarra...
// glMatrixMode(GL_MODELVIEW);
 glLoadIdentity;
 gluLookAt(XP/2,0,ZP, XP/2,0,0, 0,-1,0); // Posici�n y punto de vista de la c�mara...
 GLPointSize(2.0); //Asignamos un tama�o de 2 pixeles para cada punto...

 UglContext.LiniesQuadrat(0, 0, 0.0, XP, YP, 0, 0, 0);
// UTexture.Texture.DrawTexture(Tex_Fondo);

// T.DibuixarOGL();

 SwapGL();
end;

procedure TFGL.FormKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = '8' then begin // amunt
  YP := YP - 6;
  FormPaint(Sender);
 end;
 if Key = '2' then begin // avall
  YP := YP + 6;
  FormPaint(Sender);
 end;
 if Key = '4' then begin // dreta
  XP := XP - 6;
  FormPaint(Sender);
 end;
 if Key = '6' then begin // esquerra
  XP := XP + 6;
  FormPaint(Sender);
 end;
 if Key = '+' then begin // dins
  ZP := ZP + 6;
  FormPaint(Sender);
 end;
 if Key = '-' then begin // fora
  ZP := ZP - 6;
  FormPaint(Sender);
 end;
end;

/////////////////////////////////////////////////////////////////
////////// funcions
//////////////////////////////////////////////////////////////

procedure IniGrafics();
begin
  TextGL.FontPath := ExtractFilePath(ParamStr(0)) + 'fonts\';
  UTexture.TexturePath := ExtractFilePath(ParamStr(0)) + 'textures\';

  Texture := TTextureUnit.Create;
  Texture.Limit := 1024*1024;
  BuildFont;
  LoadTextures()
end;

procedure LoadTextures();
begin
 Tex_Fondo := UTexture.Texture.GetTexture(UTexture.TexturePath+'Brown.jpg', 'Plain');
 Tex_Fondo.X := 0.0;
 Tex_Fondo.Y := 0.0;
 Tex_Fondo.Z := 0.01;
 Tex_Fondo.W := XP;
 Tex_Fondo.H := YP;
end;

procedure FinGrafics();
begin
 Texture.FreeTextures();
 Texture.Destroy();
end;

procedure SetMarges(MX, MY: Double);
begin
 Tex_Fondo.X := Tex_Fondo.X - MX;
 Tex_Fondo.Y := Tex_Fondo.Y - MY;
 Tex_Fondo.W := Tex_Fondo.W + (2 * MX);
 Tex_Fondo.H := Tex_Fondo.H + (2 * MY);
end;

procedure DibuixarTablero(Num: Integer);
begin
 glClearColor(1,1,1,0); // Ponemos un color Oscuro de Fondo...
 glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT); // Algo as� como borrar la Pizarra...
// glMatrixMode(GL_MODELVIEW);
 glLoadIdentity;
 gluLookAt(XP/2,0,ZP, XP/2,0,0, 0,-1,0); // Posici�n y punto de vista de la c�mara...
 GLPointSize(2.0); //Asignamos un tama�o de 2 pixeles para cada punto...

// UTexture.Texture.DrawTexture(Tex_Fondo);
 UglContext.LiniesQuadrat(0, 0, 0.0, XP, YP, 0, 0, 0);
 
 T.Dibuixar(Num, XP, YP);

 SwapGL();
end;

end.
