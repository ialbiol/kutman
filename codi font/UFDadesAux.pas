unit UFDadesAux;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NiceGrid, ExtCtrls, StdCtrls, Buttons;

type
  TFDadesAux = class(TForm)
    NGColors: TNiceGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    NGGruix: TNiceGrid;
    Panel3: TPanel;
    NGMat: TNiceGrid;
    BTOk: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure NGColorsColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure NGGruixColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure NGMatColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure BTOkClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }

    LastColRow: Integer;
    LastColCol: Integer;
    LastColValue: String;

    LastGruRow: Integer;
    LastGruCol: Integer;
    LastGruValue: String;

    LastMatRow: Integer;
    LastMatCol: Integer;
    LastMatValue: String;

    DoChange: Boolean;
  public
    { Public declarations }
  end;

var
  FDadesAux: TFDadesAux;

implementation

uses UMySQLDB, UDades, UInici;

{$R *.dfm}

procedure TFDadesAux.FormShow(Sender: TObject);
begin
 DoChange := False;

 //mostrar tots los llistats
 LCol := TList.Create();
 LMat := TList.Create();
 LGruix := TList.Create();

 LCol.AddDB(BD.Query('SELECT * FROM Colors'));
 LMat.AddDB(BD.Query('SELECT * FROM Materials'));
 LGruix.AddDB(BD.Query('SELECT * FROM Gruixos'));

 LCol.FillCB(NGColors);
 LMat.FillCB(NGMat);
 LGruix.FillCB(NGGruix);

 LastColRow:= NGColors.Row;
 LastColCol:= NGColors.Col;
 LastColValue:= NGColors.Cells[NGColors.Col, NGColors.Row];

 LastMatRow:= NGMat.Row;
 LastMatCol:= NGMat.Col;
 LastMatValue:= NGMat.Cells[NGMat.Col, NGMat.Row];

 LastGruRow:= NGGruix.Row;
 LastGruCol:= NGGruix.Col;
 LastGruValue:= NGGruix.Cells[NGGruix.Col, NGGruix.Row];

 DoChange := True;
end;

procedure TFDadesAux.NGColorsColRowChanged(Sender: TObject; Col,
  Row: Integer);
var
 aux, Sql, S: String;
 i: Integer;
begin
 if DoChange then begin
  if (LastColRow < NGColors.RowCount) and (LastColCol < NGColors.ColCount) then begin
   aux := NGColors.Cells[LastColCol, LastColRow];
   if (aux <> LastColValue) then begin
    //poden pasar 3 coses
    //1- aux<>'' i LastColValue = '' -> crear un nou registre
    if (aux <> '') and (LastColValue = '') then begin
     Sql := 'INSERT INTO Colors (Nom) VALUES ("'+aux+'")';
     BD.ModQuery(Sql);
     i := BD.NewID;
     LCol.Add(IntToStr(i), aux);
    end;

    //2- aux<>'' i LastColValue <> '' -> modificar dada
    if (aux <> '') and (LastColValue <> '') then begin
     S := LCol.FindValueFromString(LastColValue);
     Sql := 'UPDATE Colors SET Nom="'+aux+'" WHERE IDColor="'+S+'"';
     BD.ModQuery(Sql);
     LCol.Change(LCol.FindString(LastColValue), aux);
    end;

    //3- aux='' i LastColValue <> '' -> eliminar dada
    if (aux = '') and (LastColValue <> '') then begin
     S := LCol.FindValueFromString(LastColValue);
     Sql := 'DELETE FROM Colors WHERE IDColor="'+S+'"';
     BD.ModQuery(Sql);
     LCol.DeleteString(LastColValue);
    end;

//    showMessage(aux+'-'+LastColValue);
   end;
  end else begin // la celda antiga esta buida i sa tret automaticament
    if (LastColValue <> '') then begin
     S := LCol.FindValueFromString(LastColValue);
     Sql := 'DELETE FROM Colors WHERE IDColor="'+S+'"';
     BD.ModQuery(Sql);
     LCol.DeleteString(LastColValue);
    end;
  end;

  LastColRow:= Row;
  LastColCol:= Col;
  LastColValue:= NGColors.Cells[Col, Row];
 end;
end;

procedure TFDadesAux.NGGruixColRowChanged(Sender: TObject; Col,
  Row: Integer);
var
 aux, Sql, S: String;
 i: Integer;
begin
 //gruixos
 if DoChange then begin
  if (LastGruRow < NGGruix.RowCount) and (LastGruCol < NGGruix.ColCount) then begin
   aux := NGGruix.Cells[LastGruCol, LastGruRow];
   if (aux <> LastGruValue) then begin
    //poden pasar 3 coses
    //1- aux<>'' i LastColValue = '' -> crear un nou registre
    if (aux <> '') and (LastGruValue = '') then begin
     Sql := 'INSERT INTO Gruixos (Nom) VALUES ("'+aux+'")';
     BD.ModQuery(Sql);
     i := BD.NewID;
     LGruix.Add(IntToStr(i), aux);
    end;

    //2- aux<>'' i LastColValue <> '' -> modificar dada
    if (aux <> '') and (LastGruValue <> '') then begin
     S := LGruix.FindValueFromString(LastGruValue);
     Sql := 'UPDATE Gruixos SET Nom="'+aux+'" WHERE IDGruix="'+S+'"';
     BD.ModQuery(Sql);
     LGruix.Change(LGruix.FindString(LastGruValue), aux);
    end;

    //3- aux='' i LastColValue <> '' -> eliminar dada
    if (aux = '') and (LastGruValue <> '') then begin
     S := LGruix.FindValueFromString(LastGruValue);
     Sql := 'DELETE FROM Gruixos WHERE IDGruix="'+S+'"';
     BD.ModQuery(Sql);
     LGruix.DeleteString(LastGruValue);
    end;

//    showMessage(aux+'-'+LastColValue);
   end;
  end else begin // la celda antiga esta buida i sa tret automaticament
    if (LastGruValue <> '') then begin
     S := LGruix.FindValueFromString(LastGruValue);
     Sql := 'DELETE FROM Gruixos WHERE IDGruix="'+S+'"';
     BD.ModQuery(Sql);
     LGruix.DeleteString(LastGruValue);
    end;
  end;

  LastGruRow:= Row;
  LastGruCol:= Col;
  LastGruValue:= NGGruix.Cells[Col, Row];
 end;
end;

procedure TFDadesAux.NGMatColRowChanged(Sender: TObject; Col,
  Row: Integer);
var
 aux, Sql, S: String;
 i: Integer;
begin
 //materials
 if DoChange then begin
  if (LastMatRow < NGMat.RowCount) and (LastMatCol < NGMat.ColCount) then begin
   aux := NGMat.Cells[LastMatCol, LastMatRow];
   if (aux <> LastMatValue) then begin
    //poden pasar 3 coses
    //1- aux<>'' i LastColValue = '' -> crear un nou registre
    if (aux <> '') and (LastMatValue = '') then begin
     Sql := 'INSERT INTO Materials (Nom) VALUES ("'+aux+'")';
     BD.ModQuery(Sql);
     i := BD.NewID;
     LMat.Add(IntToStr(i), aux);
    end;

    //2- aux<>'' i LastColValue <> '' -> modificar dada
    if (aux <> '') and (LastMatValue <> '') then begin
     S := LMat.FindValueFromString(LastMatValue);
     Sql := 'UPDATE Materials SET Nom="'+aux+'" WHERE IDMaterial="'+S+'"';
     BD.ModQuery(Sql);
     LMat.Change(LMat.FindString(LastMatValue), aux);
    end;

    //3- aux='' i LastColValue <> '' -> eliminar dada
    if (aux = '') and (LastMatValue <> '') then begin
     S := LMat.FindValueFromString(LastMatValue);
     Sql := 'DELETE FROM Materials WHERE IDMaterial="'+S+'"';
     BD.ModQuery(Sql);
     LMat.DeleteString(LastMatValue);
    end;

//    showMessage(aux+'-'+LastColValue);
   end;
  end else begin // la celda antiga esta buida i sa tret automaticament
    if (LastMatValue <> '') then begin
     S := LMat.FindValueFromString(LastMatValue);
     Sql := 'DELETE FROM Materials WHERE IDMaterial="'+S+'"';
     BD.ModQuery(Sql);
     LMat.DeleteString(LastMatValue);
    end;
  end;

  LastMatRow:= Row;
  LastMatCol:= Col;
  LastMatValue:= NGMat.Cells[Col, Row];
 end;
end;

procedure TFDadesAux.BTOkClick(Sender: TObject);
begin
 FDadesAux.ModalResult := mrOk;
end;

procedure TFDadesAux.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 LCol.Destroy();
 LMat.Destroy();
 LGruix.Destroy();
end;

end.
