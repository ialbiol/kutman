object FResum: TFResum
  Left = 1335
  Top = 153
  Width = 953
  Height = 677
  Caption = 'KutMan - Resum'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 20
  object NGXaps: TNiceGrid
    Left = 0
    Top = 0
    Width = 945
    Height = 281
    Cursor = 101
    ColCount = 3
    RowCount = 1
    AlternateColor = clSilver
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -16
    HeaderFont.Name = 'Arial'
    HeaderFont.Style = [fsBold]
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    ReadOnly = True
    Columns = <
      item
        Title = 'Xapat'
        Width = 500
      end
      item
        Title = 'Mm Almacen'
        Width = 150
      end
      item
        Title = 'Mm Previstos'
        Width = 150
      end>
    GutterFont.Charset = DEFAULT_CHARSET
    GutterFont.Color = clWindowText
    GutterFont.Height = -11
    GutterFont.Name = 'MS Sans Serif'
    GutterFont.Style = []
    ShowFooter = False
    OnDrawCell = NGXapsDrawCell
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = []
    Align = alCustom
    TabOrder = 0
  end
  object BTOk: TBitBtn
    Left = 816
    Top = 576
    Width = 75
    Height = 73
    Caption = 'OK'
    TabOrder = 1
    OnClick = BTOkClick
    Glyph.Data = {
      360C0000424D360C000000000000360000002800000020000000200000000100
      180000000000000C0000120B0000120B00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAEDF9D7FD1
      6261C93D4DC42343C21741C2164AC4205DC9367AD05AA6DD90FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80D0613ABC0D28BC0029C5
      002FD10033D90034DD0135DE0134DB0030D30029C60026BC0034BB066FCC4DFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8BD66F32B90528C10031DA0035EC0138F0
      0139F30137F30137F30137F30037F20038F30137F00136ED0232DE0029C5002D
      B90077CF56FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF59C53325BB0030D80137EE0238F30136F10037F0
      0136F00034F00036F00037F00036F00137F00136F10137F10137F20137F00233
      DD0125BF0047C01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF42BC1926C20036E70137F30037F10038F00139F0023BF0
      0436EF0141EE0B38EE032FEF0036F10139F10037F00137F00037F00036F10037
      F30137EB032AC90033B80AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF47BF2129C70038EC0339F3023AF0033CF0063EF00940F10A39F0
      0287F267FEFEFECDF8BF84F06241ED0D35F0003BF00539F00237F00036F00037
      F00037F10037F0022DCE0036B90DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF59C53725C1003AED043CF3053FF00941F10C44F10F46F21148F21243EF
      0ED2F9C5FEFEFEFEFEFEFEFEFED6F9CC6FF0493BEF053EF0083CF00438F00136
      F00036F00038F10138F0022AC80043BE1BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF23BA003AE50640F30944F10F46F1124AF2154BF2194DF21B44F2117AF2
      53FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEDBF9D148EF1642F10D40F00B3CF00639
      F00237F00036F00038F20038EB0325BF0078CF5AFFFFFFFFFFFFFFFFFFFFFFFF
      3BBC1035D60244F40D47F2134BF2174DF21B50F21F53F2224FF21E5BF02EDCF9
      D3FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE6DF14344F20E45F11140F00C3D
      F00638F00137F00037F00138F30034DF002DB900FFFFFFFFFFFFFFFFFF8CD572
      27C00045EF114AF2174DF21C51F22055F22458F32954F42260F233D2F8C5FEFE
      FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE82F46146F2124AF21745F11240
      F10C3BF00538F00037F00038F00038F1022AC70069CA48FFFFFFFFFFFF45BE1B
      38D6054DF51B50F21F54F22459F3295CF42D56F3266EF245D2F7C7FEFEFEFEFE
      FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE8BF36C4AF2184EF21C4AF21645
      F2113FF1093AF00336F00136F00037F30133DD0032BA04FFFFFFC3E9B628BA00
      46E71553F52257F3265CF42D5DF52E5AF32B8FF270FEFEFEFEFEFEFEFEFEFEFE
      FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE8FF46E4FF31E51F2214DF21B49
      F21542F10D3CF00639F00137F00036F20136EC0126BD009BDA8391D7782BC100
      53F22258F42A5DF42F5BF52B6AF240B7F6A1FBFEFAFEFEFEFEFEFEFEFEFEFEFE
      FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE8DF56D54F42356F3264FF21F4B
      F21945F2113EF0093AF00237F00037F10138F2012AC9006BCC496ECE4B33CB02
      5BF62B5EF42F5BF32C82F360E0FAD6FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
      FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE8EF56D59F4285AF42A53F2234D
      F21C48F11441F00C3BEF0537F00037F00037F30130D50048C11E52C72741D511
      60F73266F43AB0F79AFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFE
      FED4F7C8E8FAE0FEFEFEFEFEFEFEFEFEFEFEFE94F5765BF42C5CF42D56F3264F
      F21E49F21643F10F3DF00638F00036F00038F20033DC003ABC0C42C1154AD91B
      62F83489F565FFFEFFFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFFFEFFD2F9
      C37CF657C8FAB8FEFEFEFEFEFEFEFEFEFEFEFEA6F68B5CF42D5EF43058F42850
      F2204BF21844F1103EF00838F00137F00038F30033DF0034BC0542C2174CDA1C
      6CF84173F64BD6F9CCFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEC6F9B888F9
      6583F95FA4F88BFEFEFEFEFEFEFEFEFEFEFEFED0F9C461F3335FF4325AF42A52
      F2224CF21A46F1113EF00838F00137F00038F30134DF0035BC0754C82A47D417
      72FA4972F74980F55CD1FAC3FEFEFEFEFEFEFEFEFEFEFEFEC3FAB18EFA6E91FB
      708EFB6C8AF868FEFEFEFEFEFEFEFEFEFEFEFEFEFEFE7FF45B5CF42C5AF32B53
      F2234CF21A46F1123EF00938F00137F00037F30031DB003BBC0D73CF5039CA08
      75F94E7CF7547EF95883F75FADF896FEFEFEFEFEFEABF99395FA7799FC7A98FA
      7994FB7389FA66A9F88FFEFEFEFEFEFEFEFEFEFEFEFEC3F8B259F2295AF42A53
      F2234CF21945F1113EF00838F00136F00037F3012ED3004DC22396DA7E2EC000
      75F54D7FF95A85F8628BF96A8EFA6B96FA7799FB7B9EFD7FA1FD85A1FC839DFC
      7E96FB7790FB6F86F863D0FAC2FEFEFEFEFEFEFEFEFEFEFEFE95F3784FF31D52
      F3224BF21945F1113DF00738F00036F00038F10129C70071CD4FCDECC02AB800
      68E93E85FB6288F96690FA6F97FB779DFD7FA3FD87A7FD8DA8FD8EA5FD899FFC
      8298FB7A91FB7088FA6587F664E0FAD7FEFEFEFEFEFEFEFEFEFEFEFE8AF26A48
      F11449F21544F0103CF00637F00036F20137EA0226BB00A3DD8FFFFFFF49C222
      4ED5208AFD678CF96A94FB739AFB7CA1FC85A9FD8EAFFE95ACFE94A6FD8A9FFC
      8397FB7990FA708AF9687EF85983F65FDBF9D0FEFEFEFEFEFEFEFEFEFEFEFEA4
      F48A45EF103EF1073CF00536F00037F40132DA0038BB0BFFFFFFFFFFFF98DA81
      2BBD0080F35B91FB6E95FB759CFC7FA2FC87A9FD90AEFD95ABFD91A4FD889DFC
      7F96FB768FFA6E88F96481F85C74F74D78F451CEF7BEFEFEFEFEFEFEFEFEFEFE
      FEFEB8F5A342EF1034F00036F10237EF0227C30076CF58FFFFFFFFFFFFFFFFFF
      41BE1A4DD32394FE7195FB769BFC7DA2FC85A6FC8BA7FD8CA5FD899FFD8298FB
      7A92FB718BFA6A85F9607EF85877F7506CF64265F43893F373FEFEFEFEFEFEFE
      FEFEFEFEFED2F7C647EE1734F30031DA0034BA08FFFFFFFFFFFFFFFFFFFFFFFF
      A5DE9123B70070E84B99FF7998FB7A9DFC80A1FC84A1FD849EFC8099FB7B94FB
      748DFA6C86F96481F95D7AF75473F64A6DF64265F53856F32660F1349BF57EFE
      FEFEFEFEFEFEFEFE6EF0472EE80023BB0089D56FFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF6CCB4D28BB0082F25D9BFE7C97FA7999FB7C99FB7A96FB7793FA738EFA
      6D87F86683F95E7DF85775F74E6FF74669F53E61F4345BF42D51F31F42F11044
      EE1264EE3B68F13E38EC0327C30051C22EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF57C53333C20680F15C99FE7893FB7292FB7190FB6F8CFA6B88F9
      6483F85E7DF85777F75070F7476BF64064F4385EF42F57F3284FF21F48F11641
      F10B35F3002EEC002AC80045BE1DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF51C22C29BC006FE74791FE708EFC6C88F96586F96283F8
      5D7DF85676F74F72F7486CF64166F53960F4325AF32A52F2224BF21946F21140
      F30937E50326C20042BD19FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF71CE5126B8004CD3207AF15386FC6182FB5C7CF8
      5576F74F71F7476DF64166F53A60F4335BF42C54F4244EF41C49F5143FED0C31
      D50225B9005EC739FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE0963CBD102ABC004AD31C68EB3E71F4
      4871F7486EF94369F83D63F8355DF72E56F4264DF01D45E91235D50427BE0039
      BB0A95DA7CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1DB8B4DC2222CBA002CBF
      0036C80443D31349D91A46D9173ED30D31C9012AC0002ABB0047C11B91D678FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDECC199D9
      8275D05257C92D43C41843C21652C7276FCF4C93D67AC4E8B7FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    Layout = blGlyphTop
  end
  object NGTabs: TNiceGrid
    Left = 0
    Top = 288
    Width = 945
    Height = 281
    Cursor = 101
    ColCount = 3
    RowCount = 1
    AlternateColor = clSilver
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clWindowText
    HeaderFont.Height = -16
    HeaderFont.Name = 'Arial'
    HeaderFont.Style = [fsBold]
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    ReadOnly = True
    Columns = <
      item
        Title = 'Tablero'
        Width = 500
      end
      item
        Title = 'Almacen'
        Width = 150
      end
      item
        Title = 'Previstos'
        Width = 150
      end>
    GutterFont.Charset = DEFAULT_CHARSET
    GutterFont.Color = clWindowText
    GutterFont.Height = -11
    GutterFont.Name = 'MS Sans Serif'
    GutterFont.Style = []
    ShowFooter = False
    OnDrawCell = NGTabsDrawCell
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = []
    Align = alCustom
    TabOrder = 2
  end
end
