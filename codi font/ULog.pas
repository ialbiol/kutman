unit ULog;

interface

var
 Ftx: TextFile;

 CTXT: Boolean; // 1- dibuixar les mides de les peces
 CLOG: Boolean; // 2 - fer log

procedure IniciLog();
procedure Escriurelog(Txt: String);
procedure EscriureSeparador();
procedure FinalLog();

implementation

uses SysUtils;

procedure IniciLog();
begin
 AssignFile(Ftx, 'Log.txt');
 ReWrite(Ftx);
end;

procedure Escriurelog(Txt: String);
begin
 WriteLn(Ftx, Txt);
 Flush(Ftx);
end;

procedure EscriureSeparador();
begin
 WriteLn(Ftx, '');
 WriteLn(Ftx, '============================================================');
 WriteLn(Ftx, '');
 Flush(Ftx);
end;

procedure FinalLog();
begin
 CloseFile(Ftx);
end;

end.
