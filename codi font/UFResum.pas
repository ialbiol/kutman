unit UFResum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, NiceGrid;

type
  TFResum = class(TForm)
    NGXaps: TNiceGrid;
    BTOk: TBitBtn;
    NGTabs: TNiceGrid;
    procedure BTOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NGXapsDrawCell(Sender: TObject; ACanvas: TCanvas; X,
      Y: Integer; Rc: TRect; var Handled: Boolean);
    procedure NGTabsDrawCell(Sender: TObject; ACanvas: TCanvas; X,
      Y: Integer; Rc: TRect; var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FResum: TFResum;

implementation

uses UDades;

{$R *.dfm}

procedure TFResum.BTOkClick(Sender: TObject);
begin
 //sortir
 FResum.ModalResult := mrOk;
end;

procedure TFResum.FormShow(Sender: TObject);
begin
 //mostrar dades
 LXap.FillCBUs(NGXaps);
 LTab.FillCBUs(NGTabs);
end;

procedure TFResum.NGXapsDrawCell(Sender: TObject; ACanvas: TCanvas; X,
  Y: Integer; Rc: TRect; var Handled: Boolean);
begin
 if StrToIntDef(NGXaps.Cells[1, Y], 0) < StrToIntDef(NGXaps.Cells[2, Y], 0) then begin
  ACanvas.Font.Color := clRed;
 end;
end;

procedure TFResum.NGTabsDrawCell(Sender: TObject; ACanvas: TCanvas; X,
  Y: Integer; Rc: TRect; var Handled: Boolean);
begin
 if StrToIntDef(NGTabs.Cells[1, Y], 0) < StrToIntDef(NGTabs.Cells[2, Y], 0) then begin
  ACanvas.Font.Color := clRed;
 end;
end;

end.
