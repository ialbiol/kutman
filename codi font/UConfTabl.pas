unit UConfTabl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, NiceGrid;

type
  TFConfTabl = class(TForm)
    Label1: TLabel;
    BTOk: TBitBtn;
    NGTab: TNiceGrid;
    CBMat: TComboBox;
    CBCol: TComboBox;
    CBGruix: TComboBox;
    procedure BTOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NGTabColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure NGTabDrawCell(Sender: TObject; ACanvas: TCanvas; X,
      Y: Integer; Rc: TRect; var Handled: Boolean);
    procedure CBColEnter(Sender: TObject);
    procedure CBColCloseUp(Sender: TObject);
    procedure CBColChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }

   LastRow: Integer;
   LastCol: Integer;
   LastValue: String;
   LastStr: String;
   LastColor: String;
   LastMat: String;
   LastGruix: String;
   LastAmple: String;
   LastAlt: String;
   LastQuant: String;

   DoChange: Boolean;
   DrawCb: Boolean;

   R: TRect;
  public
    { Public declarations }
  end;

var
  FConfTabl: TFConfTabl;

implementation

uses UDades, UInici;

{$R *.dfm}

procedure TFConfTabl.BTOkClick(Sender: TObject);
begin
 // ok
 FConfTabl.ModalResult := mrOk;
end;

procedure TFConfTabl.FormShow(Sender: TObject);
begin
 // inici
 DoChange := False;
 DrawCb := True;

 //mostrar tots los llistats
 LCol := TList.Create();
 LMat := TList.Create();
 LGruix := TList.Create();

 LCol.AddDB(BD.Query('SELECT * FROM Colors'));
 LMat.AddDB(BD.Query('SELECT * FROM Materials'));
 LGruix.AddDB(BD.Query('SELECT * FROM Gruixos'));

 LCol.FillCB(CBCol);
 LMat.FillCB(CBMat);
 LGruix.FillCB(CBGruix);

 LTab.FillCB(NGTab);

 LastRow:= NGTab.Row;
 LastCol:= NGTab.Col;
 LastStr:= NGTab.Cells[0, NGTab.Row];
 LastValue:= LTab.FindValueFromString(LastStr);
 LastColor:= NGTab.Cells[1, NGTab.Row];
 LastMat:= NGTab.Cells[2, NGTab.Row];
 LastGruix:= NGTab.Cells[3, NGTab.Row];
 LastAmple:= NGTab.Cells[4, NGTab.Row];
 LastAlt:= NGTab.Cells[5, NGTab.Row];
 LastQuant:= NGTab.Cells[6, NGTab.Row];

 DoChange := True;
end;

procedure TFConfTabl.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 //limpiesa
 LCol.Destroy();
 LMat.Destroy();
 LGruix.Destroy();
end;

procedure TFConfTabl.NGTabColRowChanged(Sender: TObject; Col,
  Row: Integer);
var
 auxStr, auxCol, auxMat, auxGru, auxAmp, auxAlt, auxQnt: String;
 Sql: String;
 i: Integer;
 buit, auxbuit: boolean;
begin
 CBMat.Visible := False; // amago per si de cas
 CBGruix.Visible := False; // amago per si de cas
 CBCol.Visible := False;


 if DoChange then begin
  buit := True;
  if ((LastStr <> '') or
      (LastColor <> '') or
      (LastMat <> '') or
      (LastGruix <> '') or
      (LastAmple <> '') or
      (LastQuant <> '') or
      (LastAlt <> '')) then buit := false;

  if (LastRow < NGTab.RowCount) then begin
   auxStr:= NGTab.Cells[0, LastRow];
   auxCol:= NGTab.Cells[1, LastRow];
   auxMat:= NGTab.Cells[2, LastRow];
   auxGru:= NGTab.Cells[3, LastRow];
   auxAmp:= NGTab.Cells[4, LastRow];
   auxAlt:= NGTab.Cells[5, LastRow];
   auxQnt:= NGTab.Cells[6, LastRow];

   if ((auxStr <> LastStr) or
       (auxCol <> LastColor) or
       (auxMat <> LastMat) or
       (auxGru <> LastGruix) or
       (auxAmp <> LastAmple) or
       (auxQnt <> LastQuant) or
       (auxAlt <> LastAlt)) then begin

    auxBuit := True;
   if ((auxStr <> '') or
       (auxCol <> '') or
       (auxMat <> '') or
       (auxGru <> '') or
       (auxAmp <> '') or
       (auxQnt <> '') or
       (auxAlt <> '')) then auxbuit := false;

    //busco los codis
    auxCol := LCol.FindValueFromString(auxCol);
    auxMat := LMat.FindValueFromString(auxMat);
    auxGru := LGruix.FindValueFromString(auxGru);

    //poden pasar 3 coses
    //1- aux<>'' i LastColValue = '' -> crear un nou registre
    if (auxbuit = False) and (buit = True) then begin
     Sql := 'INSERT INTO Tableros (Nom, IDColor, IDMaterial, IDGruix, Ample, Alt, Quant) VALUES ';
     Sql := Sql + '("'+auxStr+'", "'+auxCol+'", "'+auxMat+'", "'+auxGru+'", "'+auxAmp+'", "'+auxAlt+'", "'+auxQnt+'")';
     BD.ModQuery(Sql);
     i := BD.NewID;
     LTab.Add(IntToStr(i), auxStr, auxCol, auxMat, auxGru, auxAmp, auxAlt, auxQnt);
    end;

    //2- aux<>'' i LastColValue <> '' -> modificar dada
    if (auxbuit = False) and (buit = False) then begin
     Sql := 'UPDATE Tableros SET Nom="'+auxStr+'", IDColor="'+auxCol+'", IDMaterial="'+auxMat+'", IDGruix="'+auxGru+'", ';
     Sql := Sql + 'Ample="'+auxAmp+'", Alt="'+auxAlt+'", Quant="'+auxQnt+'" WHERE IDTablero="'+LastValue+'"';
     BD.ModQuery(Sql);
     LTab.Change(LTab.FindString(LastStr), auxStr, auxCol, auxMat, auxGru, auxAmp, auxAlt, auxQnt);
    end;

    //3- aux='' i LastColValue <> '' -> eliminar dada
    if (auxbuit = True) and (buit = False) then begin
     Sql := 'DELETE FROM Tableros WHERE IDTablero="'+LastValue+'"';
     BD.ModQuery(Sql);
     LTab.DeleteString(LastStr);
    end;

//    showMessage(aux+'-'+LastColValue);
   end;
  end else begin // la celda antiga esta buida i sa tret automaticament
    if (buit = False) then begin
     Sql := 'DELETE FROM Tableros WHERE IDTablero="'+LastValue+'"';
     BD.ModQuery(Sql);
     LTab.DeleteString(LastStr);
    end;
  end;

  //act
  LastRow:= NGTab.Row;
  LastCol:= NGTab.Col;
  LastStr:= NGTab.Cells[0, NGTab.Row];
  LastValue:= LTab.FindValueFromString(LastStr);
  LastColor:= NGTab.Cells[1, NGTab.Row];
  LastMat:= NGTab.Cells[2, NGTab.Row];
  LastGruix:= NGTab.Cells[3, NGTab.Row];
  LastAmple:= NGTab.Cells[4, NGTab.Row];
  LastAlt:= NGTab.Cells[5, NGTab.Row];
  LastQuant:= NGTab.Cells[6, NGTab.Row];
 end;
end;

procedure TFConfTabl.NGTabDrawCell(Sender: TObject; ACanvas: TCanvas; X,
  Y: Integer; Rc: TRect; var Handled: Boolean);
begin
 if DrawCb then begin
 if NGTab.Col = 1 then // dibuixo el combobox dels colors
 if (X = NGTab.Col) and (Y = NGTab.Row) then
  begin
      with CBCol do
      begin
        BringToFront;
        CopyRect(R, Rc);
        R.Top  := R.Top + NGTab.Top+2;
        R.Left := R.Left + NGTab.Left+2;
        SetBounds(R.Left, R.Top, R.Right-Rc.Left, R.Bottom-Rc.Top);
        ItemIndex := Items.IndexOf( NGTab.Cells[ NGTab.Col, NGTab.Row ] );
        Visible := True;
      end;
  end;

 if NGTab.Col = 2 then  // dibuixo el combobox dels materials
 if (X = NGTab.Col) and (Y = NGTab.Row) then
  begin
      with CBMat do
      begin
        BringToFront;
        CopyRect(R, Rc);
        R.Top  := R.Top + NGTab.Top+2;
        R.Left := R.Left + NGTab.Left+2;
        SetBounds(R.Left, R.Top, R.Right-Rc.Left, R.Bottom-Rc.Top);
        ItemIndex := Items.IndexOf( NGTab.Cells[ NGTab.Col, NGTab.Row ] );
        Visible := True;
      end;
  end;

 if NGTab.Col = 3 then  // dibuixo el combobox dels gruixos
 if (X = NGTab.Col) and (Y = NGTab.Row) then
  begin
      with CBGruix do
      begin
        BringToFront;
        CopyRect(R, Rc);
        R.Top  := R.Top + NGTab.Top+2;
        R.Left := R.Left + NGTab.Left+2;
        SetBounds(R.Left, R.Top, R.Right-Rc.Left, R.Bottom-Rc.Top);
        ItemIndex := Items.IndexOf( NGTab.Cells[ NGTab.Col, NGTab.Row ] );
        Visible := True;
      end;
  end;
 end;
end;

procedure TFConfTabl.CBColEnter(Sender: TObject);
begin
 DrawCb := False;
end;

procedure TFConfTabl.CBColCloseUp(Sender: TObject);
begin
 DrawCb := True;
 NGTab.SetFocus();
 CBCol.Visible := False;
 CBMat.Visible := False; // amago per si de cas
 CBGruix.Visible := False; // amago per si de cas
end;

procedure TFConfTabl.CBColChange(Sender: TObject);
begin
 with NGTab do
  Cells[Col, Row] := (Sender As TComboBox).Text;

 CBCol.Visible := False;
 CBMat.Visible := False;
 CBGruix.Visible := False; // amago per si de cas
end;

end.
