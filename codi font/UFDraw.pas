unit UFDraw;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, NiceGrid, ComCtrls, ToolWin, Grids, ActnList, ImgList,
  StdCtrls;

type
  TFDraw = class(TForm)
    TB1: TToolBar;
    TB01: TToolButton;
    TB02: TToolButton;
    TS01: TToolButton;
    NGTabs: TNiceGrid;
    PDib: TPanel;
    SGDib: TStringGrid;
    IList: TImageList;
    AList: TActionList;
    ATornar: TAction;
    AImprimir: TAction;
    EX: TLabeledEdit;
    EY: TLabeledEdit;
    EAlt: TLabeledEdit;
    EAmple: TLabeledEdit;
    CBXap1: TComboBox;
    CBXap2: TComboBox;
    CBXap3: TComboBox;
    CBXap4: TComboBox;
    TB03: TToolButton;
    TS02: TToolButton;
    AResum: TAction;
    Panel1: TPanel;
    AActualitzar: TAction;
    TS03: TToolButton;
    TB04: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure NGTabsColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure ATornarExecute(Sender: TObject);
    procedure AImprimirExecute(Sender: TObject);
    procedure SGDibMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AResumExecute(Sender: TObject);
    procedure AActualitzarExecute(Sender: TObject);
  private
    { Private declarations }

    TabShow: integer;
    Ok: Boolean;

    procedure LimpiarEdits();
  public
    { Public declarations }

    procedure DrawAngledText(const ACanvas: Graphics.TCanvas; const X, Y: Integer; const Angle: Double; const Text: string);
  end;

var
  FDraw: TFDraw;

implementation

uses UTablero, UConfPesa, UDades, UFResum;

{$R *.dfm}

procedure TFDraw.ATornarExecute(Sender: TObject);
begin
 //tornar
 FDraw.ModalResult := mrOk;
end;

procedure TFDraw.AImprimirExecute(Sender: TObject);
begin
//imprimir
 T.Save();
// ShowMessage('Impresora no configurada');
end;

procedure TFDraw.AResumExecute(Sender: TObject);
begin
 //mostrar resum
 application.CreateForm(UFResum.TFResum, UFResum.FResum);

 if UFResum.FResum.ShowModal() = mrOk then
  UFResum.FResum.Destroy();
end;

procedure TFDraw.AActualitzarExecute(Sender: TObject);
begin
 //actualitzar
 LTab.UpdateUs();
 LXap.UpdateUs();
end;

//////////////////////////////////////////////////////////

procedure TFDraw.FormCreate(Sender: TObject);
begin
 Ok := False;
 TabShow := -1;
end;

procedure TFDraw.FormShow(Sender: TObject);
begin
 LXap.FillCB(CBXap1);
 LXap.FillCB(CBXap2);
 LXap.FillCB(CBXap3);
 LXap.FillCB(CBXap4);

 T.MostrarTableros(NGTabs);

 Ok := True;

 AResumExecute(Sender); 
end;

procedure TFDraw.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 FDraw.ModalResult := mrOk;
end;

procedure TFDraw.NGTabsColRowChanged(Sender: TObject; Col, Row: Integer);
begin
 if Ok then begin
  LimpiarEdits();
  TabShow := StrToIntDef(NGTabs.Cells[0, Row], 0);
  T.Dibuixar(TabShow, SGDib);
 end;
end;

procedure TFDraw.SGDibMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
 P: TPesa;
begin
 if Ok then begin
  // showmessage(inttostr(X)+'-'+inttostr(Y));
  P := T.XYPesaDibuix(TabShow, X, Y);
  if P <> nil then begin //show pesa
   EX.Text := IntToStr(P.X);
   EY.Text := IntToStr(P.Y);
   EAmple.Text := IntToStr(P.AmpleTot);
   EAlt.Text := IntToStr(P.AltTot);

   CBXap1.ItemIndex := P.PXF;
   CBXap2.ItemIndex := P.PXD;
   CBXap3.ItemIndex := P.PXT;
   CBXap4.ItemIndex := P.PXE;
  end;
 end;
end;

///////////////////////////////////////////////////////////////
procedure TFDraw.LimpiarEdits();
begin
 EX.Text := '';
 EY.Text := '';
 EAmple.Text := '';
 EAlt.Text := '';
end;

///////////////////////////////////////////////////////////////
procedure TFDraw.DrawAngledText(const ACanvas: Graphics.TCanvas; const X, Y: Integer;
  const Angle: Double; const Text: string);
var
  AngledFontH: Windows.HFONT; // handle to rotated font
  OrigFontH: Windows.HFONT;   // handle to original font
  FontInfo: Windows.TLogFont; // font definition structure
begin
  // Configure logical font structure
  if Windows.GetObject(
    ACanvas.Font.Handle, SizeOf(FontInfo), @FontInfo
  ) = 0 then
    Exit;
  FontInfo.lfEscapement := Round(Angle * 10);
  FontInfo.lfOrientation := FontInfo.lfEscapement;
  // Create angled font
  AngledFontH := Windows.CreateFontIndirect(FontInfo);
  if AngledFontH = 0 then
    Exit;
  // Use angled font to write text
  OrigFontH := Windows.SelectObject(ACanvas.Handle, AngledFontH);
  ACanvas.TextOut(X, Y, Text);
  // Restore old unrotated font
  Windows.SelectObject(ACanvas.Handle, OrigFontH);
  // Dispose of angled font
  Windows.DeleteObject(AngledFontH);
end;
///////////////////////////////////////////////////////////////

end.
