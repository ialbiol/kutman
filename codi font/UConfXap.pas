unit UConfXap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, NiceGrid;

type
  TFConfXap = class(TForm)
    Label1: TLabel;
    CBCol: TComboBox;
    NGTab: TNiceGrid;
    BTOk: TBitBtn;
    procedure BTOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure NGTabColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure NGTabDrawCell(Sender: TObject; ACanvas: TCanvas; X,
      Y: Integer; Rc: TRect; var Handled: Boolean);
    procedure CBColEnter(Sender: TObject);
    procedure CBColCloseUp(Sender: TObject);
    procedure CBColChange(Sender: TObject);
  private
    { Private declarations }

   LastRow: Integer;
   LastCol: Integer;
   LastValue: String;
   LastStr: String;
   LastColor: String;
   LastGruix: String;
   LastMXRoll: String;
   LastQuant: String;

   DoChange: Boolean;
   DrawCb: Boolean;

   R: TRect;
  public
    { Public declarations }
  end;

var
  FConfXap: TFConfXap;

implementation

uses UDades, UInici;

{$R *.dfm}

procedure TFConfXap.BTOkClick(Sender: TObject);
begin
 FConfXap.ModalResult := mrOk;
end;

procedure TFConfXap.FormShow(Sender: TObject);
begin
 // inici
 DoChange := False;
 DrawCb := True;

 //mostrar tots los llistats
 LCol := TList.Create();

 LCol.AddDB(BD.Query('SELECT * FROM Colors'));

 LCol.FillCB(CBCol);

 LXap.FillCB(NGTab);

 LastRow:= NGTab.Row;
 LastCol:= NGTab.Col;
 LastStr:= NGTab.Cells[0, NGTab.Row];
 LastValue:= LTab.FindValueFromString(LastStr);
 LastColor:= NGTab.Cells[1, NGTab.Row];
 LastGruix:= NGTab.Cells[2, NGTab.Row];
 LastMXRoll:= NGTab.Cells[3, NGTab.Row];
 LastQuant:= NGTab.Cells[4, NGTab.Row];

 DoChange := True;
end;

procedure TFConfXap.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 //limpiesa
 LCol.Destroy();
end;

procedure TFConfXap.NGTabColRowChanged(Sender: TObject; Col, Row: Integer);
var
 auxStr, auxCol, auxGru, auxMXRoll, auxQnt: String;
 Sql: String;
 i: Integer;
 buit, auxbuit: boolean;
begin
 CBCol.Visible := False;


 if DoChange then begin
  buit := True;
  if ((LastStr <> '') or
      (LastColor <> '') or
      (LastGruix <> '') or
      (LastQuant <> '') or
      (LastMXRoll <> '')) then buit := false;

  if (LastRow < NGTab.RowCount) then begin
   auxStr:= NGTab.Cells[0, LastRow];
   auxCol:= NGTab.Cells[1, LastRow];
   auxGru:= NGTab.Cells[2, LastRow];
   auxMXRoll:= NGTab.Cells[3, LastRow];
   auxQnt:= NGTab.Cells[4, LastRow];

   if ((auxStr <> LastStr) or
       (auxCol <> LastColor) or
       (auxGru <> LastGruix) or
       (auxQnt <> LastQuant) or
       (auxMXRoll <> LastMXRoll)) then begin

    auxBuit := True;
   if ((auxStr <> '') or
       (auxCol <> '') or
       (auxGru <> '') or
       (auxQnt <> '') or
       (auxMXRoll <> '')) then auxbuit := false;

    //busco los codis
    auxCol := LCol.FindValueFromString(auxCol);

    //poden pasar 3 coses
    //1- aux<>'' i LastColValue = '' -> crear un nou registre
    if (auxbuit = False) and (buit = True) then begin
     Sql := 'INSERT INTO Xapats (Nom, IDColor, Gruix, MXRoll, Quant) VALUES ';
     Sql := Sql + '("'+auxStr+'", "'+auxCol+'", "'+auxGru+'", "'+auxMXRoll+'", "'+auxQnt+'")';
     BD.ModQuery(Sql);
     i := BD.NewID;
     LXap.Add(IntToStr(i), auxStr, auxCol, auxGru, auxMXRoll, auxQnt);
    end;

    //2- aux<>'' i LastColValue <> '' -> modificar dada
    if (auxbuit = False) and (buit = False) then begin
     Sql := 'UPDATE Xapats SET Nom="'+auxStr+'", IDColor="'+auxCol+'", Gruix="'+auxGru+'", ';
     Sql := Sql + ' MXRoll="'+auxMXRoll+'", Quant="'+auxQnt+'" WHERE IDXapat="'+LastValue+'"';
     BD.ModQuery(Sql);
     LXap.Change(LXap.FindString(LastStr), auxStr, auxCol, auxGru, auxMXRoll, auxQnt);
    end;

    //3- aux='' i LastColValue <> '' -> eliminar dada
    if (auxbuit = True) and (buit = False) then begin
     Sql := 'DELETE FROM Xapats WHERE IDXapat="'+LastValue+'"';
     BD.ModQuery(Sql);
     LXap.DeleteString(LastStr);
    end;

//    showMessage(aux+'-'+LastColValue);
   end;
  end else begin // la celda antiga esta buida i sa tret automaticament
    if (buit = False) then begin
     Sql := 'DELETE FROM Xapats WHERE IDXapat="'+LastValue+'"';
     BD.ModQuery(Sql);
     LXap.DeleteString(LastStr);
    end;
  end;

  //act
  LastRow:= NGTab.Row;
  LastCol:= NGTab.Col;
  LastStr:= NGTab.Cells[0, NGTab.Row];
  LastValue:= LXap.FindValueFromString(LastStr);
  LastColor:= NGTab.Cells[1, NGTab.Row];
  LastGruix:= NGTab.Cells[2, NGTab.Row];
  LastMXRoll:= NGTab.Cells[3, NGTab.Row];
  LastQuant:= NGTab.Cells[4, NGTab.Row];
 end;
end;

procedure TFConfXap.NGTabDrawCell(Sender: TObject; ACanvas: TCanvas; X,
  Y: Integer; Rc: TRect; var Handled: Boolean);
begin
 if DrawCb then begin
 if NGTab.Col = 1 then // dibuixo el combobox dels colors
 if (X = NGTab.Col) and (Y = NGTab.Row) then
  begin
      with CBCol do
      begin
        BringToFront;
        CopyRect(R, Rc);
        R.Top  := R.Top + NGTab.Top+2;
        R.Left := R.Left + NGTab.Left+2;
        SetBounds(R.Left, R.Top, R.Right-Rc.Left, R.Bottom-Rc.Top);
        ItemIndex := Items.IndexOf( NGTab.Cells[ NGTab.Col, NGTab.Row ] );
        Visible := True;
      end;
  end;
 end;
end;

procedure TFConfXap.CBColEnter(Sender: TObject);
begin
 DrawCb := False;
end;

procedure TFConfXap.CBColCloseUp(Sender: TObject);
begin
 DrawCb := True;
 NGTab.SetFocus();
 CBCol.Visible := False;
end;

procedure TFConfXap.CBColChange(Sender: TObject);
begin
 with NGTab do
  Cells[Col, Row] := (Sender As TComboBox).Text;

 CBCol.Visible := False;
end;

end.
