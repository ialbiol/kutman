unit UConfPesa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, UTablero;

type
  TFPesa = class(TForm)
    EAmp: TLabeledEdit;
    EAlt: TLabeledEdit;
    CBGir: TCheckBox;
    Panel1: TPanel;
    EXAlt: TEdit;
    EXEsq: TEdit;
    EXBaix: TEdit;
    EXDrt: TEdit;
    Label1: TLabel;
    BTOk: TBitBtn;
    BTKo: TBitBtn;
    Label2: TLabel;
    CBPrioritat: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BTKoClick(Sender: TObject);
    procedure BTOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    Pesa: TPesa;
  end;

var
  FPesa: TFPesa;

implementation

{$R *.dfm}

procedure TFPesa.FormCreate(Sender: TObject);
begin
 Pesa := nil;
end;

procedure TFPesa.FormShow(Sender: TObject);
begin
 //mostrar
 if Pesa <> nil then begin
  EAmp.Text := IntToStr(Pesa.AmpleTot);
  EAlt.Text := IntToStr(Pesa.AltTot);
  CBGir.Checked := Pesa.Gir;
  EXAlt.Text := IntToStr(Pesa.XF);
  EXEsq.Text := IntToStr(Pesa.XE);
  EXBaix.Text := IntToStr(Pesa.XT);
  EXDrt.Text := IntToStr(Pesa.XD);
 end;
end;

procedure TFPesa.BTOkClick(Sender: TObject);
begin
 //guardar canvis
 if Pesa <> nil then begin

 end;
end;

procedure TFPesa.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 FPesa.ModalResult := mrOk;
end;

procedure TFPesa.BTKoClick(Sender: TObject);
begin
 FPesa.ModalResult := mrOk;
end;

end.


