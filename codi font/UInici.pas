unit UInici;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ImgList, Menus, ExtCtrls, NiceGrid, Grids, ToolWin,
  ComCtrls, StdCtrls, UMySQLDB, jpeg;

type
  TFKutMan = class(TForm)
    NGLlista: TNiceGrid;
    MMenu: TMainMenu;
    AList1: TActionList;
    IList: TImageList;
    ASortir: TAction;
    Fitxer1: TMenuItem;
    Sortir1: TMenuItem;
    TB: TToolBar;
    TB1: TToolButton;
    TS1: TToolButton;
    AEsquema: TAction;
    TB2: TToolButton;
    Mostrar1: TMenuItem;
    Esquema1: TMenuItem;
    CBGir: TCheckBox;
    CBTabs: TComboBox;
    N1: TMenuItem;
    Configuraci1: TMenuItem;
    AConf: TAction;
    N2: TMenuItem;
    ObrirLlistat1: TMenuItem;
    GuardarLlistat1: TMenuItem;
    ASave: TAction;
    AOpen: TAction;
    OD1: TOpenDialog;
    SD1: TSaveDialog;
    ADades: TAction;
    N3: TMenuItem;
    Dadesauxiliars1: TMenuItem;
    ATableros: TAction;
    ableros1: TMenuItem;
    TB3: TToolButton;
    TB4: TToolButton;
    TS2: TToolButton;
    TS3: TToolButton;
    TB5: TToolButton;
    TB6: TToolButton;
    TB7: TToolButton;
    ToolButton1: TToolButton;
    Image1: TImage;
    AXapats: TAction;
    TB8: TToolButton;
    CBXap: TComboBox;
    AXapats1: TMenuItem;
    procedure ASortirExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure AEsquemaExecute(Sender: TObject);
    procedure NGLlistaDrawCell(Sender: TObject; ACanvas: TCanvas; X,
      Y: Integer; Rc: TRect; var Handled: Boolean);
    procedure NGLlistaColRowChanged(Sender: TObject; Col, Row: Integer);
    procedure CBGirClick(Sender: TObject);
    procedure AConfExecute(Sender: TObject);
    procedure ASaveExecute(Sender: TObject);
    procedure AOpenExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ADadesExecute(Sender: TObject);
    procedure ATablerosExecute(Sender: TObject);
    procedure CBTabsCloseUp(Sender: TObject);
    procedure CBTabsEnter(Sender: TObject);
    procedure CBTabsChange(Sender: TObject);
    procedure NGLlistaInsertRow(Sender: TObject; ARow: Integer);
    procedure AXapatsExecute(Sender: TObject);
  private
    { Private declarations }

    no: Boolean;
    DrawCb: Boolean;
    R: TRect;

    procedure PosarPeces();
    function TestPesa(Num: Integer): boolean;
    procedure IniciLlista();

  public
    { Public declarations }
  end;

var
  FKutMan: TFKutMan;
  BD: TMySQLDB;

implementation

uses UConfTabl, UFGL, UTablero, UDades, UConfProg, UFDadesAux, UFDraw,
  UConfXap;

{$R *.dfm}

procedure TFKutMan.ASortirExecute(Sender: TObject);
begin
 T.Destroy();

 Application.Terminate;
end;

procedure TFKutMan.AConfExecute(Sender: TObject);
begin
 application.CreateForm(UConfProg.TFConfProg, UConfProg.FConfProg);

 if UConfProg.FConfProg.ShowModal() = mrOk then
  UConfProg.FConfProg.Destroy();

 IniciLlista();
end;

procedure TFKutMan.ADadesExecute(Sender: TObject);
begin
 //obrir dades aux
 application.CreateForm(UFDadesAux.TFDadesAux, UFDadesAux.FDadesAux);

 if UFDadesAux.FDadesAux.ShowModal() = mrOk then
  UFDadesAux.FDadesAux.Destroy();

 IniciLlista();
end;

procedure TFKutMan.ATablerosExecute(Sender: TObject);
begin
//tableros
 application.CreateForm(UConfTabl.TFConfTabl, UConfTabl.FConfTabl);

 if UConfTabl.FConfTabl.ShowModal() = mrOk then
  UConfTabl.FConfTabl.Destroy();

 IniciLlista();
end;

procedure TFKutMan.AXapatsExecute(Sender: TObject);
begin
//xapats
 application.CreateForm(UConfXap.TFConfXap, UConfXap.FConfXap);

 if UConfXap.FConfXap.ShowModal() = mrOk then
  UConfXap.FConfXap.Destroy();

 IniciLlista();
end;

procedure TFKutMan.AEsquemaExecute(Sender: TObject);
var
 a, g, tb: Integer;
 xp1, xp2, xp3, xp4: Integer;
begin

 T.Clear();

// entrada de dades
 a:=0;
 while (a < NGLlista.RowCount) do begin
  if TestPesa(a) then begin
   g := 0;
   if NGLlista.Cells[4, a] = 'Si' then g := 1;

   tb := StrToIntDef(LTab.FindValueFromString(NGLlista.Cells[3, a]), 0);

   xp1 := LXap.FindString(NGLlista.Cells[6, a]);
   xp2 := LXap.FindString(NGLlista.Cells[7, a]);
   xp3 := LXap.FindString(NGLlista.Cells[8, a]);
   xp4 := LXap.FindString(NGLlista.Cells[9, a]);

   T.AddTab(StrToIntDef(NGLlista.Cells[0, a], 0), // num
         StrToIntDef(NGLlista.Cells[1, a], 0), // ample
         StrToIntDef(NGLlista.Cells[2, a], 0), // alt
         tb, //tablero
         g,                                               // girar
         StrToIntDef(NGLlista.Cells[5, a], 0), // prioritat
         xp1, // xapat
         xp2, // xapat
         xp3, // xapat
         xp4  // xapat
         );
         {
         StrToIntDef(NGLlista.Cells[6, a], 0), // xapat
         StrToIntDef(NGLlista.Cells[7, a], 0), // xapat
         StrToIntDef(NGLlista.Cells[8, a], 0), // xapat
         StrToIntDef(NGLlista.Cells[9, a], 0) // xapat
         );}
  end;
  a:=a+1;
 end;

 T.Calcular();

//opengl
{
 application.CreateForm(UFGL.TFGL, UFGL.FGL);

 if UFGL.FGL.ShowModal() = mrOk then
  UFGL.FGL.Destroy();
//}

//api win
 application.CreateForm(UFDraw.TFDraw, UFDraw.FDraw);

 if UFDraw.FDraw.ShowModal() = mrOk then
  UFDraw.FDraw.Destroy();

 IniciLlista();
end;

procedure TFKutMan.ASaveExecute(Sender: TObject);
var
 FF: TKtFile;
begin
 //guardar llistat en fitxer de text
 if (SD1.Execute()) then begin
  FF := TKtFile.Create();
  FF.ReadFrom(NGLlista);
  FF.Save(SD1.FileName);
  FF.Destroy();
 end;
end;

procedure TFKutMan.AOpenExecute(Sender: TObject);
var
 FF: TKtFile;
begin
 //obrir llistat en fixer de text
 if (OD1.Execute()) then begin
  FF := TKtFile.Create();
  FF.Load(OD1.FileName);
  FF.WriteTo(NGLlista);
  FF.Destroy();
 end;
end;

///////////////////////////////////////////////////
/////////
/////////////////////////////////////////////////////

procedure TFKutMan.FormShow(Sender: TObject);
begin
 no := True;
 DrawCb := True;

 UConfProg.LoadConf(ExtractFilePath(ParamStr(0)) + 'config.cfg');

 T := TKutMan.Create();

 BD := TMySQLDB.Create(ExtractFilePath(ParamStr(0)) + 'config.cfg');

 LTab := TTabList.Create();
 LTab.AddDB(BD.Query('SELECT * FROM Tableros'));
 LTab.FillCB(CBTabs);

 LXap := TXapList.Create();
 LXap.AddDB(BD.Query('SELECT * FROM Xapats'));
 LXap.FillCB(CBXap);

// si les peces han d venir d ela bd, fer aixo. seria importar desde altre prog
 PosarPeces();
end;

procedure TFKutMan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 BD.Destroy();
 LTab.Destroy();
end;

///////////////////////////////////////////////////////////////
procedure TFKutMan.PosarPeces();
{var
 Q: TMySQLQuery;
 a: Integer;}
begin
{
 Q := BD.Query('SELECT * FROM Llistat');
 a := 1;
 while a <= Q.Count do begin
   T.AddTab(Q.GetInt(1), // num
         Q.GetInt(2), // ample
         Q.GetInt(3), // alt
         Q.GetInt(8), // gruix
         Q.GetInt(4), // xapat
         Q.GetInt(5), // xapat
         Q.GetInt(7), // xapat
         Q.GetInt(6), // xapat
         Q.GetInt(9), // girar
         Q.GetInt(10)); // material

   NGLlista.Cells[0, NGLlista.Row] := Q.GetStr(1);
   NGLlista.Cells[1, NGLlista.Row] := Q.GetStr(2);
   NGLlista.Cells[2, NGLlista.Row] := Q.GetStr(3);
   NGLlista.Cells[3, NGLlista.Row] := Q.GetStr(8);
   NGLlista.Cells[4, NGLlista.Row] := Q.GetStr(4);
   NGLlista.Cells[5, NGLlista.Row] := Q.GetStr(5);
   NGLlista.Cells[6, NGLlista.Row] := Q.GetStr(6);
   NGLlista.Cells[7, NGLlista.Row] := Q.GetStr(7);
   NGLlista.Cells[8, NGLlista.Row] := 'Si';
   NGLlista.Cells[9, NGLlista.Row] := Q.GetStr(10);

   NGLlista.RowCount := NGLlista.RowCount + 1;
   NGLlista.Col := 0;
   NGLlista.Row := NGLlista.Row + 1;

   Q.Next();

  a := a + 1;
 end;
 }
end;

function TFKutMan.TestPesa(Num: Integer): boolean;
begin
 result := False;
 if (NGLlista.Cells[0, Num] <> '') and
    (NGLlista.Cells[1, Num] <> '') and
    (NGLlista.Cells[2, Num] <> '') and
    (NGLlista.Cells[3, Num] <> '') and
    (NGLlista.Cells[4, Num] <> '') and
    (NGLlista.Cells[5, Num] <> '') and
    (NGLlista.Cells[6, Num] <> '') and
    (NGLlista.Cells[7, Num] <> '') and
    (NGLlista.Cells[8, Num] <> '') and
    (NGLlista.Cells[9, Num] <> '') then result := True;
end;

procedure TFKutMan.IniciLlista();
begin
 NGLlista.Col := 0;
 NGLlista.Row := 0;
end;
////////////////////////////////////////////////////////////////

procedure TFKutMan.FormResize(Sender: TObject);
begin
// T.Escalar();
end;

procedure TFKutMan.NGLlistaInsertRow(Sender: TObject; ARow: Integer);
begin

 NGLlista.Cells[0, ARow] := '1'; //num
 NGLlista.Cells[1, ARow] := ''; //ample
 NGLlista.Cells[2, ARow] := ''; //alt
 NGLlista.Cells[3, ARow] := ''; //tablero
 NGLlista.Cells[4, ARow] := 'Si'; //girar
 NGLlista.Cells[5, ARow] := '100'; //prioritat
 NGLlista.Cells[6, ARow] := '0 mm'; //xapats
 NGLlista.Cells[7, ARow] := '0 mm';
 NGLlista.Cells[8, ARow] := '0 mm';
 NGLlista.Cells[9, ARow] := '0 mm';
end;

procedure TFKutMan.NGLlistaDrawCell(Sender: TObject; ACanvas: TCanvas; X,
  Y: Integer; Rc: TRect; var Handled: Boolean);
begin
 if DrawCb then begin // dibuixo el combobox
 if NGLlista.Col = 3 then
 if (X = NGLlista.Col) and (Y = NGLlista.Row) then
  begin
      with CBTabs do
      begin
        BringToFront;
        CopyRect(R, Rc);
        R.Top  := R.Top + NGLlista.Top+2;
        R.Left := R.Left + NGLlista.Left+2;
        SetBounds(R.Left, R.Top, R.Right-Rc.Left, R.Bottom-Rc.Top);
        ItemIndex := Items.IndexOf( NGLlista.Cells[ NGLlista.Col, NGLlista.Row ] );
        Visible := True;
      end;
  end;

 if (NGLlista.Col = 6) or
    (NGLlista.Col = 7) or
    (NGLlista.Col = 8) or
    (NGLlista.Col = 9) then //cb xapat
 if (X = NGLlista.Col) and (Y = NGLlista.Row) then
  begin
      with CBXap do
      begin
        BringToFront;
        CopyRect(R, Rc);
        R.Top  := R.Top + NGLlista.Top+2;
        R.Left := R.Left + NGLlista.Left+2;
        SetBounds(R.Left, R.Top, R.Right-Rc.Left, R.Bottom-Rc.Top);
        ItemIndex := Items.IndexOf( NGLlista.Cells[ NGLlista.Col, NGLlista.Row ] );
        Visible := True;
      end;
  end;
 end;//end drawcb

 if NGLlista.Col = 4 then // dibuixo el checkbox
 if (X = NGLlista.Col) and (Y = NGLlista.Row) then
  begin
      with CBGir do
      begin
        BringToFront;
        CopyRect(R, Rc);
        R.Top  := R.Top + NGLlista.Top+2 + ((NGLlista.DefRowHeight - Height) div 2);
        R.Left := R.Left + NGLlista.Left+2 + ((NGLlista.Columns[7].Width - Width) div 2);
        Top := R.Top;
        Left := R.Left;
    //    SetBounds(R.Left, R.Top, R.Right-Rc.Left, R.Bottom-Rc.Top);
        if (NGLlista.Cells[ NGLlista.Col, NGLlista.Row ] = 'Si') or (NGLlista.Cells[ NGLlista.Col, NGLlista.Row ] = '') then
         Checked := True Else Checked := False;
        Visible := True;
      end;
  end;
end;

procedure TFKutMan.NGLlistaColRowChanged(Sender: TObject; Col,
  Row: Integer);
begin
 CBTabs.Visible := False; // amago per si de cas
 CBXap.Visible := False;
 CBGir.Visible := False;
end;

procedure TFKutMan.CBGirClick(Sender: TObject);
begin
 if CBGir.Checked then begin
  NGLlista.Cells[NGLlista.Col, NGLlista.Row] := 'Si';
 end else begin
  NGLlista.Cells[NGLlista.Col, NGLlista.Row] := 'No';
 end;
 CBGir.Visible := False;
end;

procedure TFKutMan.CBTabsCloseUp(Sender: TObject);
begin
 DrawCb := True;
 NGLlista.SetFocus();
 CBTabs.Visible := False;
 CBXap.Visible := False;
end;

procedure TFKutMan.CBTabsEnter(Sender: TObject);
begin
 DrawCb := False;
end;

procedure TFKutMan.CBTabsChange(Sender: TObject);
begin
 with NGLlista do
  Cells[Col, Row] := (Sender As TComboBox).Text;

 CBTabs.Visible := False;
 CBXap.Visible := False;
end;

end.
