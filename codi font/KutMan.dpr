program KutMan;

uses
  Forms,
  UInici in 'UInici.pas' {FKutMan},
  UConfTabl in 'UConfTabl.pas' {FConfTabl},
  UFGL in 'UFGL.pas' {FGL},
  UglContext in 'include\UglContext.pas',
  TextGL in 'include\TextGL.pas',
  UTexture in 'include\UTexture.pas',
  UTablero in 'UTablero.pas',
  ULog in 'ULog.pas',
  UBaseBD in 'bd\UBaseBD.pas',
  UMySQLDB in 'bd\UMySQLDB.pas',
  mysql in 'bd\mysql.pas',
  UDades in 'UDades.pas',
  UConfProg in 'UConfProg.pas' {FConfProg},
  UConfPesa in 'UConfPesa.pas' {FPesa},
  UIdioma in 'UIdioma.pas',
  UFDadesAux in 'UFDadesAux.pas' {FDadesAux},
  UFDraw in 'UFDraw.pas' {FDraw},
  UConfXap in 'UConfXap.pas' {FConfXap},
  UFResum in 'UFResum.pas' {FResum};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFKutMan, FKutMan);
  Application.Run;
end.
