unit UglContext;

interface

uses Windows, OpenGL, SysUtils;


function CreateGLContext(const Wnd:THandle):Boolean;
procedure DestroyGLContext;
procedure ResizeGL(const W, H:Integer);
procedure SwapGL;
procedure ClearGL;
procedure ResetModelView;
procedure glBindTexture(target: GLenum; texture: GLuint); stdcall; external opengl32;

////////////////////////////////////////////////////
////// dibuixar
///////////////////////////////////////

procedure Triangle(X1, Y1, Z1, X2, Y2, Z2, X3, Y3, Z3, R, G, B: Double);
procedure Linia(X1, Y1, Z1, X2, Y2, Z2, R, G, B: Double);
procedure LiniesQuadrat(X, Y, Z, W, H, R, G, B: Double);
procedure Quadrat(X, Y, Z, W, H, R, G, B: Double);
procedure Cubo(X, Y, Z, W, H, D, R, G, B: Double);

procedure MostrarText(X, Y, Z, Size: Double; Txt: String);

var Dc:hDc;
    glContext:hglrc;

implementation

uses TextGL;

// This procedure is used for initialization some opengl parameters
procedure Initialize;
begin
  glClearColor(0.2, 0.2, 0.4, 0.0);
  glShadeModel(GL_SMOOTH);
  glClearDepth(1);
  glEnable(GL_DEPTH_TEST);
//  glDepthFunc(GL_LEQUAL);
  glDepthFunc(GL_LESS);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

{  glEnable(GL_TEXTURE_2D);                     // Enable Texture Mapping
  LoadTexture(ExtractFilePath(ParamStr(0))+'chrome.bmp', MyTextureTex);    // Load the Texture
  glBindTexture(GL_TEXTURE_2D, MyTextureTex);  // Bind the Texture to the object

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);

  glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
  glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
  glEnable(GL_TEXTURE_GEN_S);     // Enable spherical
  glEnable(GL_TEXTURE_GEN_T);     // Environment Mapping

  glPolygonMode(GL_BACK, GL_LINE); //}
end;


// Change to ModelView matrix
procedure ResetModelView;
begin
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
  gluLookAt(0, 20, 120, 0, 30, 0, 0, 1, 0); // Set position and orientation
end;

// Set Viewport and Aspect Ratio for Projection Matrix
procedure ResizeGL(const W, H:Integer);
var Aspect:Single;
begin
  Aspect:=1;
  glViewPort(0, 0, W, H);
  if (W > 0) and (H > 0) then
   Aspect:=W / H;
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  gluPerspective(45, Aspect, 1, 300);
  ResetModelView;
end;


// Create opengl context
function CreateGlContext(const Wnd:THandle):Boolean;
var Pfd: TPixelFormatDescriptor;
    iFormat: Integer;
begin
  ZeroMemory(@Pfd, SizeOf(TPixelFormatDescriptor));
  Dc:=GetDc(Wnd);
  Pfd.nSize := SizeOf( TPixelFormatDescriptor );
  Pfd.nVersion := 1;
  Pfd.dwFlags := PFD_DRAW_TO_WINDOW or PFD_SUPPORT_OPENGL or PFD_DOUBLEBUFFER;
  Pfd.iPixelType := PFD_TYPE_RGBA;
  Pfd.cColorBits := 32;
  Pfd.cDepthBits := 32;
  Pfd.iLayerType := PFD_MAIN_PLANE;
  iFormat := ChoosePixelFormat(Dc, @Pfd);
  if not SetPixelFormat(Dc, iFormat, @Pfd) then
   begin
     MessageBox(0, 'Error setting pixel format.', 'SetPixelFormat', MB_OK );
     Result:=False;
     Exit;
   end;
  glContext := wglCreateContext(Dc);
  wglMakeCurrent(Dc, glContext);
  Initialize;
  Result:=True;
end;

// Destroy opengl context and free device context
procedure DestroyGlContext;
begin
  wglMakeCurrent(Dc, 0);
  wglDeleteContext(glContext);
  DeleteDc(Dc);
end;

// Swaps opengl buffers
procedure SwapGL;
begin
  SwapBuffers(Dc);
end;

// Clear opengl buffers
procedure ClearGL;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
end;  

///////////////////////////////////////////////////////////////////////
////////// funcions de dibuix
/////////////////////////////////////////////////////////////////////

procedure Linia(X1, Y1, Z1, X2, Y2, Z2, R, G, B: Double);
begin
 glBegin (GL_LINES);
//  glNormal3f (0.0, 0.0, 1.0);
  glColor3f (R, G, B);
  glVertex3f (X1, Y1, Z1);
  glVertex3f (X2, Y2, Z2);
 glEnd;
end;

procedure Triangle(X1, Y1, Z1, X2, Y2, Z2, X3, Y3, Z3, R, G, B: Double);
begin
 glBegin (GL_TRIANGLES);
//  glNormal3f (0.0, 0.0, 1.0);
  glColor3f (R, G, B);
  glVertex3f (X1, Y1, Z1);
  glVertex3f (X2, Y2, Z2);
  glVertex3f (X3, Y3, Z3);
 glEnd;
end;

procedure LiniesQuadrat(X, Y, Z, W, H, R, G, B: Double);
begin
 glBegin (GL_LINES);
//  glNormal3f (0.0, 0.0, 1.0);
  glColor3f (R, G, B);
  glVertex3f (X, Y, Z);
  glVertex3f (X+W, Y, Z);

  glVertex3f (X+W, Y, Z);
  glVertex3f (X+W, Y+H, Z);

  glVertex3f (X+W, Y+H, Z);
  glVertex3f (X, Y+H, Z);

  glVertex3f (X, Y+H, Z);
  glVertex3f (X, Y, Z);
 glEnd;
end;

procedure Quadrat(X, Y, Z, W, H, R, G, B: Double);
begin
 glBegin (GL_QUADS);
//  glNormal3f (0.0, 0.0, 1.0);
  glColor3f (R, G, B);
  glVertex3f (X, Y, Z);
  glVertex3f (X+W, Y, Z);
  glVertex3f (X+W, Y+H, Z);
  glVertex3f (X, Y+H, Z);
 glEnd;
end;

procedure Cubo(X, Y, Z, W, H, D, R, G, B: Double);
begin
  glBegin(GL_QUADS);
    glColor3f (R, G, B);
    // Front Face
    glNormal3f( 0.0, 0.0, 1.0);
    glVertex3f(X,   Y,   Z);
    glVertex3f(X+W, Y,   Z);
    glVertex3f(X+W, Y+H, Z);
    glVertex3f(X,   Y+H, Z);
    // Back Face
    glNormal3f( 0.0, 0.0,-1.0);
    glVertex3f(X,   Y,   Z+D);
    glVertex3f(X,   Y+H, Z+D);
    glVertex3f(X+W, Y+H, Z+D);
    glVertex3f(X+W, Y,   Z+D);
    // Top Face
    glNormal3f( 0.0, 1.0, 0.0);
    glVertex3f(X,   Y+H, Z+D);
    glVertex3f(X,   Y+H, Z);
    glVertex3f(X+W, Y+H, Z);
    glVertex3f(X+W, Y+H, Z+D);
    // Bottom Face
    glNormal3f( 0.0,-1.0, 0.0);
    glVertex3f(X,   Y,   Z+D);
    glVertex3f(X+W, Y,   Z+D);
    glVertex3f(X+W, Y,   Z);
    glVertex3f(X,   Y,   Z);
    // Right face
    glNormal3f( 1.0, 0.0, 0.0);
    glVertex3f(X+W, Y,   Z+D);
    glVertex3f(X+W, Y+H, Z+D);
    glVertex3f(X+W, Y+H, Z);
    glVertex3f(X+W, Y,   Z);
    // Left Face
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(X,   Y,   Z+D);
    glVertex3f(X,   Y,   Z);
    glVertex3f(X,   Y+H, Z);
    glVertex3f(X,   Y+H, Z+D);
  glEnd();
end;

procedure MostrarText(X, Y, Z, Size: Double; Txt: String);
begin
 glColor3f (0, 0, 0);
 SetFontStyle(0);
 SetFontSize(Size);
 SetFontPos(X, Y, Z);
 glPrint(PChar(Txt));
 SetFontStyle(0); // reset to default

{ glFontBegin(&font);
// glScalef(8.0, 8.0, 8.0);
 glTranslatef(30, 30, 0);
 glFontTextOut("Test", 5, 5, 0);
 glFontEnd();
// glFlush();}
end;

end.
