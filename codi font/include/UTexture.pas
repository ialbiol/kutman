unit UTexture;

// Plain (alpha = 1)
// Transparent
// Transparent Range
// Font (white is drawn, black is transparent)
// Font Outline (Font with darker outline)
// Font Outline 2 (Font with darker outline)
// Font Black (black is drawn, white is transparent)
// Font Gray (gray is drawn, white is transparent)
// Arrow (for arrows, white is white, gray has color, black is transparent);

interface
uses {OpenGL12,} OpenGL, Windows, Classes, Graphics;

procedure glGenTextures(n: GLsizei; var textures: GLuint); stdcall; external opengl32;
procedure glBindTexture(target: GLenum; texture: GLuint); stdcall; external opengl32;

type
  TTexture = record
    TexNum:   integer;
    X:        real;
    Y:        real;
    Z:        real; // new
    W:        real;
    H:        real;
    Rot:      real; // 0 - 2*pi
    Int:      real; // intensity
    ColR:     real;
    ColG:     real;
    ColB:     real;
    TexW:     real; // used?
    TexH:     real; // used?
    TexX1:    real;
    TexY1:    real;
    TexX2:    real;
    TexY2:    real;
    Alpha:    real;
    FadeType: integer; //fade mode of this texture. 0-no fade. 1-up. 2-down. 3-right. 4-left
  end;

  TTextureEntry = record
    Name:       string;
    Typ:        string;
    // we use normal TTexture, it's easier to implement and if needed - we copy ready data
    Texture:    TTexture;
  end;

  TTextureDatabase = record
    Texture:    array of TTextureEntry;
  end;

  TTextureUnit = class
    Limit:      integer;

    function GetTexture(Name, Typ: string): TTexture;
    function FindTexture(Name, Typ: string): integer;
    function LoadTexture(FromRegistry: boolean; Nazwa, Format, Typ: PChar; Col: LongWord): TTexture; overload;

    procedure DrawTexture(Tex: TTexture);

    procedure FreeTextures();
  end;

var
  TexturePath:  String;
  Texture:          TTextureUnit;
  TextureDatabase:  TTextureDatabase;

  PrintScreenData:  array[0..1024*768-1] of longword;

  ActTex:     GLuint;//integer;

  TexOrygW:   integer;
  TexOrygH:   integer;
  TexNewW:    integer;
  TexNewH:    integer;

  TexFitW:    integer;
  TexFitH:    integer; // new for limit

  TextureD8:    array[1..1024*1024] of byte; // 1MB
  TextureD16:   array[1..1024*1024, 1..2] of byte;  // luminance/alpha tex (2MB)
  TextureD24: array[1..1024*1024, 1..3] of byte;  // normal 24-bit tex    (3MB)
  TextureD32: array[1..1024*1024, 1..4] of byte; // transparent 32-bit tex (4MB)

  Mipmapping: Boolean;

  PFade: real; //fade percent
  
implementation
uses Math, SysUtils, JPEG;

function TTextureUnit.GetTexture(Name, Typ: string): TTexture;
var
  T:    integer; // texture
begin
  T := FindTexture(Name, Typ);
  if T >= 0 then begin
    // use preloaded texture
    Result := TextureDatabase.Texture[T].Texture;
  end else begin
    // preload texture and use it
    T := Length(TextureDatabase.Texture);
    SetLength(TextureDatabase.Texture, T+1);
    TextureDatabase.Texture[T].Name := Name;
    TextureDatabase.Texture[T].Typ := Typ;    
    TextureDatabase.Texture[T].Texture := LoadTexture(false, pchar(Name), 'JPG', pchar(Typ), $0);

    Result := TextureDatabase.Texture[T].Texture;
  end;
end;

function TTextureUnit.FindTexture(Name, Typ: string): integer;
var
  T:    integer; // texture
begin
  Result := -1;
  for T := 0 to high(TextureDatabase.Texture) do
    if (TextureDatabase.Texture[T].Name = Name) and (TextureDatabase.Texture[T].Typ = Typ) then
      Result := T;
end;

function TTextureUnit.LoadTexture(FromRegistry: boolean; Nazwa, Format, Typ: PChar; Col: LongWord): TTexture;
var
  Res:        TResourceStream;
  TextureB:   TBitmap;
  TextureJ:   TJPEGImage;

  Pet:        integer;
  Pet2:       integer;
  Pix:        integer;
  ColInt:     real;
  PPix:       PByteArray;
  TempA:      integer;
  Error:      integer;
begin
  Mipmapping := true;

  if FromRegistry then begin
    try
      Res := TResourceStream.Create(HInstance, Nazwa, Format);
    except
      beep;
 //     MSGERROR:= MSGERROR+ ' / can not open resources';
 //     ALLOK:= false;
      Exit;
    end;
  end;

  if FromRegistry or ((not FromRegistry) and FileExists(Nazwa)) then begin

  TextureB := TBitmap.Create;

  if Format = 'JPG' then begin
    TextureJ := TJPEGImage.Create;
    if FromRegistry then TextureJ.LoadFromStream(Res)
    else begin
      if FileExists(Nazwa) then begin
        FileMode := fmOpenRead; // read only. dvd mode
        TextureJ.LoadFromFile(Nazwa);
      end else begin
//        MSGERROR:= MSGERROR+ ' / can not open texture file: '+Nazwa;
//        ALLOK:= false;
        Exit;
      end;
    end;
    TextureB.Assign(TextureJ);
    TextureJ.Free;
  end;
  if Format = 'BMP' then begin
    if FromRegistry then TextureB.LoadFromStream(Res)
    else begin
      if FileExists(Nazwa) then begin
         FileMode := fmOpenRead; // read only. dvd mode
         TextureB.LoadFromFile(Nazwa);
      end else begin
//        MSGERROR:= MSGERROR+ ' / can not open texture file: '+Nazwa;
//        ALLOK:= false;
        Exit;
      end;
    end;
  end;

  if FromRegistry then Res.Free;

  glGenTextures(1, ActTex);
  glBindTexture(GL_TEXTURE_2D, ActTex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  if Typ = 'Plain' then begin
    // wymiary
    TexOrygW := TextureB.Width;
    TexOrygH := TextureB.Height;
    TexNewW := Round(Power(2, Ceil(Log2(TexOrygW))));
    TexNewH := Round(Power(2, Ceil(Log2(TexOrygH))));

    // kopiowanie
    TextureB.PixelFormat := pf24bit;

  // new system
    if (TexOrygW <= Limit) and (TexOrygW <= Limit) then begin
      if (TextureB.PixelFormat = pf24bit) then begin
        for Pet := 0 to TexOrygH-1 do begin
          PPix := TextureB.ScanLine[Pet];
          for Pet2 := 0 to TexOrygW-1 do begin
            TextureD24[Pet*TexNewW + Pet2+1, 1] := PPix[Pet2*3+2];
            TextureD24[Pet*TexNewW + Pet2+1, 2] := PPix[Pet2*3+1];
            TextureD24[Pet*TexNewW + Pet2+1, 3] := PPix[Pet2*3];
          end;
        end;
      end;
    end else begin
      // limit
      TexFitW := 4 * (TexOrygW div 4); // fix for bug in gluScaleImage
      TexFitH := TexOrygH;
      if (TextureB.PixelFormat = pf24bit) then begin
        for Pet := 0 to TexOrygH-1 do begin
          PPix := TextureB.ScanLine[Pet];
          for Pet2 := 0 to TexOrygW-1 do begin
            TextureD24[Pet*TexFitW + Pet2+1, 1] := PPix[Pet2*3+2];
            TextureD24[Pet*TexFitW + Pet2+1, 2] := PPix[Pet2*3+1];
            TextureD24[Pet*TexFitW + Pet2+1, 3] := PPix[Pet2*3];
          end;
        end;
      end;

      gluScaleImage(GL_RGB, TexFitW, TexFitH, GL_UNSIGNED_BYTE, @TextureD24, Limit, Limit, GL_UNSIGNED_BYTE, @TextureD24); // takes some time

      TexNewW := Limit;
      TexNewH := Limit;
      TexOrygW := Limit;
      TexOrygH := Limit;
    end;

    glTexImage2D(GL_TEXTURE_2D, 0, 3, TexNewW, TexNewH, 0, GL_RGB, GL_UNSIGNED_BYTE, @TextureD24);
    if Mipmapping then begin
      Error := gluBuild2DMipmaps(GL_TEXTURE_2D, 3, TexNewW, TexNewH, GL_RGB, GL_UNSIGNED_BYTE, @TextureD24);
      if Error > 0 then beep;
    end
  end;

  if Typ = 'Transparent' then begin
    // wymiary
    TexOrygW := TextureB.Width;
    TexOrygH := TextureB.Height;
    TexNewW := Round(Power(2, Ceil(Log2(TexOrygW))));
    TexNewH := Round(Power(2, Ceil(Log2(TexOrygH))));
    TextureB.Width := TexNewW;
    TextureB.Height := TexNewH;
    // kopiowanie
    for Pet := 0 to TexOrygH-1 do begin
      for Pet2 := 0 to TexOrygW-1 do begin
        Pix := TextureB.Canvas.Pixels[Pet2, Pet];
        if Pix = Col then begin
          TextureD32[Pet*TexNewW + Pet2 + 1, 1] := 0;
          TextureD32[Pet*TexNewW + Pet2 + 1, 2] := 0;
          TextureD32[Pet*TexNewW + Pet2 + 1, 3] := 0;
          TextureD32[Pet*TexNewW + Pet2 + 1, 4] := 0;
        end else begin
          TextureD32[Pet*TexNewW + Pet2+1, 1] := Pix;
          TextureD32[Pet*TexNewW + Pet2+1, 2] := Pix div 256;
          TextureD32[Pet*TexNewW + Pet2+1, 3] := Pix div (256*256);
          TextureD32[Pet*TexNewW + Pet2+1, 4] := 255;
        end;
      end;
    end;
    glTexImage2D(GL_TEXTURE_2D, 0, 4, TexNewW, TexNewH, 0, GL_RGBA, GL_UNSIGNED_BYTE, @TextureD32);
  end;

  if Typ = 'Font' then begin
    TextureB.PixelFormat := pf24bit;
    for Pet := 0 to TextureB.Height-1 do begin
      PPix := TextureB.ScanLine[Pet];
      for Pet2 := 0 to TextureB.Width-1 do begin
        Pix := PPix[Pet2 * 3];
        TextureD16[Pet*TextureB.Width + Pet2 + 1, 1] := 255;
        TextureD16[Pet*TextureB.Width + Pet2 + 1, 2] := Pix;
      end;
    end;
    glTexImage2D(GL_TEXTURE_2D, 0, 2, TextureB.Width, TextureB.Height, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, @TextureD16);

    if Mipmapping then glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    if Mipmapping then begin
      Error := gluBuild2DMipmaps(GL_TEXTURE_2D, 2, TextureB.Width, TextureB.Height, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, @TextureD16);
      if Error > 0 then beep;
    end;
  end;

  if Typ = 'Font Outline' then begin
    TextureB.PixelFormat := pf24bit;
    for Pet := 0 to TextureB.Height-1 do begin
      PPix := TextureB.ScanLine[Pet];
      for Pet2 := 0 to TextureB.Width-1 do begin
        Pix := PPix[Pet2 * 3];

        Col := Pix;
        if Col < 127 then Col := 127;

        TempA := Pix;
        if TempA >= 95 then TempA := 255;
        if TempA >= 31 then TempA := 255;
        if Pix < 95 then TempA := (Pix * 256) div 96;

        TextureD16[Pet*TextureB.Width + Pet2 + 1, 1] := Col;
        TextureD16[Pet*TextureB.Width + Pet2 + 1, 2] := TempA;

      end;
    end;
    glTexImage2D(GL_TEXTURE_2D, 0, 2, TextureB.Width, TextureB.Height, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, @TextureD16);

    if Mipmapping then glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    if Mipmapping then begin
      Error := gluBuild2DMipmaps(GL_TEXTURE_2D, 2, TextureB.Width, TextureB.Height, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, @TextureD16);
      if Error > 0 then beep;
    end;
  end;

  if Typ = 'Font Black' then begin
    // normalnie 0,125s     bez niczego 0,015s - 0,030s    z pix 0,125s
    // wymiary
    TextureB.PixelFormat := pf24bit;
    TexOrygW := TextureB.Width;
    TexOrygH := TextureB.Height;
    TexNewW := Round(Power(2, Ceil(Log2(TexOrygW))));
    TexNewH := Round(Power(2, Ceil(Log2(TexOrygH))));
    TextureB.Width := TexNewW;
    TextureB.Height := TexNewH;
    // kopiowanie
    for Pet := 0 to TextureB.Height-1 do begin
      PPix := TextureB.ScanLine[Pet];
      for Pet2 := 0 to TextureB.Width-1 do begin
        Pix := PPix[Pet2*3];
        TextureD32[Pet*TextureB.Width + Pet2 + 1, 1] := 255; // 0
        TextureD32[Pet*TextureB.Width + Pet2 + 1, 2] := 255;
        TextureD32[Pet*TextureB.Width + Pet2 + 1, 3] := 255;
        TextureD32[Pet*TextureB.Width + Pet2 + 1, 4] := 255 - (Pix mod 256);
      end;
    end;
    glTexImage2D(GL_TEXTURE_2D, 0, 4, TextureB.Width, TextureB.Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, @TextureD32);
  end;

  TextureB.Free;

  Result.X := 0;
  Result.Y := 0;
  Result.Z := 0;
  Result.W := 10;
  Result.H := 10;
  Result.Rot := 0;
  Result.TexNum := ActTex;
  Result.TexW := TexOrygW / TexNewW;
  Result.TexH := TexOrygH / TexNewH;

  Result.Int := 1;
  Result.ColR := 1;
  Result.ColG := 1;
  Result.ColB := 1;
  Result.Alpha := 1;
  Result.FadeType := 0;//no fade

  // 0.4.2 new test - default use whole texure, taking TexW and TexH as const and changing these
  Result.TexX1 := 0;
  Result.TexY1 := 0;
  Result.TexX2 := 1;
  Result.TexY2 := 1;
  end; // end if
end;

procedure TTextureUnit.DrawTexture(Tex: TTexture);
var
  x1, x2, x3, x4:       real;
  y1, y2, y3, y4:       real;
  xt1, xt2, xt3, xt4:   real;
  yt1, yt2, yt3, yt4:   real;
begin
  with Tex do begin
    glColor4f(ColR * Int, ColG * Int, ColB * Int, Alpha);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glDepthRange(0, 10);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBindTexture(GL_TEXTURE_2D, TexNum);

    x1 := x;
    x2 := x;
    x3 := x+w;
    x4 := x+w;
    y1 := y;
    y2 := y+h;
    y3 := y+h;
    y4 := y;
    if Rot <> 0 then begin
      xt1 := x1 - (x + w/2);
      xt2 := x2 - (x + w/2);
      xt3 := x3 - (x + w/2);
      xt4 := x4 - (x + w/2);
      yt1 := y1 - (y + h/2);
      yt2 := y2 - (y + h/2);
      yt3 := y3 - (y + h/2);
      yt4 := y4 - (y + h/2);

      x1 := (x + w/2) + xt1 * cos(Rot) - yt1 * sin(Rot);
      x2 := (x + w/2) + xt2 * cos(Rot) - yt2 * sin(Rot);
      x3 := (x + w/2) + xt3 * cos(Rot) - yt3 * sin(Rot);
      x4 := (x + w/2) + xt4 * cos(Rot) - yt4 * sin(Rot);

      y1 := (y + h/2) + yt1 * cos(Rot) + xt1 * sin(Rot);
      y2 := (y + h/2) + yt2 * cos(Rot) + xt2 * sin(Rot);
      y3 := (y + h/2) + yt3 * cos(Rot) + xt3 * sin(Rot);
      y4 := (y + h/2) + yt4 * cos(Rot) + xt4 * sin(Rot);
    end;

    glBegin(GL_QUADS);
      glTexCoord2f(TexX1*TexW, TexY1*TexH); glVertex3f(x1, y1, z);
      glTexCoord2f(TexX1*TexW, TexY2*TexH); glVertex3f(x2, y2, z);
      glTexCoord2f(TexX2*TexW, TexY2*TexH); glVertex3f(x3, y3, z);
      glTexCoord2f(TexX2*TexW, TexY1*TexH); glVertex3f(x4, y4, z);
    glEnd;

  end;
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_DEPTH_TEST);
end;

procedure TTextureUnit.FreeTextures();
begin
 SetLength(TextureDatabase.Texture, 0);
end;

end.
